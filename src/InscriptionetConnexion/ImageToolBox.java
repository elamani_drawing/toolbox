package InscriptionetConnexion;

import java.nio.file.Paths;

public interface ImageToolBox {

		String calendrier  = Paths.get(System.getProperty("user.dir"), "image","calendrier.png").toString();
		String chrono  = Paths.get(System.getProperty("user.dir"), "image","chrono.png").toString();
		String envoie  = Paths.get(System.getProperty("user.dir"), "image","envoie.png").toString();
		String messagerie  = Paths.get(System.getProperty("user.dir"), "image","messagerie.png").toString();
		String portefeuille  = Paths.get(System.getProperty("user.dir"), "image","portefeuille.png").toString();
		String search  = Paths.get(System.getProperty("user.dir"), "image","search.png").toString();
		String whiteandblack  = Paths.get(System.getProperty("user.dir"), "image","whiteandblack.jpg").toString();
		String accueil  = Paths.get(System.getProperty("user.dir"), "image","accueil.png").toString();
		
	}

