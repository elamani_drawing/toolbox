package InscriptionetConnexion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.awt.CardLayout;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.UIManager;

import Accueil.Accueil;
import InscriptionetConnexion.Inscription;
import PopuperreurToolBox.Popuperreur;




public class Connexion_Variablesession {

	
	public JFrame frame;
	String valeurrecup;
	String test;
	String pseudo;
	
	ArrayList<String> liste_pseudo = new ArrayList<>();
	ArrayList<String> liste_mdp = new ArrayList<>();
	ArrayList<String> liste_id = new ArrayList<>();
	
	static String namec = "";
	static int idconnexion = 0;
	/**
	 * lance le constructeur de l'application
	 * @author Arthur
	 * 
	 * 
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Connexion_Variablesession window = new Connexion_Variablesession();
					window.frame.setVisible(true);
				} catch (Exception e) {
					Popuperreur window = new Popuperreur();
					window.frame.setVisible(true); 
				}
			}
		});
	}

	/**
	 * 
	 *R�alise la cr�ation de l'application de Connexion a l'application
	 *aucun param�tres
	 *@author Arthur
	 *
	 */

	public Connexion_Variablesession() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 346, 428);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setBounds(0, 0, 330, 36);
		frame.getContentPane().add(panel);
		
		JLabel lblToolBox = new JLabel("ToolBox");
		lblToolBox.setHorizontalAlignment(SwingConstants.CENTER);
		lblToolBox.setForeground(Color.WHITE);
		lblToolBox.setFont(new Font("Tw Cen MT", Font.PLAIN, 27));
		panel.add(lblToolBox);
		
		JTextArea textAreapseudo = new JTextArea();
		textAreapseudo.setFont(new Font("Monospaced", Font.PLAIN, 20));
		textAreapseudo.setBackground(UIManager.getColor("Button.background"));
		textAreapseudo.setBounds(26, 92, 265, 36);
		frame.getContentPane().add(textAreapseudo);
		
		JPasswordField textAreamdp = new JPasswordField();
		textAreamdp.setFont(new Font("Monospaced", Font.PLAIN, 20));
		textAreamdp.setBackground(UIManager.getColor("Button.background"));
		textAreamdp.setBounds(26, 184, 265, 36);
		frame.getContentPane().add(textAreamdp);
		
		JPanel panel_1_2 = new JPanel();
		panel_1_2.setBackground(Color.WHITE);
		panel_1_2.setBounds(53, 47, 213, 34);
		frame.getContentPane().add(panel_1_2);
		panel_1_2.setLayout(new CardLayout(0, 0));
		
		JLabel lblPseudo = new JLabel("Pseudo ");
		lblPseudo.setHorizontalAlignment(SwingConstants.CENTER);
		lblPseudo.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		panel_1_2.add(lblPseudo, "name_718411116841400");
		
		JPanel panel_1_2_1 = new JPanel();
		panel_1_2_1.setBackground(Color.WHITE);
		panel_1_2_1.setBounds(53, 139, 213, 34);
		frame.getContentPane().add(panel_1_2_1);
		panel_1_2_1.setLayout(new CardLayout(0, 0));
		
		JLabel lblMotDePasse = new JLabel("Mot de passe ");
		lblMotDePasse.setHorizontalAlignment(SwingConstants.CENTER);
		lblMotDePasse.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		panel_1_2_1.add(lblMotDePasse, "name_718453559522000");
		
		JButton btnConnexion = new JButton("Connexion");
		btnConnexion.setBackground(Color.WHITE);
		btnConnexion.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.PLAIN, 17));
		btnConnexion.setBounds(26, 231, 265, 47);
		frame.getContentPane().add(btnConnexion);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.BLACK);
		panel_2.setBounds(0, 353, 330, 36);
		frame.getContentPane().add(panel_2);
		
		JButton btnInscription = new JButton("Inscription");
		btnInscription.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.PLAIN, 17));
		btnInscription.setBackground(Color.WHITE);
		btnInscription.setBounds(26, 289, 265, 47);
		frame.getContentPane().add(btnInscription);
		
		/**
		 * 
		 * l'action listener ouvre la page inscription si la personne n'a pas de compte (bouton inscription)
		 * @author Arthur
		 */
		
		btnInscription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Inscription window = new Inscription();
				window.frame.setVisible(true);//ouverture de l'inscription
				frame.dispose();
			}
		});
		
		try { 


            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root","root");

            Statement st =  con.createStatement(); // permet d'assigner une requete sql

            ResultSet res = st.executeQuery("SELECT pseudo FROM inscription");  //assigne la requete sql a res
            
            
            
            		while(res.next()) { // parcours pseudo FROM inscriptions
            			liste_pseudo.add(res.getString("pseudo")); //liste_pseudo = parcours pseudo FROM inscriptions

            		
            };
            
    		
    		
}catch(Exception v) {
    System.out.println("une erreur");
}
            	
            		
		            btnConnexion.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
        	        
                	pseudo = textAreapseudo.getText();
                	char[] c = textAreamdp.getPassword();
   		            String mdp = new String(c);
   		   
                    boolean test = liste_pseudo.contains(mdp);
            
            		if (test = true) {
            			
            			
            			
            			
            			try {
            				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root","root");

            	            Statement st =  con.createStatement(); 
            				
            				
							ResultSet res2 = st.executeQuery("SELECT mdp FROM inscription where pseudo ='"+pseudo+"'");
							while(res2.next()) { // parcours SELECT mdp FROM inscriptions where pseudo ='pseudo'
								liste_mdp.add(res2.getString("mdp")); //liste_pseudo = SELECT mdp FROM inscriptions where pseudo ='pseudo'
			            		
			            		
			            } //fin while
							
								
							if (liste_mdp.get(0).equals(mdp)) {
								namec = pseudo;
								Accueil window = new Accueil();
								window.frame.setVisible(true); 
								frame.dispose();
								
								Connection con2 = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root","root");

	            	            Statement st2 =  con.createStatement(); 
	            				
	            			
								ResultSet res3 = st.executeQuery("SELECT id FROM inscription where pseudo ='"+pseudo+"'");
								while(res3.next()) { // parcours SELECT id FROM inscriptions where pseudo ='pseudo'
									liste_id.add(res3.getString("id")); //liste_pseudo = SELECT id FROM inscriptions where pseudo ='pseudo'
								}
									 
								  idconnexion = Integer.parseInt(liste_id.get(0));
								  
							}	
							
            			} catch (Exception e1) {
							// TODO Auto-generated catch block
            				Popuperreur window = new Popuperreur();
							window.frame.setVisible(true);
						}
				
							
							
			
            		}
                }});
	
	}
	
	/**
	 * @author Arthur
	 * 
	 * le String getNameC  recup�re le nom
	 * @return le nom de l'utilisateur connect�
	 */
	public static String getNameC() {
		return namec;
	}
	
	/**
	 * @author Arthur
	 * 
	 * le String getIDC  recup�re l'ID
	 * @return l'ID de l'utilisateur connect�
	 */
	public static int getIdC() {
		return idconnexion;
	}
	}
