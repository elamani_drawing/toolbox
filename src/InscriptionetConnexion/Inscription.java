package InscriptionetConnexion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.awt.CardLayout;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.UIManager;

import InscriptionetConnexion.Connexion_Variablesession;
import PopuperreurToolBox.Popuperreur;

public class Inscription {

	JFrame frame;
	ArrayList<String> liste = new ArrayList<>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inscription window = new Inscription();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Initialise l'application Inscription
	 * @author Arthur
	 */
	public Inscription() {
		initialize();
	}

	/**
	 * Initialise les diff�rents elements de l'application Inscription
	 * @author Arthur
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 346, 609);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setBounds(0, 0, 330, 36);
		frame.getContentPane().add(panel);
		
		JLabel lblToolBox = new JLabel("ToolBox");
		lblToolBox.setHorizontalAlignment(SwingConstants.CENTER);
		lblToolBox.setForeground(Color.WHITE);
		lblToolBox.setFont(new Font("Tw Cen MT", Font.PLAIN, 27));
		panel.add(lblToolBox);
		
		JTextArea textAreanom = new JTextArea();
		textAreanom.setFont(new Font("Monospaced", Font.PLAIN, 20));
		textAreanom.setBackground(UIManager.getColor("Button.background"));
		textAreanom.setBounds(26, 92, 265, 36);
		frame.getContentPane().add(textAreanom);
		
		JTextArea textAreapseudo = new JTextArea();
		textAreapseudo.setFont(new Font("Monospaced", Font.PLAIN, 20));
		textAreapseudo.setBackground(UIManager.getColor("Button.background"));
		textAreapseudo.setBounds(26, 276, 265, 36);
		frame.getContentPane().add(textAreapseudo);
		
		JTextArea textAreaprenom = new JTextArea();
		textAreaprenom.setFont(new Font("Monospaced", Font.PLAIN, 20));
		textAreaprenom.setBackground(UIManager.getColor("Button.background"));
		textAreaprenom.setBounds(26, 184, 265, 36);
		frame.getContentPane().add(textAreaprenom);
		
		JPasswordField textAreamdp = new JPasswordField();
		textAreamdp.setFont(new Font("Monospaced", Font.PLAIN, 20));
		textAreamdp.setBackground(UIManager.getColor("Button.background"));
		textAreamdp.setBounds(26, 368, 265, 36);
		frame.getContentPane().add(textAreamdp);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(53, 47, 213, 34);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(new CardLayout(0, 0));
		
		JLabel lblNewLabel = new JLabel("Nom ");
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(lblNewLabel, "name_718375387965700");
		
		JPanel panel_1_1 = new JPanel();
		panel_1_1.setBackground(Color.WHITE);
		panel_1_1.setBounds(53, 139, 213, 34);
		frame.getContentPane().add(panel_1_1);
		panel_1_1.setLayout(new CardLayout(0, 0));
		
		JLabel lblPrnom = new JLabel("Pr\u00E9nom ");
		lblPrnom.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrnom.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		panel_1_1.add(lblPrnom, "name_718409832611800");
		
		JPanel panel_1_2 = new JPanel();
		panel_1_2.setBackground(Color.WHITE);
		panel_1_2.setBounds(53, 231, 213, 34);
		frame.getContentPane().add(panel_1_2);
		panel_1_2.setLayout(new CardLayout(0, 0));
		
		JLabel lblPseudo = new JLabel("Pseudo ");
		lblPseudo.setHorizontalAlignment(SwingConstants.CENTER);
		lblPseudo.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		panel_1_2.add(lblPseudo, "name_718411116841400");
		
		JPanel panel_1_2_1 = new JPanel();
		panel_1_2_1.setBackground(Color.WHITE);
		panel_1_2_1.setBounds(53, 323, 213, 34);
		frame.getContentPane().add(panel_1_2_1);
		panel_1_2_1.setLayout(new CardLayout(0, 0));
		
		JLabel lblMotDePasse = new JLabel("Mot de passe ");
		lblMotDePasse.setHorizontalAlignment(SwingConstants.CENTER);
		lblMotDePasse.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		panel_1_2_1.add(lblMotDePasse, "name_718453559522000");
		
		JButton btninscription = new JButton("inscription");
		btninscription.setBackground(Color.WHITE);
		btninscription.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.PLAIN, 17));
		btninscription.setBounds(26, 415, 265, 47);
		frame.getContentPane().add(btninscription);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.BLACK);
		panel_2.setBounds(0, 534, 330, 36);
		frame.getContentPane().add(panel_2);
		
		JButton btnConnexion = new JButton("connexion");
		btnConnexion.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.PLAIN, 17));
		btnConnexion.setBackground(Color.WHITE);
		btnConnexion.setBounds(26, 473, 265, 47);
		frame.getContentPane().add(btnConnexion);
		btnConnexion.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	Connexion_Variablesession window = new Connexion_Variablesession();
				window.frame.setVisible(true);
				frame.dispose();
            }
            });
		btninscription.addActionListener(new ActionListener() {
			
			
			/**
			 * Ici, l'action performed enregistre toutes les informations rentr�es dans les textarea dans des string
			 * petite variation pour le mot de passe car j'utilise un JPasswordfield et non un Jtextfield o� JtextArea
			 * @author Arthur
			 */
            public void actionPerformed(ActionEvent e) {
	        
		         String Nom = textAreanom.getText();
		         String Prenom = textAreaprenom.getText();
		         String Pseudo = textAreapseudo.getText();
		         char[] c = textAreamdp.getPassword();
		         String mdp = new String(c);
		         
		         try { //debut try


		             Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root","root");

		             Statement st =  con.createStatement(); // permet d'assigner une requete sql

		             ResultSet res = st.executeQuery("SELECT pseudo FROM inscription");  //assigne la requete sql a res
		             
		             
		             
		             		while(res.next()) { // parcours tout ta requete sql 
		             		liste.add(res.getString("pseudo"));

		             		
		             };
		             
		     		
		     		// fin try
		 }catch(Exception v) {
		     System.out.println("une erreur");
		 }
		         
		         /**
		     	 * ici le boolean test verifie le fait que le pseudo ne soit pas d�j� pris
		     	 * Si ce n'est pas le cas, l'utilisateur peut s'inscrire et ses informations sont envoy�es a la base de donn�es.
		     	 * Si c'est le cas, le popup erreur s'ouvre, lui indiquant que son nom d'utilisateur est d�j� pris.
		     	 * @author Arthur
		     	 */
		         
		         boolean test = liste.contains(Pseudo);
		         
         		if (test == false) {
		       
		         try {

		        	 //on enregistre les informations de l'inscription dans la bdd
		        	 
		             Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root","root");
		             Connection con2 = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root","root");
		             Statement st =  con.createStatement(); // permet d'assigner une requete sql
		             Statement st2 =  con2.createStatement();
		             
		            String sql = "INSERT INTO `inscription`(`nom`, `prenom`, `pseudo`, `mdp`) VALUES ('"+Nom+"','"+Prenom+"','"+Pseudo+"','"+mdp+"')";  //assigne la requete sql a res
		            st.executeUpdate(sql);
		            String sql2 = "INSERT INTO `gestionportefeuille`(`ajoutargent`, `ajoutdepense`, `Utilisateur`) VALUES ('0','0','"+Pseudo+"')";
		            st2.executeUpdate(sql2);
		            String sql3 = "INSERT INTO `note`(`note`, `utilisateur`) VALUES ('note vide pour test','"+Pseudo+"')";
		            st2.executeUpdate(sql3);
		            
		            Connexion_Variablesession window = new Connexion_Variablesession();
					window.frame.setVisible(true); 
		            frame.dispose();
		             }
		         catch(Exception v) {
		             System.out.println("une erreur"+v);
		         }
         		
            }else {
         			popuperreur window = new popuperreur();
					window.frame.setVisible(true);//ouverture popuperreur
				
				frame.dispose();
				}
		         
		         
		         
	        }});
	}
}
