package InscriptionetConnexion;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import Accueil.Accueil;
import MyClockIT.MyClockDashboard;

import java.awt.Color;

public class popuperreur {

	public static JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					popuperreur window = new popuperreur();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Initialise l'application de popuperrreur
	 * @author Arthur
	 */
	public popuperreur() {
		initialize();
	}

	/**
	 * Initialise les différents elements de l'application popup
	 * @author Arthur
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBackground(Color.WHITE);
		frame.getContentPane().setForeground(Color.WHITE);
		frame.setBounds(100, 100, 410, 218);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setBounds(0, 0, 400, 36);
		frame.getContentPane().add(panel);
		
		JLabel lblNewLabel_2 = new JLabel("ToolBox");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tw Cen MT", Font.PLAIN, 27));
		panel.add(lblNewLabel_2);
		
		JButton btnNewButton = new JButton("Compris !");
		btnNewButton.setBounds(136, 99, 108, 33);
		frame.getContentPane().add(btnNewButton);
		
		JTextArea txtrVotrePseudoEst = new JTextArea();
		txtrVotrePseudoEst.setFont(new Font("Times New Roman", Font.PLAIN, 17));
		txtrVotrePseudoEst.setForeground(Color.BLACK);
		txtrVotrePseudoEst.setText("Votre pseudo est d\u00E9j\u00E0 utilis\u00E9");
		txtrVotrePseudoEst.setBounds(89, 61, 207, 33);
		frame.getContentPane().add(txtrVotrePseudoEst);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.BLACK);
		panel_2.setBounds(0, 143, 400, 36);
		frame.getContentPane().add(panel_2);
		
		/**
		 * l'actionlistener ferme le popup
		 * @author Arthur
		 */
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				popuperreur.frame.setVisible(true);//fermeture popup
				frame.dispose();
				Inscription window = new Inscription();
				window.frame.setVisible(true);
			}
		});
		
		
	}

}