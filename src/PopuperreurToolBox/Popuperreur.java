package PopuperreurToolBox;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class Popuperreur {
	/**
	 * @author Alan et Benjamin
	 * @version 2.0
	 * Launch the application.
	 */
	public JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Popuperreur window = new Popuperreur();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Popuperreur() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.getContentPane().setBackground(Color.BLACK);
		frame.setBounds(100, 100, 572, 100);
		frame.getContentPane().setLayout(null);
		
		JLabel lblerreur = new JLabel("Erreur : merci de v\u00E9rifier les informations");
		lblerreur.setForeground(Color.RED);
		lblerreur.setHorizontalAlignment(SwingConstants.CENTER);
		lblerreur.setFont(new Font("Monospaced", Font.BOLD, 20));
		lblerreur.setBounds(20, 0, 516, 83);
		frame.getContentPane().add(lblerreur);
	}
}
