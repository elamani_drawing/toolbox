package MyClock;

import java.awt.AWTException;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import MyClockIT.ConstanteDashboard;
import NotifyManager.Notification;

public class AlarmeBasique extends Alarme{


	/**
	 * l'alarme basique est c'elle qui se d�clenche une seule fois, puis est supprimer.
	 * @author Said El Amani
	 * @param id est l'id de l'alarme.
	 * @param jour est le jour. Exemple : "Lundi"
	 * @param regulariter est la frequence de l'alarme. Exemple : "Tout les jours"
	 * @param titre est le titre de l'alarme. Exemple "Rendez vous important."
	 * @param heures est l'heure � l'aquelle l'alarme doit sonner
	 * @param minutes est la minute � laquelle l'alarme doit sonner 
	 * @param delayTimeMilliSeconde est le temps separant l'alarme de l'instant ou elle doit s'enclencher. Le temps est en milliseconde
	 *@since 1.0
	 *@version 2.0
	 */
	public AlarmeBasique(int id, String jour , String regulariter,String titre, int heures, int minutes, long delayTimeMilliSeconde) {
		super(id,jour, regulariter, titre, heures, minutes, delayTimeMilliSeconde);
	}
	
	/**
	 * La fonction permet de calculer de calculer le nouveau delay avant que l'alarme ne se redeclenche, 7 jours pour une alarmeSemaine, 24 h pour une alarme basique, et 0 seconde pour une alarmeBasique, elle permet aussi de d�clencher la notification � l'utilisateur au d�clenchement de l'alarme
	 * @author Said El Amani
	 * @since 1.0
	 */
	@Override
	public void recalculTemps() {
		
		//a la fin de lalarme on creer un popup pour dire que lalarme cest terminer
		String message ="Votre alarme :'"+this.getTitre()+ "' prevus pour le "+ this.getDayHour()+" viens de se d�clencher.";

		//on envoie une notification
		try {
			this.ManagerAlarme.MyClockManagerNotification.sendNotification(new Notification("MyClock : Alarme", message));
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JOptionPane.showMessageDialog(this.ManagerAlarme.label,message ,"Une alarme vient de se d�clencher.", JOptionPane.PLAIN_MESSAGE);
		//on supprime lalarme de la bdd
		try {
			this.ManagerAlarme.reqDeleteAlarme(this);
		} catch (SQLException e1) {
			e1.printStackTrace();
		} 
		//on supprime lalarme de la liste
		this.ManagerAlarme.deleteAlarmeBasique(this);
		//on met a jour le visuel
		ConstanteDashboard.panelMesAlarmes.updatePanelMesAlarmes(-1);//on met a jour le visuel de "mes alarmes"
	}
	
}
