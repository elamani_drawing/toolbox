package MyClock;
import java.time.LocalDate;
//time
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import NotifyManager.Notification;
import NotifyManager.NotificationManager;

import java.awt.AWTException;
//bdd
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;

public class AlarmeManager {
	//une liste qui va servir a contenir les alarmes de lutilisateur 
	private ArrayList<Alarme> LesAlarmes = new ArrayList<Alarme>();
	//une liste qui va servir a contenir les alarmes quaura manquer lutilisateur
	private ArrayList<Alarme> alarmeManquer = new ArrayList<Alarme>();
	//une reference au notification manager de myclock pour pouvoir creer des notificatins
	public NotificationManager MyClockManagerNotification = null;//il sera initialiser dans le constructeur
	public JLabel label = null; //un jlabel qui sert de reference pour laffichage des pop up lorsqu'une alarme vien de se declencher

	private int userID;//l'id de lutilisateur
	//les informations necessaire � la connexion dans la base de donnee
	private String url;
	private String username;
	private String password;
	//un object connexion (pour la base de donnee)
	private Connection connection;
	//des references au derniers moment ou l'utilisateur cest connecter, elles sont importantes pour situer l'alarme dans le temps et savoir si l'utilisateur etait connecter lors d'un declenchement d'une alarme, ou s'il a manquer son alarme car il etait deconnecter
	private String derniereConnexionDate="";
	private String derniereConnexioonHeure="";
	
	/**
	 * Un object manager possedant toutes les fonctions necessaire au traitement des alarmes, leur inscription dans la bdd, leur suppression etc.
	 * @param userID est l'id de l'utilisateur connecte.
	 * @param url est l'url mennant vers la base de donn�e
	 * @param username est l'username permettant de se connecter � la base de donn�e.
	 * @param password est le mot de passe permettant de se connecter � la base de donn�e
	 */
	public AlarmeManager(int userID, String url, String username, String password) {

		this.userID = userID;
		this.url = url;
		this.username =username;
		this.password = password;
		//on initialise lapplication que si lid est positif, par defaut lapplication est lancer avec lid de lutilisateur qui est -1
		if(userID > -1) {
			this.reStart();//on relance lapplication
		}
		

	}
	
	
	/**
	 * La fonction permet de relancer le manager, elle reparametre les notifications et reverifie/informe l'utilisateur des alarmes quil a manquer durant son absence.
	 * @author Said El Amani
	 * @version 2.0
	 */
	public void reStart() {
		this.MyClockManagerNotification =  MyClockIT.ConstanteDashboard.MyClockManagerNotification ;
		//on creer un object de connexion et un statement pour executer nos requetes
		try {
			this.connection = DriverManager.getConnection(this.url+"?serverTimezone=UTC", this.username, this.password);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		//on initialise lapplication, on verifie si lutilisateur a rater de salarmes durant sa deconexion
		try {
			this.initialisation();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * La fonction permet d'initialiser le manager, elle verifie et informe l'utilisateur de s'il � manquer des alarmes , elle met � jour la derniere connexion de l'utilisateur 
	 * @author Said El Amani
	 * @since 1.0
	 */
	private void initialisation() throws SQLException {
		//on reenitialise les listes d'alarme
		this.LesAlarmes = new ArrayList<Alarme>();
		this.alarmeManquer = new ArrayList<Alarme>();
		//on verifie sil y a des alarmes, et s'ils ont �t� manquer ou non
		this.verificationManquerQuotidien();//alarme quotidienne (tout les 24h)
		this.verificationManquerBasique();//alarme basique (une seule fois)
		this.verificationManquerSemaine();//alarmes semaine (toutes les semaines)
			
		//on recupere le moment actuel
        //je reccupere la date local
		LocalDateTime myDateObj = LocalDateTime.now();
	    //je reccupere la valeur de lheure et de la date
		String instantMaintenantDate = myDateObj.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	    String instantMaintenantHeure = myDateObj.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
		//on change la date de derniere connexion de lutilisateur
		//on actualise le temps de la derniere connexion de lutilisateur 
		this.setDerniereConnexionDate(instantMaintenantDate);
		this.setDerniereConnexioonHeure(instantMaintenantHeure);
		this.reqInsertConnexionUser();
		//on verifie si l'utilisateur � des alarmes manquer, si oui on le lui dit
		this.showAlarmeManquer();
		
	}
	/**
	 * La fonction permet de reccuperer l'id de l'utilisateur
	 * @author Said El Amani
	 * @since 1.0
	 * @return retourne un int representant l'id de l'utilisateur
	 */
	public int getUserID() {
		return userID;
	}
	/**
	 * La fonction permet de modifier l'id de l'utilisateur
	 * @author Said El Amani
	 * @since 1.0
	 * @param userID est un int representant l'id de l'utillisateur
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}
	/**
	 * La fonction permet de reccuperer les alarmes de l'utilisateur
	 * @author Said El Amani
	 * @since 1.0
	 * @return retourne une ArrayList contenant les alarmes de l'utilisateur
	 */
	public ArrayList<Alarme> getLesAlarmes() {
		return LesAlarmes;
	}
	/**
	 * La fonction permet de modifier l'ArrayList contenant les alarmes de l'utilisateur
	 * @author Said El Amani
	 * @since 1.0
	 * @param lesAlarmes est une ArrayList d'alarme.
	 */
	public void setLesAlarmes(ArrayList<Alarme> lesAlarmes) {
		LesAlarmes = lesAlarmes;
	}
	
	/**
	 * La fonction permet de reccuperrer l'object Connexion fesant la liaison avec la BDD
	 * @author Said El Amani
	 * @since 2.0
	 * @return l'object connexion
	 */
	private Connection getConnection() {
		return connection;
	}

	/**
	 * La fonction permet de modifier l'object connection permettant d'echanger avec la bdd
	 * @author Said El Amani
	 * @since 2.0
	 * @param connection est un object Connection
	 */
	private void setConnection(Connection connection) {
		this.connection = connection;
	}

	/**
	 * La fonction permet de reccuperer un object Statement pour realiser une requete
	 * @author Said El Amani
	 * @since 2.0
	 */
	private Statement getStatement() throws SQLException {
		return this.connection.createStatement();
	}


	/**
	 * La fonction permet de reccuperer la date de la derniere connexion de l'utilisateur.
	 * @author Said El Amani
	 * @since 2.0
	 * @return Un string representant la date de la derniere connexion de l'utilisateur. C'est une date de la forme "yyyy-MM-dd"
	 */
	public String getDerniereConnexionDate() {
		return derniereConnexionDate;
	}
	

	/**
	 * La fonction permet parametrer la date de la derniere connexion de l'utilisateur
	 * @author Said El Amani
	 * @since 2.0
	 * @param derniereConnexionDate est un string de la forme "yyyy-MM-ss"
	 */
	public void setDerniereConnexionDate(String derniereConnexionDate) {
		this.derniereConnexionDate = derniereConnexionDate;
	}
	/**
	 * La fonction permet de reccupperer l'heure de la derniere connexion de l'utilisateur
	 * @author Said El Amani
	 * @since 2.0
	 * @return retourne un String representant la date de la derniere connexion de l'utilisateur
	 */
	public String getDerniereConnexioonHeure() {
		return derniereConnexioonHeure;
	}
	/**
	 * La fonction permet de modifier l'heure de la derniere connexion de l'utilisateur
	 * @author Said El Amani
	 * @since 2.0
	 * @param  derniereConnexioonHeure est un string de la forme "HH:mm:ss"
	 */
	public void setDerniereConnexioonHeure(String derniereConnexioonHeure) {
		this.derniereConnexioonHeure = derniereConnexioonHeure;
	}
	
	/**
	 * La fonction permet de reccupperer la liste des alarmes manquer de l'utilisateur 
	 * @author Said El Amani
	 * @since 2.0
	 * @return retourne une ArrayList contenant les alarmes que l'utilisateur � manquer
	 */
	public ArrayList getAlarmeManquer() {
		return alarmeManquer;
	}
	/**
	 * La fonction permet de modifier la liste contenant les alarmes manquer de l'utilisateur
	 * @author Said El Amani
	 * @since 2.0
	 * @param  alarmeManquer est une Arraylist contenant des alarmes
	 */
	public void setAlarmeManquer(ArrayList alarmeManquer) {
		this.alarmeManquer = alarmeManquer;
	}
	
	/**
	 * La fonction permet d'ajouter une alarme � la liste des alarmes de l'utilisateur
	 * @author Said El Amani
	 * @since 2.0
	 * @param  alarme est un object Alarme
	 */
	public void addAlarme(Alarme alarme) {
		//jajoute l'alarme
		this.LesAlarmes.add(alarme);
	}
	
	/**
	 * La fonction permet d'ajouter une alarme a la liste des alarmes manquer
	 * @author Said El Amani
	 * @since 2.0
	 * @param  alarme est un object de type Alarme 
	 */
	public void addAlarmeManquer(Alarme alarme) {
		//jajoute l'alarme
		this.alarmeManquer.add(alarme);
	}
	
	/**
	 * La fonction permet si titre est vide
	 * @author Said El Amani
	 * @since 2.0
	 * @param  titre est un String 
	 * @return un boolean indiquant si le titre est vide ou non, si c'est vide ca renerra true
	 */
	private boolean titreIsVide(String titre){
		for(char lettre : titre.toCharArray()) {
			if (lettre !=' ' ) {
				return false;
				//si on trouve une lettre on arrete la boucle et en retourne false
			}
		}
		//si on a fini la boucle cest que titre est vide 
		return true;
	}

	/**
	 * La fonction permet de reccuperer l'id du jour de demain 
	 * @author Said El Amani
	 * @since 2.0
	 * @param idJour est un int representant l'id du jour d'aujourdhui, les id vont de 1 � 7 
	 * @return un int representant l'id du jour de demain dans la semaine
	 */
	private int getIdDemain(int idjour) {
		idjour+=1;
		//si on est samedi (7) et que demain on est dimanche( 1) au lieu davoir 8 on a 1
		if(idjour >7) {
			return 1;
		}
		return idjour;
	}	
	
	/**
	 * La fonction permet de reccuperer le jour de la semaine
	 * @author Said El Amani
	 * @since 2.0
	 * @param date est un object LocalDate
	 * @return un int representant l'id du jour de la semaine 
	 */
	private int getIdDayWeek(LocalDate date) {
		int idJour = date.getDayOfWeek().getValue();//ici idJour est egal � 1 pour Lundi, mais dans notre liste lundi est egal � 2, donc
		return this.getIdDemain(idJour); //on retourne la valeur de demain qui serait 2
	}
	/**
	 * La fonction permet de reccuperrer l'id du jour d'hier
	 * @author Said El Amani
	 * @since 2.0
	 * @param  idjour est un int 
	 * @return un int representant le jour d'hier
	 */
	private int getIdHier(int idjour) {
		idjour-=1;
		//si on est Dimance (1) et que hier on etait samedi(7) au lieu davoir 0 on a 7
		if(idjour <1) {
			return 7;
		}
		return idjour;
	}
	
	/**
	 * La fonction permet de reccupperer le numero de la semaine prochaine dans l'annee
	 * @author Said El Amani
	 * @since 2.0
	 * @param idSemaine est un int compris entre 1 et 52
	 * @return un int representant le numero de la semaine prochaine 
	 */
	private int getIdNextSemaine(int idSemaine) {
		idSemaine+=1;
		//il ya que 52 semaine par ans, si lid est plus cest quon passe a une nouvelle ann�e
		if(idSemaine>52) {
			return 1;
		}
		return idSemaine;
	}

	/**
	 * La fonction permet de reccuperrer les alarmes basiques de l'utilisateur connecter dans la bdd
	 * @author Said El Amani
	 * @since 2.0
	 * @return un ResulSet contenant les alarmes basiques de l'utilisateur
	 */
	private ResultSet reqGetAlarmesBasique() throws SQLException {
		String req = "SELECT * FROM `Alarmes` WHERE `id_user` ="+ this.getUserID()+" AND `regularite`= 'Une seule fois'";
		
		ResultSet data = this.getStatement().executeQuery(req);
		return data;
	}
	
	/**
	 * La fonction permet de reccuperrer les alarmes quotidiennes de l'utilisateur connecter dans la bdd
	 * @author Said El Amani
	 * @since 2.0
	 * @return un ResulSet contenant les alarmes quotidiennes de l'utilisateur
	 */
	private ResultSet reqGetAlarmesQuotidient() throws SQLException {
		String req = "SELECT * FROM `Alarmes` WHERE `id_user` ="+ this.getUserID()+" AND `regularite`= 'Tout les jours'";
		
		ResultSet data = this.getStatement().executeQuery(req);
		return data;
	}
	/**
	 * La fonction permet de reccuperrer les alarmes semaines de l'utilisateur connecter dans la bdd
	 * @author Said El Amani
	 * @since 2.0
	 * @return un ResulSet contenant les alarmes semaines de l'utilisateur
	 */
	private ResultSet reqGetAlarmesSemaine() throws SQLException {
		String req = "SELECT * FROM `Alarmes` WHERE `id_user` ="+ this.getUserID()+" AND `regularite`= 'Toutes les semaines'";
		
		ResultSet data = this.getStatement().executeQuery(req);
		return data;
	}
	/**
	 * La fonction permet de reccuperrer l'id de la derniere alarmes qu'un utilisateur � creer
	 * @author Said El Amani
	 * @since 2.0
	 * @throws java.sql.SQLException
	 * @return un int etant l'id de la derniere alarme que l'utilisateur connecter � ajouter.
	 */
	public int reqGetIdLastAlarm() throws SQLException {
		String req ="SELECT `id_alarme` FROM `Alarmes` WHERE `id_user`="+this.getUserID()+" ORDER BY `id_alarme` Desc limit 1;";
		
		ResultSet data = this.getStatement().executeQuery(req);
		int id=0;
		while(data.next()) {
			//on reccupere le derniere id
			id=data.getInt("id_alarme");
		}
		
		return id;
	}
	
	/**
	 * La fonction permet de supprimer une alarme dans la bdd
	 * @author Said El Amani
	 * @since 2.0
	 * @param alarme est l'objec alarme � supprimer
	 * @throws java.sql.SQLException
	 */
	public void reqDeleteAlarme(Alarme alarme) throws SQLException {
		String req = "DELETE FROM `alarmes` WHERE `id_alarme`= "+alarme.getIdAlarme()+" AND `id_user`= "+this.getUserID();
		//jexecute le requete
		this.getStatement().executeUpdate(req);
	}
	/**
	 * La fonction permet de reccuperrer la date et l'heure de la derniere connexion de l'utilisateur connecter
	 * @author Said El Amani
	 * @since 2.0
	 */
	private void reqGetLastConnexionUser(){
		String req="SELECT * FROM `userLastConnexion` WHERE `id_user` ="+this.getUserID()+" ORDER BY `id_user` Desc limit 1;";
		
		ResultSet data;
		try {
			//execute la requete
			data = this.getStatement().executeQuery(req);
			while(data.next()) {
				//on configure l'attribut this.dernierConnexionDate et this.derniereConnexionHeure
				this.setDerniereConnexionDate(data.getString("dateLastConnexion"));
				this.setDerniereConnexioonHeure(data.getString("heureLastConnexion"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * La fonction permet de mettre � jour la derniere connexion de l'utilisateur
	 * @author Said El Amani
	 * @since 2.0
	 */
	private void reqInsertConnexionUser(){
		//si les valeurs ne sont pas vide on met a jour les donn�es, sinon on ne fait rien
		if(this.getDerniereConnexionDate() !="" &&  this.getDerniereConnexioonHeure() != "") {
			String reqDelete="DELETE FROM `userLastConnexion` WHERE `id_user`="+this.getUserID();//on supprime lanciene sauvegarde, sil yen avait pas ca ne va rien supprimer
			String reqInsert="INSERT INTO `userLastConnexion`(`id_user`, `dateLastConnexion`,`heureLastConnexion`) VALUES ('"+this.getUserID()+"', '"+this.getDerniereConnexionDate()+"', '"+this.getDerniereConnexioonHeure()+"' )";
			try {
				//execution des requetes
				//je supprime lancienne ligne
				this.getStatement().executeUpdate(reqDelete);
				//je met � jour la nouvelle
				this.getStatement().executeUpdate(reqInsert);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	/**
	 * La fonction permet d'ajouter une alarme dans la base de donn�e
	 * @author Said El Amani
	 * @since 2.0
	 * @param alarme est l'object alarme a ajotuer dans la bdd
	 * @throws java.sql.SQLException
	 */
	public void reqAddAlarme(Alarme alarme) throws SQLException {
		String req = "INSERT INTO `alarmes`(`id_alarme`, `id_user`, `jour`, `regularite`, `heures`, `minutes`, `titre`, `dateCreation`, `heureCreation`, `numeroJourRealisation`) VALUES (NULL, "+this.getUserID()+", '"+alarme.getJour()+"', '"+alarme.getRegularite()+"', "+alarme.getHeures()+", "+alarme.getMinutes()+", '"+alarme.getTitre()+"' , '"+alarme.getAlarmeDateCreation()+"', '"+alarme.getAlarmeHeureCreation()+"', '"+alarme.getAlarmeNbrJourRealisation()+"')";	
		//jexecute le requete
		this.getStatement().executeUpdate(req);
	}	

	/**
	 * La fonction permet de mettre � jour une alarme dans la base de donn�e
	 * @author Said El Amani
	 * @since 2.0
	 * @param alarme est l'alarme a mettre � jour.
	 */
	public void reqUpdateAlarme(Alarme alarme) {
		String req ="UPDATE `alarmes` SET `jour`= '"+alarme.getJour()+"', `regularite`= '"+alarme.getRegularite()+"', `heures`= "+alarme.getHeures()+", `minutes`= "+alarme.getMinutes()+",`titre`= '"+alarme.getTitre()+"',`dateCreation`= '"+alarme.getAlarmeDateCreation()+"',`heureCreation`= '"+alarme.getAlarmeHeureCreation()+"',`numeroJourRealisation`= '"+alarme.getAlarmeNbrJourRealisation()+"'  WHERE `id_alarme` = "+alarme.getIdAlarme()+" AND `id_user`="+this.getUserID();
		//jexecute le requete
		try {
			this.getStatement().executeUpdate(req);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * La fonction permet de savoir si une ann�e est bissextile ou non
	 * @author Said El Amani
	 * @since 2.0
	 * @return un boolean, True si l'ann�e est bissextile et False si ce n'est pas le cas
	 */
	private boolean yearIsBisextile(int annee) {
		if(annee % 4 ==0 && annee % 100 != 0){
			   return true;
			}else if(annee % 400 == 0){
			   return true;
			}else {
			 return false;
		}
	}
	
	
	/**
	 * La fonction permet de savoir si 2 jours sont dans une intervalle de 7 jours
	 * @author Said El Amani
	 * @since 2.0
	 * @param jour1 est un int compris entre 1 et 365/366
	 * @param jour2 est un int compris entre 1 et 365/366
	 * @return un boolean, true si cest la meme semaine et false si ce nest pas le cas
	 */
	private boolean isMemeSemaine(int jour1, int jour2) {
		int nbrJourAnnee= 365;
		int annee = 2021;
		if (this.yearIsBisextile(annee)==true){
			nbrJourAnnee = 366;
		}
		if(jour1+7 <=nbrJourAnnee ) {
			//on est sur la meme annee
			return (jour1<=jour2 && jour2<=nbrJourAnnee);
		}else {
			//on est dans la meme annee ou lannee dapres
			return ((jour1<=jour2 && jour2<=nbrJourAnnee) || (0< jour2&& jour2<= (jour1+7)-nbrJourAnnee)); 
		}
		
	}
	/**
	 * La fonction permet de verfier si 2 jours sont les memes
	 * @author Said El Amani
	 * @since 2.0
	 * @param jour1 et jour2 sont des int
	 * @return un boolean, true si jour1 est egal � jour2 et false sinon
	 */
	private boolean isMemeJour(int jour1, int jour2) {
		return jour1==jour2;
	}
	/**
	 * La fonction permet de savoir si jour2 est le lendemain de jour1
	 * @author Said El Amani
	 * @since 2.0
	 * @return un boolean, true si cest bien le lendemain sinon false
	 */
	private boolean isDemain(int jour1, int jour2) {
		return (jour1 == jour2-1);
	}
	
	/**
	 * La fonction permet de savoir si jour 2 est la veille (hier) de jour1
	 * @author Said El Amani
	 * @since 2.0
	 * @return un boolean, true si cest la veille, sinon false
	 */
	private boolean isHier(int jour1, int jour2) {
		return (jour1 == jour2+1);
	}
	/**
	 * La fonction permet d'afficher � l'utilisateur les alarmes qu'il � manquer et de lui envoyer la notification correspondante
	 * @author Said El Amani
	 * @since 2.0
	 */
	private void showAlarmeManquer(){
		//si l'atrray nest pas vide
		if(this.getAlarmeManquer().size()>0) {
			//jenvoie une notification
			try {
				this.MyClockManagerNotification.sendNotification(new Notification("MyClock : Alarme", "Vous avez rat� une ou plusieures Alarme durant votre absence"));
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String message ="";
			LocalDate dateAlarmeManquer;//sert a reccuperer la date ou lalarme etait prevu
			LocalDate dateCreationAlarme;//sert a sauvegarder lannee ou lalarme a ete creer
			for (Alarme alarme : this.alarmeManquer) {
				//je reccupere la date de creation de lalarme car jai besoin de lann�e
				dateCreationAlarme = LocalDate.parse(alarme.getAlarmeDateCreation());
				//grace a lannee, et le numero de jour de lannee ou devait se realiser lalarme, je peux reccuperer la date
				dateAlarmeManquer = Year.of(dateCreationAlarme.getYear()).atDay(alarme.getAlarmeNbrJourRealisation()) ;
				//on creer notre message en reccuperant les informations des alarmes
				message+="("+dateAlarmeManquer.toString()+"): '"+alarme.getTitre()+"', prevu le "+alarme.getDayHour()+". \n";
			}
			//on r�enitialise la liste des alarmes manquer
			this.setAlarmeManquer(new ArrayList<Alarme>());
			//jaffiche � l'ecran les alarmes manquer
			JOptionPane.showMessageDialog(this.label,message ,"Voici les alarmes que vous avez rat� durant votre absence : ", JOptionPane.PLAIN_MESSAGE);
			
		}
	}
	
	/**
	 * La fonction permet de verifier si une alarme basique � ete manquer ou non durant la deconnexion de l'utilisateur
	 * @author Said El Amani
	 * @since 2.0
	 * @param id est un int, c'est l'id de l'alarme
	 * @param alarmeDateCreation est un String de la forme "yyyy-MM-dd" qui est la date � laquelle lalarme � ete creer
	 * @param alarmeTimeCreation est un string de la forme "HH:mm:ss" qui est l'heure � laquelle � ete creer l'alarme
	 * @param a_nbrJourYearRealisation est un int representant le jour de l'ann�e ou doit se realiser lalarme, un chiffre entre 1 et 365/366
	 * @param a_jour est String, c'est le jour ou doit se r�aliser l'alarme
	 * @param a_regularite est un string, c'est la frequence de l'alarme
	 * @param a_heure est un int, c'est l'heure ou doit se realiser l'alarme
	 * @param a_minute est un int, cest la minute ou doit se realiser l'alarme
	 * @param a_titre est un string, cest le titre de l'alarme
	 */
	//verifie si lalarme c'est effectuer durant labsence de lutilisateur
	private void alarmeManquerBasique(int id, String alarmeDateCreation, String alarmeTimeCreation, int a_nbrJourYearRealisation, String a_jour, String a_regularite,int a_heure,int a_minute,String a_titre) {
		boolean rater = true;//par defaut on a rater lheure
		//on recupere le moment
        //je reccupere la date local
		LocalDateTime myDateObj = LocalDateTime.now();
	    //je reccupere la valeur de lheure et de la date
		String instantMaintenantDate = myDateObj.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		LocalDate dateMaintenant = LocalDate.parse(instantMaintenantDate);
	    LocalDate dateCreation = LocalDate.parse(alarmeDateCreation);
	    
	    int m_nbrJourYear = dateMaintenant.getDayOfYear(); 
	    int a_nbrJourYearCreer = dateCreation.getDayOfYear();//le jour ou lalarme � ete creer, une valeur entre 1 et 365-366
	    
		Alarme alarme=null;
		//si la regualite est une seul fois
		//si cest le meme jour ou la semaine � laquelle on a creer lalarme 
		if((this.isMemeJour(m_nbrJourYear, a_nbrJourYearCreer)==true) || (this.isMemeSemaine(a_nbrJourYearCreer, a_nbrJourYearRealisation))) {
			//on peux verifier les heures mais on va essayer de creer lalarme, sil y a une erreur cest que l'alarme est dans le passer, donc lalarme � ete rater
			try {
				alarme= this.creerAlarmeBasique(a_jour, a_regularite, a_heure, a_minute, a_titre, true);
				//sil ny a pas eu derreur, cest que lalarme na pas ete manquer
				alarme.setIdAlarme(id);//pon lui remet son id
				//lalarmee na pas ete manquer on lajoute dans la liste des alarmes
				this.getLesAlarmes().add(alarme);
			} catch (ClockException e) {
				//lalarme � ete manquer, on la recreer pour la mettre dans les alarmes manquer
				try {
					//on creer lalarme, on utilise dirrectement le constructeur
					alarme= new AlarmeBasique(id, a_jour, a_regularite, a_titre, a_heure, a_minute, 0);
					alarme.stopAlarm();//on larrete parcequelle ne doit pas se realiser

					this.addAlarmeManquer(alarme);//on ajoute lalarme dans nos alarmes manquer
					alarme.setIdAlarme(id);//on lui remet son id
					alarme.setAlarmeDateCreation(alarmeDateCreation);//sa date de creation
					alarme.setAlarmeHeureCreation(alarmeTimeCreation);//son heure de creation
					alarme.setAlarmeNbrJourRealisation(a_nbrJourYearRealisation);//son jour de realisation
					this.reqDeleteAlarme(alarme);//on supprime lalarme de la bdd
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
		}else {
			//lalarme a etait creer depuis plus dune semaine donc elle a ete manquer
			try {
				//on essaye de creer l'alarme
				alarme= new AlarmeBasique(id, a_jour, a_regularite, a_titre, a_heure, a_minute, 0);
				alarme.stopAlarm();//jarrete lalarme
				this.addAlarmeManquer(alarme);//on ajoute lalarme dans nos alarmes manquer
				alarme.setIdAlarme(id);//on lui remet son id
				alarme.setAlarmeDateCreation(alarmeDateCreation);//sa date de creation
				alarme.setAlarmeHeureCreation(alarmeTimeCreation);//son heure de creation
				alarme.setAlarmeNbrJourRealisation(a_nbrJourYearRealisation);//son jour de realisation
				this.reqDeleteAlarme(alarme);//on supprime lalarme de la bdd
				
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}		
		
	}	
	
	/**
	 * La fonction permet de verifier si une alarme quotidienne � ete manquer ou non durant la deconnexion de l'utilisateur
	 * @author Said El Amani
	 * @since 2.0
	 * @param id est un int, c'est l'id de l'alarme
	 * @param alarmeDateCreation est un String de la forme "yyyy-MM-dd" qui est la date � laquelle lalarme � ete creer
	 * @param alarmeTimeCreation est un string de la forme "HH:mm:ss" qui est l'heure � laquelle � ete creer l'alarme
	 * @param a_nbrJourYearRealisation est un int representant le jour de l'ann�e ou doit se realiser lalarme, un chiffre entre 1 et 365/366
	 * @param a_jour est String, c'est le jour ou doit se r�aliser l'alarme
	 * @param a_regularite est un string, c'est la frequence de l'alarme
	 * @param a_heure est un int, c'est l'heure ou doit se realiser l'alarme
	 * @param a_minute est un int, cest la minute ou doit se realiser l'alarme
	 * @param a_titre est un string, cest le titre de l'alarme
	 */
	private void alarmeManquerQuotidien(int id, String alarmeDateCreation, String alarmeTimeCreation,int a_nbrJourYearRealisation, String a_jour, String a_regularite,int a_heure,int a_minute,String a_titre) {
		boolean rater = true;//par defaut on a rater lheure
		//on recupere le moment
        //je reccupere la date local
		LocalDateTime myDateObj = LocalDateTime.now();
	    //je reccupere la valeur de lheure et de la date
		String instantMaintenantDate = myDateObj.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	    String instantMaintenantHeure = myDateObj.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
	    //de la je demande un object date et temps
		LocalDate dateMaintenant = LocalDate.parse(instantMaintenantDate);
		Time dateTimeMaintenant = Time.valueOf(instantMaintenantHeure);
		//je reccupere la date et lheure de la derniere connexion de l'utilisateur
		Time dateTimeConnexion = Time.valueOf(this.getDerniereConnexioonHeure());
		LocalDate dateConnexion = LocalDate.parse(this.getDerniereConnexionDate());
		Time dateTimeCreation = Time.valueOf(alarmeTimeCreation);//lheure et la minute a laquelle lalarme a ete creeer
		LocalDate dateCreation = LocalDate.parse(alarmeDateCreation);//la date a laquelle lalarme a ete creer
	    
	    int a_idJourSemaineCreation = this.getIdDayWeek(dateCreation);//une valeur entre 1 et 7
	    int a_heureCreation = dateTimeCreation.getHours();;
	    int a_minuteCreation =  dateTimeCreation.getMinutes();;
	    
	    //m_les instant de maintenant
	    int m_nbrJourYear = dateMaintenant.getDayOfYear(); //1-365
		int m_idJourSemaine= this.getIdDayWeek(dateMaintenant); //1-7
		int m_heure = dateTimeMaintenant.getHours();
		int m_minute= dateTimeMaintenant.getMinutes();
		
		//c_ est les instant de la derniere conenxion de lutilisateur
	    int c_nbrJourYear = dateConnexion.getDayOfYear(); //1-365, le dernier jour ou lutilisateur cest connecter
		int c_heure = dateTimeConnexion.getHours();;
		int c_minute = dateTimeConnexion.getMinutes();;
		

		//si on est le dernier jour ou lutilisateur cest connecter, lalarme avait deja etait checker avant, donc il ne peut pas etre realiser, ou bien elle cest realiser et elle a ete relancer, dans tout les cas on la deja traiter si elle a ete manquer
		if(this.isMemeJour(m_nbrJourYear, c_nbrJourYear)) {
			//on est le jour ou lalarme � ete lancer 
			//on a juste besoin de verifier lheure et les minutes, si lheure et les minutes sont pas encore depasser ou ils sont depasser mais quon a creer lalarme aujourdhui cest que lalarme na pas ete rater
			if((m_heure < a_heure) || ((m_heure == a_heure && m_minute <a_minute) || (a_idJourSemaineCreation == m_idJourSemaine )))  {
				//si lheure nest pas encore passer, lalarme sonnera dans le futur
				rater =false;
			}
			//sinon cest quon a rater l'alarme
			
		}else if(this.isHier(m_nbrJourYear ,c_nbrJourYear)) {
			//le dernier jour de connexion etait hier
			//lalarme � ete creer apres la connexion de lutilisateur cest que il etait la lorsque lalarme � ete redemarrer
			if((c_heure < a_heureCreation) || (c_heure == a_heureCreation && c_minute <=a_minuteCreation )) {
				//on verifie si aujourdhui lheure de lalarme est passer ou pas encore
				if((m_heure <a_heure) ||  (m_heure == a_heure && m_minute <a_minute)){
					rater =false;
				}
			}
		}
		//on peut creer l'alarme 
		Alarme alarme = this.creerAlarmeQuotidien(a_jour, a_regularite, a_heure, a_minute, a_titre, true);
		//on redonne les information necessaire au bon fonctionnement de l'alarme
		alarme.setIdAlarme(id);//on lui remet son id
		alarme.setAlarmeDateCreation(alarmeDateCreation);
		alarme.setAlarmeHeureCreation(alarmeTimeCreation);
		alarme.setAlarmeNbrJourRealisation(a_nbrJourYearRealisation);
		if(rater ==false){
			//lalarmee na pas ete manquer on lajoute dans la liste des alarmes
			this.addAlarme(alarme);
		}else {
			//lalarme a ete manquer
			this.addAlarmeManquer(alarme);
			alarme.setAlarmeDateCreation(dateMaintenant.toString());//on change la date de creation
			alarme.setAlarmeHeureCreation(dateTimeMaintenant.toString());//on change lheure de creation
			//on met a jour lalarme dans la bdd
			this.reqUpdateAlarme(alarme);
			//on l'ajoute dans la liste des alarmes puisquelle doit ressonner plutard ou demain
			this.addAlarme(alarme);
		}
		
	}	
	
	/**
	 * La fonction permet de verifier si une alarme semaine � ete manquer ou non durant la deconnexion de l'utilisateur
	 * @author Said El Amani
	 * @since 2.0
	 * @param id est un int, c'est l'id de l'alarme
	 * @param alarmeDateCreation est un String de la forme "yyyy-MM-dd" qui est la date � laquelle lalarme � ete creer
	 * @param alarmeTimeCreation est un string de la forme "HH:mm:ss" qui est l'heure � laquelle � ete creer l'alarme
	 * @param a_nbrJourYearRealisation est un int representant le jour de l'ann�e ou doit se realiser lalarme, un chiffre entre 1 et 365/366
	 * @param a_jour est String, c'est le jour ou doit se r�aliser l'alarme
	 * @param a_regularite est un string, c'est la frequence de l'alarme
	 * @param a_heure est un int, c'est l'heure ou doit se realiser l'alarme
	 * @param a_minute est un int, cest la minute ou doit se realiser l'alarme
	 * @param a_titre est un string, cest le titre de l'alarme
	 */
	private void alarmeManquerSemaine(int id, String alarmeDateCreation, String alarmeTimeCreation, int a_nbrJourYearRealisation, String a_jour, String a_regularite,int a_heure,int a_minute,String a_titre) {

		boolean rater = true;//par defaut on a rater lheure
		//on recupere le moment
        //je reccupere la date local
		LocalDateTime myDateObj = LocalDateTime.now();
	    //je reccupere la valeur de lheure et de la date
		String instantMaintenantDate = myDateObj.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	    String instantMaintenantHeure = myDateObj.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
	    //de la je demande un object date et temps
		LocalDate dateMaintenant = LocalDate.parse(instantMaintenantDate);
		Time dateTimeMaintenant = Time.valueOf(instantMaintenantHeure);
		
		//je reccupere la date et lheure de la derniere connexion de l'utilisateur
		Time dateTimeConnexion = Time.valueOf(this.getDerniereConnexioonHeure());
		LocalDate dateConnexion = LocalDate.parse(this.getDerniereConnexionDate());
		//je reccupere la date et lheure a laquelle lalarme a ete creer
		Time dateTimeCreation = Time.valueOf(alarmeTimeCreation);
		LocalDate dateCreation = LocalDate.parse(alarmeDateCreation);

		//int a_nbrJourYearRealisation = 1;//le jour de lannee ou lalarme doit se realiser, 1-365
	    int a_nbrJourYearCreer = dateCreation.getDayOfYear(); //le jour ou lalare � ete creer , 1-365
	    //lheure et la minute a laquelle lalarme a ete creer
	    int a_heureCreation = dateTimeCreation.getHours();;
	    int a_minuteCreation =  dateTimeCreation.getMinutes();;
	    
	    //m_ sont les instant de maintenant
	    int m_nbrJourYear = dateMaintenant.getDayOfYear(); //1-365
		int m_heure = dateTimeMaintenant.getHours();
		int m_minute= dateTimeMaintenant.getMinutes();

		//c_ est les instant de la derniere conenxion de lutilisateur
	    int c_nbrJourYear = dateConnexion.getDayOfYear(); //1-365, le dernier jour ou lutilisateur cest connecter
		int c_heure = dateTimeConnexion.getHours();;
		int c_minute = dateTimeConnexion.getMinutes();;

		//si la regualite est une seul fois
		
		//si lutilisateur cest deja connecter cette semaine cest que soit il avait manquer lalarme on lui a deja dit, soit il ne la pas manquer
		if(this.isMemeSemaine(c_nbrJourYear, m_nbrJourYear)) {
			System.out.println("meme semaine");
			//il cest connecter cette semaine
			if(this.isMemeJour(c_nbrJourYear,  a_nbrJourYearRealisation)) {//sa derniere connexion etait le jour ou lalarme devait se realiser
				if((c_heure < a_heureCreation) || (c_heure== a_heureCreation && c_minute <= a_minuteCreation)){
					//on rentre ici si lutilisateur cest connecter avant la creation de l'alarme, donc il etait la lorsque lalarme cest realiser
					rater=false;

				}
				
			}else if(this.isMemeJour(a_nbrJourYearRealisation, m_nbrJourYear)) {//si on est le jour ou lalarme doit senclencher
				if((m_heure < a_heure) || (m_heure== a_heure && m_minute <= a_minute)){
					//on rentre ici si lheure de lalarme nest pas encore passer
					rater=false;
				}
			}else if((c_nbrJourYear < a_nbrJourYearRealisation)) {
				//si  le jour etait apres qu'il se soit connecter on a pas rater lalarme
				rater=false;

			}
			
			//sinon cest quon a rater l'alarme
			
		}else if(this.isMemeSemaine(m_nbrJourYear, c_nbrJourYear+7)) {
			//il cest connecter la semaine derniere
			if(this.isMemeJour(a_nbrJourYearRealisation, m_nbrJourYear)) {
				//si cest aujourdhui que lalarme doit s'enclencer
				//on verifie lheure
				if((m_heure < a_heure) || (m_heure== a_heure && m_minute <= a_minute)){
					//on rentre ici si lheure de lalarme nest pas encore passer
					rater = false;
				}
			}else if(m_nbrJourYear < a_nbrJourYearRealisation) {
				//si le jour ou doit senclencher lalarme est dans le futur
				rater= false;
			}
		}

		//on peut creer l'alarme, si l'alarme na pas ete rater, cet object sera ajouter dans la liste des alarmes, sinon elle sera ajouter dans les alarmes manquer et cest alarmes2 qui sera ajouter dans la liste des alarmes
		Alarme alarme = this.creerAlarmeSemaine(a_jour, a_regularite, a_heure, a_minute, a_titre, true);
		//on creer une alarme qui va servir de nouvelle alarme, elle aura les modifications que va subbir l'alarme(le jour ou elle a ete creer, le jour de l'ann�e ou elle doit se declencher etc. )
		Alarme alarme2 = this.creerAlarmeSemaine(a_jour, a_regularite, a_heure, a_minute, a_titre, false);
		
		//on redonne les information necessaire au bon fonctionnement de l'alarme
		alarme.setIdAlarme(id);//on lui remet son id
		alarme2.setIdAlarme(id);
		alarme.setAlarmeDateCreation(alarmeDateCreation);
		alarme.setAlarmeHeureCreation(alarmeTimeCreation);
		alarme.setAlarmeNbrJourRealisation(a_nbrJourYearRealisation);
		
		if(rater ==false){
			//lalarmee na pas ete manquer on lajoute dans la liste des alarmes, aucune modification ne lui ai apporter
			this.addAlarme(alarme);
		}else {
			//lalarme a ete manquer
			this.addAlarmeManquer(alarme);//on ajoute lalarme dans la liste des elements manquer
			alarme.stopAlarm();//on stop lalarme
			//on met a jour lalarme dans la bdd
			this.reqUpdateAlarme(alarme2);
			//on l'ajoute dans la liste des alarmes puisquelle doit ressonner plutard ou demain
			this.addAlarme(alarme2);
			
		}
		
		
	}	

	/**
	 * La fonction permet de supprimer une alarme de la liste des alarmes de l'utilisateur connecter
	 * @author Said El Amani
	 * @since 2.0
	 * @param alarme est un object Alarme
	 */
	public void deleteAlarmeBasique(Alarme alarme) {
		int i = 0;
		boolean trouver =false;
		Alarme alarme2;
		while(i<this.getLesAlarmes().size() && trouver==false) {
			alarme2 = this.getLesAlarmes().get(i);//on reccupere lalarme a lindice i
			if(alarme.equals(alarme2)) {
				//si cest les memes alarmes, on change le boolean pour quitter la boucle au prochain tour
				this.LesAlarmes.remove(i);//On supprime lalarme
			}
			i++;
		}
	}

	/**
	 * La fonction permet de preparer la verification d'une alarme Semaine pour savoir si elle � �t� manquer durant l'absence de l'utilisateur. La fonction reccupere les alarmes Semaine de l'utilisateur dans la base de donn�e, et lance la verification pour chaque alarme.
	 * @author Said El Amani
	 * @since 2.0
	 */
	private void verificationManquerSemaine() throws SQLException {
		//on reccupere toutes les alarmes quotidienne
		ResultSet data = this.reqGetAlarmesSemaine();
		//on verifie s'il yen a qui se sont realiser pendant la deconnexion de lutilisateur
		int id=-1;
		int heure=-1;
		int minute = -1;
		int numeroJourRealisation =-1;

		String jour ="";
		String regularite ="";
		String titre ="";
		String dateCreation ="";
		String heureCreation ="";
		
		//ici pour le positionnement de lalarme en fonction du temps, on a besoin de connaitre le dernier jour ou il cest connecter
		while(data.next()) {
			//sil a au moin une alarme cest que il cest deja connecter donc on a une date de derniere connexion 
			this.reqGetLastConnexionUser();
			//on reccupere les valeurs de l'alarme
			heure = data.getInt("heures");
			minute = data.getInt("minutes");
			titre = data.getString("titre");
			regularite = data.getString("regularite");
			jour = data.getString("jour");
			id = data.getInt("id_alarme");
			dateCreation = data.getString("dateCreation");
			heureCreation = data.getString("heureCreation");
			numeroJourRealisation = data.getInt("numeroJourRealisation");

			//si la regularite est tout les jours
			this.alarmeManquerSemaine(id, dateCreation, heureCreation,numeroJourRealisation, jour, regularite, heure, minute, titre);
		}
	}
	
	/**
	 * La fonction permet de preparer la verification d'une alarme quotidienne pour savoir si elle � �t� manquer durant l'absence de l'utilisateur. La fonction reccupere les alarmes quotidienne de l'utilisateur dans la base de donn�e, et lance la verification pour chaque alarme.
	 * @author Said El Amani
	 * @since 2.0
	 */
	private void verificationManquerQuotidien() throws SQLException {
		//on reccupere toutes les alarmes quotidienne
		ResultSet data = this.reqGetAlarmesQuotidient();
		//on verifie s'il yen a qui se sont realiser pendant la deconnexion de lutilisateur
		int id=-1;
		int heure=-1;
		int minute = -1;

		String jour ="";
		String regularite ="";
		String titre ="";
		String dateCreation ="";
		String heureCreation ="";
		int numeroJourRealisation =-1;

		//ici pour le positionnement de lalarme en fonction du temps, on a besoin de connaitre le dernier jour ou il cest connecter
		while(data.next()) {
			//sil a au moin une alarme cest que il cest deja connecter donc on a une date de derniere connexion 
			this.reqGetLastConnexionUser();
			//on reccupere les valeurs de l'alarme
			heure = data.getInt("heures");
			minute = data.getInt("minutes");
			titre = data.getString("titre");
			regularite = data.getString("regularite");
			jour = data.getString("jour");
			id = data.getInt("id_alarme");
			dateCreation = data.getString("dateCreation");
			heureCreation = data.getString("heureCreation");
			numeroJourRealisation = data.getInt("numeroJourRealisation");
			
			//si la regularite est tout les jours
			this.alarmeManquerQuotidien(id, dateCreation, heureCreation,numeroJourRealisation, jour, regularite, heure, minute, titre);
		}
	}
	
	/**
	 * La fonction permet de preparer la verification d'une alarme Basique pour savoir si elle � �t� manquer durant l'absence de l'utilisateur. La fonction reccupere les alarmes Basique de l'utilisateur dans la base de donn�e, et lance la verification pour chaque alarme.
	 * @author Said El Amani
	 * @since 2.0
	 */
	private void verificationManquerBasique() throws SQLException {

		//on reccupere toutes les alarmes quotidienne
		ResultSet data = this.reqGetAlarmesBasique();
		//on verifie s'il yen a qui se sont realiser pendant la deconnexion de lutilisateur
		int id=-1;
		int heure=-1;
		int minute = -1;

		String jour ="";
		String regularite ="";
		String titre ="";
		String dateCreation ="";
		String heureCreation ="";
		int numeroJourRealisation =-1;

		//ici pour le positionnement de lalarme en fonction du temps, on a besoin de connaitre le dernier jour ou il cest connecter
		while(data.next()) {
			//sil a au moin une alarme cest que il cest deja connecter donc on a une date de derniere connexion 
			this.reqGetLastConnexionUser();
			//on reccupere les valeurs de l'alarme
			heure = data.getInt("heures");
			minute = data.getInt("minutes");
			titre = data.getString("titre");
			regularite = data.getString("regularite");
			jour = data.getString("jour");
			id = data.getInt("id_alarme");
			dateCreation = data.getString("dateCreation");
			heureCreation = data.getString("heureCreation");
			numeroJourRealisation = data.getInt("numeroJourRealisation");

			//si la regularite est tout les jours
			this.alarmeManquerBasique(id, dateCreation, heureCreation,numeroJourRealisation, jour, regularite, heure, minute, titre);
		}
	}

	/**
	 * La fonction permet de verifier les donn�es d'une alarme, verifier si l'heure et la minute sont dans le bonne intervalle etc..
	 * @author Said El Amani
	 * @since 2.0
	 * @param heure est un int representant l'heure a laquelle l'alarme doit se d�clencher
	 * @param minute est un int representant la minute � laquelle l'heure doit se declencher
	 * @param jour est un String representant un jour de la semaine, exemple Mardi
	 * @param titre est un String representant le titre de l'alarme
	 * @param regularite est un String representant la frequence de l'alarme
	 * @throws ClockException
	 */
	public void verificationDonneeAlarme(int heure, int minute, String jour, String titre, String regularite) throws ClockException {
		//si les heures ne sont pas dans lintervalle il y a une erreur
		if(heure >23 || heure <0) {
			throw new ClockException("Votre Alarme doit etre compris entre 23 h 59 et 00h 00.");
		}
		//si les minutes ne sont pas dans linterval il y a une erreur
		if(minute>59 || minute<0) {
			throw new ClockException("Les minutes de votre Alarme doivent etre compris entre 59 et 00.");
		}
		//si lutilisateur na pas renseinger de titre ou juste des espace demande de modifier 
		if(this.titreIsVide(titre) == true || titre.length()<2 || titre.length()>100) {
			throw new ClockException("Votre titre ne peux pas etre vide et doit contenir entre 2 et 100 carracteres.");
		}
		if(Constante.JOUR_DE_LA_SEMAINE.contains(jour)==false) {
			throw new ClockException("Votre jour n'est pas valide, selectionnez-en un parmis les choix proposer.");
		}
		if( Constante.REGULARITE_ALARME.contains(regularite)==false) {
			throw new ClockException("La frequence de votre alarme n'est pas correcte, selectionnez-en un parmis les choix proposer.");
		}
		
	}
	/**
	 * La fonction permet finir la configuration de l'alarme, elle parametre l'instant de creation, le jour ou l'alarme doit se declencher etc.
	 * @author Said El Amani
	 * @since 2.0
	 * @param alarme est un objecy Alarme, c'est l'alarme qui doit etre parametrer
	 * @param nbrJour est le nombre de jour restant avant que l'alarme ne se realise.
	 * @return un object Alarme
	 */
	public Alarme setAlarmeDetails(Alarme alarme, int nbrJour) {
        //je reccupere la date local
		LocalDateTime myDateObj = LocalDateTime.now();
		//je reccupere la valeur de lheure et de la date
		String instantMaintenantDate = myDateObj.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	    String instantMaintenantHeure = myDateObj.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
	    //de la je demande un object date et temps
		LocalDate dateMaintenant = LocalDate.parse(instantMaintenantDate);
		Time dateTimeMaintenant = Time.valueOf(instantMaintenantHeure);
		int aujourdhui =dateMaintenant.getDayOfYear(); //je reccupere le numero de jour de l'annee, une valeur entre 1 et 365/366 jour
		//on parametre lalarme
		alarme.setAlarmeDateCreation(instantMaintenantDate);
		alarme.setAlarmeHeureCreation(instantMaintenantHeure);
		
		int annee = 365;//On saisit le nombre de jour de lannee
		if(this.yearIsBisextile(dateMaintenant.getYear())) {
			//si lannee est bisextile, on change le nombre de jour de lannee
			annee= 366;
		}
		//si le jour ou doit se realiser lalarme se situe sur la meme annee on peut juste ajouter le nombre de jour restant
		if(aujourdhui+nbrJour <=annee ) {
			//on est sur la meme annee
			alarme.setAlarmeNbrJourRealisation(aujourdhui+nbrJour);
		}else {
			//sinon si
			//on est dans la meme semaine, mais lannee va changer, on met les jours jusqua atteindre le derniere jour 365/366, ensuite on recommence � 1 et on ajoute les jours restant
			alarme.setAlarmeNbrJourRealisation(annee-(aujourdhui+nbrJour));
		}
		return alarme;
	}
	
	/**
	 * La fonction permet creer une Alarme Basique.
	 * @author Said El Amani
	 * @since 2.0
	 * @param jour est un String du jour de la semaine ou doit se realiser lalarm 
	 * @param regulariter est un string representant la frequence de l'alarme
	 * @param heures est un int representant l'heure ou doit se declencher l'alarme
	 * @param minutes est un int representant la minute � laquelle devra se declencher l'alarme
	 * @param titre est un String representant le titre de l'alarme
	 * @param init est un boolean, s'il est false on parametre notre alarme, s'il est true on ne parametre pas notre alarme. Il permet de savoir si l'alarme doit etre initialiser ou pas, cela est utile lors de la verification de l'alarme, cela permet de passer par cette fonction pour creer notre alarme sans preciser l'heure � laquelle elle � �t� cr�e etc.
	 * @return un object Alarme Basique
	 * @throws ClockException
	 */
	public Alarme creerAlarmeBasique(String jour, String regulariter, int heures, int minutes, String titre, boolean init) throws ClockException {
		//on creer une alarme null
		Alarme alarme = null;
		
		//on va calculer le temps en milliseconde qui nous separer de la fin de l'alarme
		Calendar cal = Calendar.getInstance(); //un calendrier qui va servir � positionner notre jour dans le temps, es ce quil est dans le futur, le pass�, ou c'est aujourdhui

		int id_jour_aujourdhui = cal.get(Calendar.DAY_OF_WEEK);//on reccupere le jour d'aujourdhui ex : lundi, mardi etc.
		
		int id_jour = Constante.JOUR_DE_LA_SEMAINE.indexOf(jour); //on reccupere le jour de lalarme
		
		LocalDateTime localDate = LocalDateTime.now(); //on reccupere lheure actuel, qui va nous permettre situer plus precisement notre alarme dans le temps par apport au heure, minute
		int heure_actuel = Integer.parseInt(DateTimeFormatter.ofPattern("H").format(localDate)); 
		int minute_actuel = Integer.parseInt(DateTimeFormatter.ofPattern("m").format(localDate));
		
		long timeMilliSeconde;

		if (id_jour ==id_jour_aujourdhui) {
			//si cest aujourdhui 
			if (heures>heure_actuel) {
				//futur
				
				//regulariter une seul fois 
				timeMilliSeconde=((heures*60 +minutes)-(heure_actuel*60+minute_actuel))*60*1000;
				
			}else if(heures==heure_actuel && minutes> minute_actuel) {
				//Maintenant
				
				//le temps qui nous separe est la difference de minutes 
				timeMilliSeconde=(minutes-minute_actuel)*60*1000;
				
			}else {
				//passer
				
				//l'alarme ne peut pas etre creer, elle est dans le passer 
				throw new ClockException("Pour creer une alarme a regularite <<une seule fois>> vous devez saisir une heure, une minute et/ ou un jour qui se situe dans le futur.");
			}
		}else if(id_jour>id_jour_aujourdhui) {
			//si cest dans le futur
			if (id_jour_aujourdhui+1 == id_jour) {
				//si cest le lendemain on calcul juste la difference de temps jusqua minuit et on rajoute le temps restant
				timeMilliSeconde=(((24*60)-(heure_actuel*60+minute_actuel))+heures*60+minutes)*60*1000;
			}else {
				//si cest un autre jour, on calcul le temps de lalarme du lendemain, et on rajoute les jours qui manque
				timeMilliSeconde=((((24*60)-(heures*60+minutes))+heures*60+minutes)+((id_jour-id_jour_aujourdhui+1)*24*60))*60*1000;
			}
			//on creer lalarme
			
		}else {
			//si cest dans le passer
			//l'alarme ne peut pas etre creer car elle est dans le passer
			throw new ClockException("Pour creer une alarme a regularite <<une seule fois>> vous devez saisir une heure, une minute et/ ou un jour qui se situe dans le futur.");
		}
		
		//on peut creer lalarme
		alarme = new AlarmeBasique(-1,  jour, regulariter,titre, heures, minutes, timeMilliSeconde);
		//si on initialise pas  l'application, on peut detailer lalarme
		if(init==false) {
			//on precise le mois et l'ann�e ou � ete creer l'alarme, elles seront mise � jour a chaque fois qu'une alarme se relance, ex : les alarmes quotidiennent, les alarmes semaine
			alarme = this. setAlarmeDetails(alarme, id_jour_aujourdhui - id_jour);
			//on ajoute
		}
		return alarme;
	}
	
	/**
	 * La fonction permet creer une AlarmeQuotidien.
	 * @author Said El Amani
	 * @since 2.0
	 * @param jour est un String du jour de la semaine ou doit se realiser lalarm 
	 * @param regulariter est un string representant la frequence de l'alarme
	 * @param heures est un int representant l'heure ou doit se declencher l'alarme
	 * @param minutes est un int representant la minute � laquelle devra se declencher l'alarme
	 * @param titre est un String representant le titre de l'alarme
	 * @param init est un boolean, s'il est false on parametre notre alarme, s'il est true on ne parametre pas notre alarme. Il permet de savoir si l'alarme doit etre initialiser ou pas, cela est utile lors de la verification de l'alarme, cela permet de passer par cette fonction pour creer notre alarme sans preciser l'heure � laquelle elle � �t� cr�e etc.
	 * @return un object AlarmeQuotidien
	 */
	public Alarme creerAlarmeQuotidien(String jour, String regulariter, int heures, int minutes, String titre, boolean init) {

		//on creer une alarme null
		Alarme alarme = null;
		
		//on va calculer le temps en milliseconde qui nous separer de la fin de l'alarme
		Calendar cal = Calendar.getInstance(); //un calendrier qui va servir � positionner notre jour dans le temps, es ce quil est dans le futur, le pass�, ou c'est aujourdhui

		int id_jour_aujourdhui = cal.get(Calendar.DAY_OF_WEEK);//on reccupere le jour d'aujourdhui ex : lundi, mardi etc.

		int id_jour = Constante.JOUR_DE_LA_SEMAINE.indexOf(jour); //on reccupere le jour de lalarme
	
		LocalDateTime localDate = LocalDateTime.now(); //on reccupere lheure actuel, qui va nous permettre situer plus precisement notre alarme dans le temps par apport au heure, minute
		int heure_actuel = Integer.parseInt(DateTimeFormatter.ofPattern("H").format(localDate)); 
		int minute_actuel = Integer.parseInt(DateTimeFormatter.ofPattern("m").format(localDate));
		
		long timeMilliSeconde;

		if (id_jour ==id_jour_aujourdhui) {
			//si cest aujourdhui et que lheure nest pas encore passer lalarme sonnera aujourdhui
			if (heures>heure_actuel) {
				//futur
				//regulariter tous les jours 
				timeMilliSeconde=((heures*60 +minutes)-(heure_actuel*60+minute_actuel))*60*1000;
				
			}else if(heures==heure_actuel && minutes> minute_actuel) {
				//Maintenant
				
				timeMilliSeconde=(minutes-minute_actuel)*60*1000;
				//les minutes sont plus grandes
				
			}else {
				//dans le passer donc lalarme sonnera demain
				
				//le temps est 24 h - la difference entre (lheure de maintenant- lheure de lalarme )
				timeMilliSeconde=((24*60)-((heure_actuel*60+minute_actuel)-(heures*60 +minutes)))*60*1000;
			}
		}else if(id_jour>id_jour_aujourdhui) {
			//si cest dans le futur
				//si cest le lendemain on calcul juste la difference de temps
				// le temps est egal au temps qui nous reste avant minuit + le temps de lalarme 
				timeMilliSeconde=(((24*60)-(heure_actuel*60+minute_actuel))+heures*60+minutes)*60*1000;
		}else {
			//si cest dans le passer
			//on calcul le temps pour demain puisque lalarme sactive tout les jours
			//le temps est 24 h - la difference entre (lheure de maintenant- lheure de lalarme )
			timeMilliSeconde=((24*60)-((heure_actuel*60+minute_actuel)-(heures*60 +minutes)))*60*1000;
			
		}
		//on creer l'alarme
		alarme = new AlarmeQuotidien(-1, jour, regulariter, titre, heures, minutes, timeMilliSeconde);
		//si on initialise pas  l'application, on peut detailer lalarme
		if(init==false) {
			//on precise le mois et l'ann�e ou � ete creer l'alarme, elles seront mise � jour a chaque fois qu'une alarme se relance, ex : les alarmes quotidiennent, les alarmes semaine
			alarme = this. setAlarmeDetails(alarme, id_jour_aujourdhui - id_jour);
			//on ajoute
		}
		return alarme;
	}
	
	/**
	 * La fonction permet creer une AlarmeSemaine.
	 * @author Said El Amani
	 * @since 2.0
	 * @param jour est un String du jour de la semaine ou doit se realiser lalarm 
	 * @param regulariter est un string representant la frequence de l'alarme
	 * @param heures est un int representant l'heure ou doit se declencher l'alarme
	 * @param minutes est un int representant la minute � laquelle devra se declencher l'alarme
	 * @param titre est un String representant le titre de l'alarme
	 * @param init est un boolean, s'il est false on parametre notre alarme, s'il est true on ne parametre pas notre alarme. Il permet de savoir si l'alarme doit etre initialiser ou pas, cela est utile lors de la verification de l'alarme, cela permet de passer par cette fonction pour creer notre alarme sans preciser l'heure � laquelle elle � �t� cr�e etc.
	 * @return un object AlarmeSemaine
	 */
	public Alarme creerAlarmeSemaine(String jour, String regulariter, int heures, int minutes, String titre, boolean init) {

		//on creer une alarme null
		Alarme alarme = null;
		
		//on va calculer le temps en milliseconde qui nous separer de la fin de l'alarme
		Calendar cal = Calendar.getInstance(); //un calendrier qui va servir � positionner notre jour dans le temps, es ce quil est dans le futur, le pass�, ou c'est aujourdhui

		int id_jour_aujourdhui = cal.get(Calendar.DAY_OF_WEEK);//on reccupere le jour d'aujourdhui ex : lundi, mardi etc.
		//si on est ici, cest que jour est dans la liste des JOUR_DE_LA_SEMAINE, donc id_jour <-1
		int id_jour = Constante.JOUR_DE_LA_SEMAINE.indexOf(jour); //on reccupere le jour de lalarme
	
		LocalDateTime localDate = LocalDateTime.now(); //on reccupere lheure actuel, qui va nous permettre situer plus precisement notre alarme dans le temps par apport au heure, minute
		int heure_actuel = Integer.parseInt(DateTimeFormatter.ofPattern("H").format(localDate)); 
		int minute_actuel = Integer.parseInt(DateTimeFormatter.ofPattern("m").format(localDate));
		
		long timeMilliSeconde;

		if (id_jour ==id_jour_aujourdhui) {
			//si cest aujourdhui et que lheure nest pas encore passer lalarme sonnera d'abbord aujourdhui 
			if (heures>heure_actuel) {
				//futur
				
				//regulariter toutes les semaines
				timeMilliSeconde=((heures*60 +minutes)-(heure_actuel*60+minute_actuel))*60*1000;
				//alarme = new AlarmeBasique(id_alarme, titre, heures, minutes,timeMilliSeconde);
				
			}else if(heures==heure_actuel && minutes> minute_actuel) {
				//l'alarme va sonner dans quelque minutes
				timeMilliSeconde=(minutes-minute_actuel)*60*1000;
			}else {
				//passer
				//le temps est passer donc lalarme ne sonnera pas aujourdhui, mais dans 1 semaine
				// (on calcul lalarme pour demain)				
				timeMilliSeconde=((24*60)-((heure_actuel*60+minute_actuel)-(heures*60 +minutes)))*60*1000;
				//elle est de regulariter Toutes les semaines
				//donc on on rajoute 6 jours a demain pour que lalarme tombe le bon jour � la bonne heure
				timeMilliSeconde=timeMilliSeconde+(6*24*60*60*1000);
			}
		}else if(id_jour>id_jour_aujourdhui) {
			//si cest dans le futur
			if (id_jour_aujourdhui+1 == id_jour) {
				//si cest le lendemain on calcul juste la difference de temps jusqua minuit et on rajoute le temps restant
				timeMilliSeconde=(((24*60)-(heure_actuel*60+minute_actuel))+heures*60+minutes)*60*1000;
			}else {
				//si cest un autre jour, on calcul le temps de lalarme du lendemain, et on rajoute les jours qui manque
				timeMilliSeconde=((((24*60)-(heure_actuel*60+minute_actuel))+heures*60+minutes)+((id_jour-id_jour_aujourdhui+1)*24*60))*60*1000;
			}
			
		}else {
			//si cest dans le passer
			//on calcul le temps pour demain
			timeMilliSeconde=((24*60)-((heure_actuel*60+minute_actuel)-(heures*60 +minutes)))*60*1000;
			//je calcul le temps pour une semaine (donc demain dans une semaine)
			timeMilliSeconde=timeMilliSeconde+(7*24*60*60*1000);
			// je soustraie le temps qui cest deja ecouler pour avoir le temps quil reste avant lalarme 
			//temps - la difference de jour 
			timeMilliSeconde = timeMilliSeconde - ((id_jour_aujourdhui - id_jour)*24*60*60*1000);
		}
		//on creer l'alarme
			alarme = new AlarmeSemaine(-1,  jour, regulariter, titre, heures, minutes, timeMilliSeconde);
			//si on initialise pas  l'application, on peut detailer lalarme
			if(init==false) {
				//on precise le mois et l'ann�e ou � ete creer l'alarme, elles seront mise � jour a chaque fois qu'une alarme se relance, ex : les alarmes quotidiennent, les alarmes semaine
				alarme = this. setAlarmeDetails(alarme, id_jour_aujourdhui - id_jour);
				//on ajoute
			}
			return alarme;
	}
}
