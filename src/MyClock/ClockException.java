package MyClock;

public class ClockException  extends Exception {

	/**
	 * Un object exception specialement pour les Clock
	 */
	public ClockException() {

	}

	/**
	 * Un object exception specialement pour les Clock
	 * @param s est un string, cest le message � renvoyer dans le cas ou il y a une erreur
	 */
	public ClockException(String s) {
		super(s);
	}

}
