package MyClock;

public class Chronometre {
	private long delayTimeMilliSeconde;
	private boolean active;

	/**
	 * Un object Chronometre.
	 */
	public Chronometre() {	
	}
	
	/**
	 * La fonction permet de reccupperrer le temps en millisseconde du chronometre
	 * @author Said El Amani
	 * @since 1.0
	 * @return un long representant le delay en millisseconde 
	 */
	public long getDelayTimeMilliSeconde() {
		return delayTimeMilliSeconde;
	}

	/**
	 * La fonction permet de changer le temps en millisseconde du chronometre
	 * @author Said El Amani
	 * @since 1.0
	 * @param delayTimeMilliSeconde est un long representant le temps en millisseconde
	 */
	public void setDelayTimeMilliSeconde(long delayTimeMilliSeconde) {
		this.delayTimeMilliSeconde = delayTimeMilliSeconde;
	}
	
	/**
	 * La fonction permet d'ajouter un temps ai delay du chronometre
	 * @author Said El Amani
	 * @since 2.0
	 * @param delayTimeMilliSeconde est un long qui sera ajouter au delay du chronometre
	 */
	public void addDelayTimeMilliSeconde(long delayTimeMilliSeconde) {
		this.delayTimeMilliSeconde += delayTimeMilliSeconde;
	}

	/**
	 * La fonction permet de savoir si le chronometre est activer ou non
	 * @author Said El Amani
	 * @since 2.0
	 * @return un boolean, true si le chronometre est aciter, sinon false
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * La fonction permet de changer la valeur de l'attribut active de l'object chronometre
	 * @author Said El Amani
	 * @since 2.0
	 * @param active est un boolean, true pour activer le chronometre et false pour desactiver
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * La fonction permet d'afficher le temps passer du chronometre en minute et seconde
	 * @author Said El Amani
	 * @since 2.0
	 * @return un String indiquant les minutes et les secondes qui se sont ecoul�es  
	 */
	public String PrintDelayConvert() {
		long time = this.getDelayTimeMilliSeconde()/1000;
		int milliseconde = (int) (this.getDelayTimeMilliSeconde() %1000);
		int minute=0;
		int seconde=0;
		int heure=0;
		int jour=0;
		if(time>=60) {
			//on reccupere les minutes et les secondes
			minute = (int) time/60;
			seconde = (int) (time%60);
			if (minute>=60) {
				//si les minutes sont au dessu de 60, on peut les converir en heure et minutes
				heure = (int) minute/60;
				minute = minute%60;
				if(heure>=24 ) {
					//si les heures sont au dessu de 24, on peut les convertires en heures et jours
					jour = (int) heure/24;
					heure = heure%24;
					
				}
			}
		}else {
			seconde = (int)time;
		}
		//on formatte le texte
		String s_seconde = String.valueOf(seconde);
		String s_minute = String.valueOf(minute);
		if( s_seconde.length() == 1) {
			s_seconde = "0"+s_seconde;
		}if( s_minute.length() == 1) {
			s_minute = "0"+s_minute;
		}
		return (s_minute+" : "+s_seconde);
	}
	/**
	 * La fonction permet de stopper un chronometre
	 * @author Said El Amani
	 * @since 2.0
	 */
	public void stop() {
		this.setActive(false);
	}

	/**
	 * La fonction permet de lancer le chronometre
	 * @author Said El Amani
	 * @since 2.0
	 */
	public void start() {
		//on active le chronometre
		this.setActive(true);
		
	}
}
