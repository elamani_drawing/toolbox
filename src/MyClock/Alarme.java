package MyClock;

import java.util.Timer;
import java.util.TimerTask;

import MyClockIT.ConstanteDashboard;


public abstract class Alarme extends Horloge{

	private int id_Alarme;
	private String titre;

	//Pour avoir les jours et les regularite possible, il faut se refferer a l'interface Constante
	private String jour;

	private boolean finalDelayInit=false;
	private boolean active=true;
	private String regularite;

	private String alarmeDateCreation;
	private String alarmeHeureCreation;
	private int alarmeNbrJourRealisation;//le jour ou lalarme doit se realiser, une valeur entre 1 et 365-366 jour
	
	//une refference au manager de lalarme
	protected AlarmeManager ManagerAlarme;

	//un object timer qui va etre utiliser pour faire un evenement a la fin de delayTimeMilliSeconde
	private Timer timeRun = new Timer();
	private long delayTimeMilliSeconde;

	
	/**
	 * L'object Alarme
	 * @param id est l'id de l'alarme.
	 * @param jour est le jour. Exemple : "Lundi"
	 * @param regularite est la frequence de l'alarme. Exemple : "Tout les jours"
	 * @param titre est le titre de l'alarme. Exemple "Rendez vous important."
	 * @param heures est l'heure � l'aquelle l'alarme doit sonner
	 * @param minutes est la minute � laquelle l'alarme doit sonner 
	 * @param delayTimeMilliSeconde est le temps separant l'alarme de l'instant ou elle doit s'enclencher. Le temps est en milliseconde
	 * @since 1.0
	 * @version 2.0
	 */
	public Alarme(int id, String jour, String regularite, String titre, int heures, int minutes,long delayTimeMilliSeconde) {
		super(heures, minutes, 0);
		this.titre = titre;
		this.delayTimeMilliSeconde = delayTimeMilliSeconde;
		this.jour = jour;
		this.regularite = regularite;
		this.id_Alarme = id;
		this.ManagerAlarme = ConstanteDashboard.ManagerAlarme;//on charge notre manager d'alarme
		//des la creation de l'object alarme on lance l'alarme
		this.startAlarm();
	}
	
	/**
	 * La fonction permet de reccuperer le jour de l'ann�e ou � ete creer l'alarme
	 * @author Said El Amani
	 * @since 1.0
	 * @return un nombre entre 1 et 365/366
	 */
	public int getAlarmeNbrJourRealisation() {
		return alarmeNbrJourRealisation;
	}
	/**
	 * La fonction permet de definir le jour de l'ann�e ou � �t� cr�er l'alarme 
	 * @author Said El Amani
	 * @since 1.0
	 * @param alarmeNbrJourRealisation est un int representant le jour de l'ann�e, elle prend une valeur comprise entre 1 et 365/366 
	 */
	public void setAlarmeNbrJourRealisation(int alarmeNbrJourRealisation) {
		this.alarmeNbrJourRealisation = alarmeNbrJourRealisation;
	}
	/**
	 * La fonction permet de reccuperer la date ou � �t� cr�er l'alarme. 
	 * @author Said El Amani
	 * @since 1.0
	 * @return Un string de forme "yyyy-MM-dd" representant la date
	 */
	public String getAlarmeDateCreation() {
		return alarmeDateCreation;
	}
	/**
	 * La fonction permet de definir la date � laquelle � �t� cr�e l'alarme.
	 * @author Said El Amani
	 * @since 1.0
	 * @param alarmeDateCreation est un string representant la date, elle doit etre de la forme "yyyy-MM-dd"
	 */
	public void setAlarmeDateCreation(String alarmeDateCreation) {
		this.alarmeDateCreation = alarmeDateCreation;
	}
	/**
	 * La fonction permet de reccuperer l'heure ou l'alarme � �t� creer.
	 * @author Said El Amani
	 * @since 1.0
	 * @return un String representant l'heure de la forme "HH:mm:ss"
	 */
	public String getAlarmeHeureCreation() {
		return alarmeHeureCreation;
	}
	/**
	 * La fonction permet de definir l'heure � laquelle l'alarme � �t� cr�e. 
	 * @author Said El Amani
	 * @since 2.0
	 * @param alarmeHeureCreation est un String representant l'heure de la creation de l'alarme, elle est de la forme "HH:mm:ss"
	 */
	public void setAlarmeHeureCreation(String alarmeHeureCreation) {
		this.alarmeHeureCreation = alarmeHeureCreation;
	}


	/**
	 * La fonction permet r�cup�rer l'id de l'alarme
	 * @author Said El Amani
	 * @since 1.0
	 * @return un int representant l'id de l'alarme
	 */
	public int getIdAlarme() {
		return id_Alarme;
	}

	/**
	 * La fonction permet de modifier l'id de l'alarme
	 * @author Said El Amani
	 * @since 1.0
	 * @param id est un int.
	 */
	public void setIdAlarme(int id) {
		this.id_Alarme = id;
	}

	/**
	 * La fonction permet de r�cup�rer le Titre de l'alarme
	 * @author  Said El Amani
	 * @since 1.0
	 * @return un String titre, qui est le titre de l'alarme.
	 */
	
	public String getTitre() {
		return titre;
	}
	
	/**
	 * La fonction permet de modifier l'attribut titre de l'alarme
	 * @author Said El Amani
	 * @since 1.0
	 * @param titre est un String
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	/**
	 * La fonction permet de r�cup�rer le Timer de l'alarme
	 * @author  Said El Amani
	 * @since 1.0
	 * @return le Timer de l'alarme (un object Timer)
	 */
	public Timer getTimeRun() {
		return timeRun;
	}
	/**
	 * La fonction permet de modifier le Timer de l'alarme
	 * @author Said El Amani
	 * @since 1.0
	 * @param timeRun qui est un object Timer
	 */
	public void setTimeRun(Timer timeRun) {
		this.timeRun = timeRun;
	}

	/**
	 * La fonction permet de reccupperer le delayTimeMilliSeconde de l'alarme.
	 * @author Said El Amani
	 * @since 1.0
	 * @return le delayTimeMilliSeconde qui est un chiffre en milliseconde
	 */
	public long getDelayTimeMilliSeconde() {
		return delayTimeMilliSeconde;
	}

	/**
	 * La fonction permet de modifier le delayTimeMilliSeconde de l'alarme.
	 * @author Said El Amani
	 * @since 1.0
	 * @param delayTimeMilliSeconde est un nombre en millisseconde.
	 */
	public void setDelayTimeMilliSeconde(long delayTimeMilliSeconde) {
		this.delayTimeMilliSeconde = delayTimeMilliSeconde;
	}
	/**
	 * La fonction permet de savoir si le delay final de l'alarme a �t� r�aliser, le delay final correspond au delay de l'alarme apres qu'elle c'est realiser une premiere fois, pour les alarmes Quotidienne ca sera tout les 24h, pour les alarmeSemaine ca sera tout les 7*24h
	 * @author Said El Amani
	 * @since 1.0
	 * @return La fonction renvoie true si le temp final a �t� initaliser, sinon false
	 */
	public boolean isFinalDelayInit() {
		return finalDelayInit;
	}
	/**
	 * La fonction permet de dire si on a initialiser le delay final de l'alarme, de sorte � ne pas la calculer a chaque d�clenchement de l'alarme.
	 * @author Said El Amani
	 * @since 1.0
	 * @param finalDelayInit qui est un boolean.
	 */
	public void setFinalDelayInit(boolean finalDelayInit) {
		this.finalDelayInit = finalDelayInit;
	}

	/**
	 * La fonction permet de savoir si l'alarme est activer ou desactiver, la fonction retourne la valeur true si l'alarme est active, sinon false
	 * @author Said El Amani
	 * @since 1.0
	 * @return La fonction retourne true si l'alarme est activer, false si elle n'est pas activer
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * La fonction permet de modifier la valeur de active qui indique si l'alarme est active ou non.
	 * @author Said El Amani
	 * @since 1.0
	 * @param active est un boolean d�crivant l'etat de l'alarme, true si l'alarme doit etre activer et false si elle doit etre d�sactiver. 
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * Permet de r�cup�rer le jour ou doit s'enclencher l'alarme.
	 * @author Said El Amani
	 * @since 1.0
	 * @return le jour ou doit s'enclencher l'alarme. Ex: Lundi, Mardi etc.
	 */
	public String getJour() {
		return jour;
	}
	/**
	 * La fonction permet de modifier la valeur de jour, jour est un jour de la semaine, exemple : Lundi, Mardi, ..., Dimanche
	 * @author Said El Amani
	 * @since 1.0
	 * @param jour est un String d�crivant un jour de la semaine. Exemple "Lundi", "Mardi" etc.
	 */
	public void setJour(String jour) {
		this.jour = jour;
	}
	
	/**
	 * La fonction permet de reccuperrer la regularite/frequence de l'alarme
	 * @author Said El Amani
	 * @since 1.0
	 * @return La regulariter
	 */
	public String getRegularite() {
		return regularite;
	}
	
	/**
	 * La fonction permet de modifier la r�gularit� de l'alarme
	 * @author Said El Amani
	 * @since 1.0
	 * @param regularite qui est la nouvelle regularit�/frequence de l'alarme ("Tous les jours", "Toutes les semaines", "Une seule fois")
	 */
	public void setRegularite(String regularite) {
		this.regularite = regularite;
	}
	
	/**
	 * La fonction permet d'annuler/arreter une alarme
	 * @author Said El Amani
	 * @since 1.0
	 */
	public void stopAlarm() {
		this.getTimeRun().cancel();
	}
	/**
	 * Permet de d�marrer l'alarme, la fonction reccupere le delayTimeMilliSeconde de l'alarme, r�alise un d�compte puis notifie l'utilisateur de son alarme 
	 * @author Said El Amani
	 * @since 1.0
	 */
	public void startAlarm() {
		//on lance le decompte
		this.getTimeRun().schedule(new TimerTask(){
			@Override
			public void run() {
				//on recalcul le temps avant de relancer l'alarme
				recalculTemps();
			}
		},this.getDelayTimeMilliSeconde());
	}
	/**
	 * La fonction permet d'avoir le jour de la semaine, l'heure et la minute ou doit se d�clencher l'alarme 
	 * @author Said El Amani
	 * @since 2.0
	 * @return Une phrase indiquant le jour, l'heure et la minute ou se d�clenche l'alarme
	 */
	public String getDayHour() {
		//on formatte le texte
		String heure = String.valueOf(this.getHeures());
		String minute = String.valueOf(this.getMinutes());
		if(heure.length() == 1) {
			heure = "0"+heure;
		}if(minute.length() == 1) {
			minute = "0"+minute;
		}
		 return this.jour+ " � "+ heure+ " h "+ minute;
		
	}
	/**
	 * La fonction equals permet de comparrer 2 alarmes et de savoir si elles sont egaux.
	 * @author Said El Amani
	 * @since 1.0
	 * @param alarme est un object Alarme
	 * @return Elle renvoit true si elles sont egaux et false si elles ne sont pas egaux.
	 */
	public boolean equals(Alarme alarme) {
		return ((alarme.id_Alarme == this.id_Alarme)&&(alarme.getTitre() == this.getTitre()) && (this.getHeures() == alarme.getHeures())&&(this.getMinutes() == alarme.getMinutes()));	
	}
	
	/**
	 * Renvoie le titre de l'alarme et l'heure � laquelle elle doit s'enclencher
	 * @author Said El Amani
	 * @since 1.0
	 */
	@Override
	public String toString() {
		return ("Le titre de l'alarme est: "+this.getTitre()+"\nL'alarme est � "+this.getHeures()+" heures, "+this.getMinutes()+" minutes, "+this.getSecondes()+ "secondes.");
	}
	

	public abstract void recalculTemps();
	
	@Override
	/**
	 * Permet de convertir et d'afficher le delayTimeMilliSeconde en Jour, heure, minutes, seconde
	 * @author Said El Amani
	 * @since 1.0
	 * @return Un string indiquant le temps de l'alarme en Jour, heure, minutes, seconde
	 */
	public String PrintDelayConvert() {
		long time = this.getDelayTimeMilliSeconde()/1000;
		int minute=0;
		int seconde=0;
		int heure=0;
		int jour=0;
		if(time>=60) {
			minute = (int) time/60;
			seconde = (int) (time%60);
			if (minute>=60) {
				heure = (int) minute/60;
				minute = minute%60;
				if(heure>=24 ) {
					jour = (int) heure/24;
					heure = heure%24;
					
				}
			}
		}else {
			seconde = (int)time;
		}
		return ("Cette alarme a un delai de "+jour+" jours,"+heure+" heures,"+minute+" minutes,"+seconde+" seconde.");
	}
	
	
}
