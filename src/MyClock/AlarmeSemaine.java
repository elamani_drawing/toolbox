package MyClock;

import java.awt.AWTException;

import javax.swing.JOptionPane;

import NotifyManager.Notification;

public class AlarmeSemaine extends Alarme{
	
	
	/**
	 * L'alarme qui se declenche tout les 7 jours, � la meme heures 
	 * @author Said El Amani
	 * @param id est l'id de l'alarme.
	 * @param jour est le jour. Exemple : "Lundi"
	 * @param regulariter est la frequence de l'alarme. Exemple : "Tout les jours"
	 * @param titre est le titre de l'alarme. Exemple "Rendez vous important."
	 * @param heures est l'heure � l'aquelle l'alarme doit sonner
	 * @param minutes est la minute � laquelle l'alarme doit sonner 
	 * @param delayTimeMilliSeconde est le temps separant l'alarme de l'instant ou elle doit s'enclencher. Le temps est en milliseconde
	 * @since 1.0
	 * @version 2.0
	 */
	public AlarmeSemaine(int id, String jour , String regulariter,String titre, int heures, int minutes, long delayTimeMilliSeconde) {
		super(id,jour, regulariter, titre, heures, minutes, delayTimeMilliSeconde);
	}

	/**
	 * La fonction permet de calculer de calculer le nouveau delay avant que l'alarme ne se redeclenche, 7 jours pour une alarmeSemaine, 24 h pour une alarme basique, et 0 seconde pour une alarmeBasique, elle permet aussi de d�clencher la notification � l'utilisateur au d�clenchement de l'alarme
	 * @author Said El Amani
	 * @since 1.0
	 */
	@Override
	public void recalculTemps() {
		//on reinitialise le delay de l'alarme pour que l'alarme se lance toute les 7jours
		if (this.isFinalDelayInit()==false) {
			//7jours x 24 heures x 60 minutes x 60 secondes x 1000 milliseconde
			long delay = 7*24*60*60*1000;
			this.setDelayTimeMilliSeconde(delay);
			//on change la valeur de delayInit pour notifier qu'on a mis le temps final de l'alarme, et quon aura plus a le refaire
			this.setFinalDelayInit(true);
		}
		
		this.startAlarm();
		//on envoie la notif
		String message ="Votre alarme :'"+this.getTitre()+ "' prevus pour le "+ this.getDayHour()+" viens de se d�clencher.";
		//on met a jour lalarme
		this.ManagerAlarme.setAlarmeDetails(this, 7);//on met a jour la date de realisation, et la date de creation

		this.ManagerAlarme.reqUpdateAlarme(this);//on met a jour la bdd
		//on envoie une notification
		try {
			this.ManagerAlarme.MyClockManagerNotification.sendNotification(new Notification("MyClock : Alarme", message));
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//on envoie un popup � l'utilisateur pour linformer
		JOptionPane.showMessageDialog(this.ManagerAlarme.label,message ,"Une alarme vient de se d�clencher.", JOptionPane.PLAIN_MESSAGE);
		
	}

}
