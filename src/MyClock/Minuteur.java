package MyClock;

import java.awt.AWTException;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import MyClockIT.PanelMinuteur;
import NotifyManager.Notification;

public class Minuteur extends Horloge{

	private Timer timeRun = new Timer();
	private long delayTimeMilliSeconde;

	private boolean active=false;
	
	/**
	 * Le constructeur de la classe Minuteur, ce constructeur permet de creer un minuteur sans preciser de temps
	 */
	public Minuteur() {
		super(0,0,0);
	}
	
	/**
	 * Le constructeur de la classe Minuteur, ce constructeur permet de creer un minuteur en precisant le nombre de minutes, secondes et le delay en millisecondes.
	 * @param minutes est un int representant les minutes avant que le minuteur ne sonne.
	 * @param secondes est un int representant les secondes avant que le minuteur ne sonne.
	 * @param delayTimeMilliSeconde est le temps en millisseconde, avant que le minuteur ne sonne.
	 */
	public Minuteur(int minutes, int secondes, long delayTimeMilliSeconde) {
		super(0, minutes, secondes);
		this.delayTimeMilliSeconde = delayTimeMilliSeconde;
	}
	
	/**
	 * La fonction permet de retourner l'attribut timeRun du Minuteur
	 * @author Said El Amani
	 * @since 1.0
	 * @return retourne un Timer
	 */
	public Timer getTimeRun() {
		return timeRun;
	}
	/**
	 * La fonction permet de modifier le timerRun du Minuteur
	 * @author Said El Amani
	 * @since 1.0
	 * @param timeRun est Timer
	 */
	public void setTimeRun(Timer timeRun) {
		this.timeRun = timeRun;
	}
	
	/**
	 * La fonction permet de reccupperer le delay du minuteur
	 * @author Said El Amani
	 * @since 1.0
	 * @return un long representant le delay du minuteur en milliseconde
	 */
	public long getDelayTimeMilliSeconde() {
		return delayTimeMilliSeconde;
	}
	
	/**
	 * La fonction permet de modifier le delay du minuteur
	 * @author Said El Amani
	 * @since 1.0
	 * @param delayTimeMilliSeconde est un long representant le nouveau delay du minuteur
	 */
	public void setDelayTimeMilliSeconde(long delayTimeMilliSeconde) {
		this.delayTimeMilliSeconde = delayTimeMilliSeconde;
	}
	/**
	 * La fonction permet de savoir si le minuteur est activer ou non
	 * @author Said El Amani
	 * @since 1.0
	 * @return un boolean, true si l'alarme est activer et false si cest pas activer
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * La fonction permet de modifier l'attribut active de l'object Minuteur
	 * @author Said El Amani
	 * @since 1.0
	 * @param active est un boolean
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * La fonction permet de reccuperer les minutes et les secondes du Minuteur
	 * @author Said El Amani
	 * @since 1.0
	 * @return un string representant les minutes et les secondes du Minuteur
	 */
	public String printTime() {
		String s_seconde = String.valueOf(this.getSecondes());
		String s_minute = String.valueOf(this.getMinutes());
		if( s_seconde.length() == 1) {
			s_seconde = "0"+s_seconde;
		}if( s_minute.length() == 1) {
			s_minute = "0"+s_minute;
		}
		return (s_minute+" : "+s_seconde);
	}
	@Override
	public String toString() {
		return ("Ce minuteur est programmer pour sonner dans minutes "+this.getMinutes()+", secondes"+this.getSecondes());
	}

	/**
	 * La fonction permet de convertir le delay du minuteur en minutes et en seconde
	 * @author Said El Amani
	 * @since 1.0
	 * @return les minutes et les secondes du minuteur
	 */
	@Override
	public String PrintDelayConvert() {
		long time = this.getDelayTimeMilliSeconde()/1000;
		int minute=0;
		int seconde=0;
		int heure=0;
		int jour=0;
		if(time>=60) {
			//on reccupere les minutes et les secondes
			minute = (int) time/60;
			seconde = (int) (time%60);
			if (minute>=60) {
				//on reccupere les heures et les minutes
				heure = (int) minute/60;
				minute = minute%60;
				if(heure>=24 ) {
					//on reccupere les jours et les heures
					jour = (int) heure/24;
					heure = heure%24;
					
				}
			}
		}else {
			seconde = (int)time;
		}
		//on formatte le texte, sil y a qu'un seul chiffre on rajoute des 0 sinon on renvoie le chiffre tel quel
		String s_seconde = String.valueOf(seconde);
		String s_minute = String.valueOf(minute);
		if( s_seconde.length() == 1) {
			s_seconde = "0"+s_seconde;
		}if( s_minute.length() == 1) {
			s_minute = "0"+s_minute;
		}
		return (s_minute+" : "+s_seconde);
	}
	/**
	 * La fonction permet d'annuler le minuteur
	 * @author Said El Amani
	 * @since 1.0
	 */
	public void cancel() {
		if(this.isActive()==false) {
			return; //si le minuteur est deja eteint on ne fait rien
		}
		//sinon on larrete 
		this.setActive(false);
		this.getTimeRun().cancel();
		//on change de timer pour une prochaine utilisation 
		this.setTimeRun(new Timer());
	}
	
	/**
	 * La fonction permet de lancer le minuteur
	 * @author Said El Amani
	 * @since 1.0
	 * @param _lblMinuteur est un JLabel servant � afficher les minutes et les secondes dans le JFrame
	 * @param panelMinuteur est un PanelMinuteur
	 */
	public void startMinuteur(JLabel _lblMinuteur, PanelMinuteur panelMinuteur) {
		if(this.isActive()==true) {
			return; //si le minuteur est deja lancer on ne fais rien
		}
		//sinon on lance le minuteur
		this.setActive(true);
		this.getTimeRun().schedule(new TimerTask(){
			@Override
			public void run() {
				//on actualise laffichage
				actualisation(_lblMinuteur, panelMinuteur);
			}
		},1000, 1000); //toutes les secondes on appel la fonction run
	}
	/**
	 * La fonction permet d'actualiser les minutes et les secondes du minuteur afficher � l'�cran
	 * @author Said El Amani
	 * @since 1.0
	 * @param _lblMinuteur un JLabel servant � afficher les minutes et les secondes dans el JFrame
	 * @param panelMinuteur est un PanelMinuteur
	 */
	private void actualisation(JLabel _lblMinuteur, PanelMinuteur panelMinuteur) {
		if(this.delayTimeMilliSeconde <= 0) {
			//sil ny a plus de temps, cest que le minuteur est fini
			//on envoie une notification a lutilisateur pour le lui dire
			try {
				//on lance une notification
				MyClockIT.ConstanteDashboard.MyClockManagerNotification.sendNotification(new Notification("ToolBox: Minuteur", "Votre minuteur c'est fini."));
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			};

			//on arrete le timer task
			//on arrete le minuteur est on actualise le visuel
			panelMinuteur.btnActionStopMinuteur();
		}else {
			//sinon on decremente et on affiche le temps restant
			this.setDelayTimeMilliSeconde(getDelayTimeMilliSeconde()-1000);//on actualise la valeur du delay en la decremente, lorsquelel arrive � 0 en arrete le minuteur
			_lblMinuteur.setText(this.PrintDelayConvert());//on change la valeur du label
		}
	}

}
