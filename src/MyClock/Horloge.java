package MyClock;

public abstract class Horloge {

	private int heures;
	private int minutes;
	private int secondes;
	
	/**
	 * L'object Horloge
	 * @param heures est un int representant l'heure.
	 * @param minutes est un int representant les minutes.
	 * @param secondes est un int representant les secondes.
	 */
	public Horloge(int heures, int minutes, int secondes) {
		this.heures = heures;
		this.minutes = minutes;
		this.secondes = secondes;
	}

	/**
	 * La fonction permet reccuperer l'heure de l'horloge
	 * @author Said El Amani
	 * @since 1.0
	 * @return un int representant l'heure de l'horloge
	 */
	public int getHeures() {
		return heures;
	}

	/**
	 * La fonction permet modifier l'heure de l'horloge
	 * @author Said El Amani
	 * @since 1.0 
	 * @param heures est un int
	 */
	public void setHeures(int heures) {
		this.heures = heures;
	}
	
	/**
	 * La fonction permet de reccuperer les minutes de l'horloge
	 * @author Said El Amani
	 * @since 1.0
	 * @return un int representant les minutes de l'alarme
	 */
	public int getMinutes() {
		return minutes;
	}

	/**
	 * La fonction permet de modifier les minutes de l'alarme
	 * @author Said El Amani
	 * @since 1.0
	 * @param minutes est un int
	 */
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	/**
	 * La fonction permet de reccuperrer les secondes de l'horloge
	 * @author Said El Amani
	 * @since 1.0
	 * @return un int representant les secondes
	 */
	public int getSecondes() {
		return secondes;
	}

	/**
	 * La fonction permet de modifier les secondes de l'horloge
	 * @author Said El Amani
	 * @since 1.0 
	 * @param secondes est ub int
	 */
	public void setSecondes(int secondes) {
		this.secondes = secondes;
	}

	/**
	 * Affiche les informations de l'alarme.
	 * @author Said El Amani
	 * @since 1.0
	 */
	public abstract String toString();
	/**
	 * Convertit et affiche le delay de l'alarme.
	 * @author Said El Amani
	 * @since 1.0
	 */
	public abstract String PrintDelayConvert();
}
