package MessagerieToolBox;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Dimension;
import java.awt.Window.Type;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Accueil.Accueil;
import InscriptionetConnexion.Connexion_Variablesession;
import PopuperreurToolBox.Popuperreur;

import java.awt.List;
import javax.swing.JMenuItem;
import java.awt.Panel;
import java.awt.Color;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import java.awt.TextArea;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.JTextArea;
import java.awt.CardLayout;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.border.MatteBorder;

public class Messagerie extends Connexion_Variablesession {

	/**
	 * @author Alan et Benjamin
	 * @version 2.0
	 * Launch the application.
	 */
	
	public JFrame frame;
	String namec = getNameC();
	static String desti = "";
	String namelist = "";
	private JTextField textField;
	int heure;
	int minute;
	int seconde;
	private ImageIcon image = new ImageIcon(new ImageIcon(ImageToolBox.whiteandblack)
			.getImage().getScaledInstance(807, 487, Image.SCALE_DEFAULT));


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Messagerie window = new Messagerie();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Messagerie() {
		M_Projet();
	}

	/**
	 * Initialize the contents of the frame.
	 *
	 * @throws SQLException
	 */
	private void M_Projet() {

		// menu principal
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 813, 513);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Border border = BorderFactory.createLineBorder(Color.WHITE);

		JPanel panpage = new JPanel();
		panpage.setBounds(0, 0, 807, 487);
		frame.getContentPane().add(panpage);

		JLabel page = new JLabel();
		page.setBounds(0, 0, 807, 487);

		panpage.setLayout(null);


				JLabel votremess1 = new JLabel("Votre message :");
				votremess1.setBounds(92, 141, 120, 22);
				panpage.add(votremess1);
				votremess1.setForeground(new Color(255, 255, 255));
				votremess1.setHorizontalAlignment(SwingConstants.CENTER);
				votremess1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 18));

		JPanel envoyeur = new JPanel();
		envoyeur.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		envoyeur.setBackground(Color.BLACK);
		envoyeur.setBounds(10, 97, 135, 30);
		panpage.add(envoyeur);
		envoyeur.setLayout(new CardLayout(0, 0));

		JLabel labenvoyeur = new JLabel(getNameC());
		envoyeur.add(labenvoyeur, "name_622090901324900");
		labenvoyeur.setHorizontalAlignment(SwingConstants.CENTER);
		labenvoyeur.setBackground(Color.BLACK);
		labenvoyeur.setForeground(Color.WHITE);
		labenvoyeur.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));


		// partie heure
		JPanel PanHeure = new JPanel();
		PanHeure.setBackground(Color.BLACK);
		PanHeure.setBounds(304, 42, 163, 39);
		panpage.add(PanHeure);
		PanHeure.setLayout(null);
		JLabel LabHeure = new JLabel("");
		LabHeure.setBounds(0, 0, 163, 39);
		PanHeure.add(LabHeure);
		LabHeure.setForeground(Color.WHITE);
		LabHeure.setFont(new Font("Gentium Book Basic", Font.PLAIN, 25));
		LabHeure.setHorizontalAlignment(SwingConstants.CENTER);

		Thread clock = new Thread() {
        /**
        * Mettre l'horloge dans l'appli
        */
			public void run() {
				while (true) {
					Calendar cal = new GregorianCalendar();
					heure = cal.get(Calendar.HOUR_OF_DAY);
					minute = cal.get(Calendar.MINUTE);
					seconde = cal.get(Calendar.SECOND);
					LabHeure.setText(String.valueOf(heure) + "h" + String.valueOf(minute) + "min"
							+ String.valueOf(seconde) + "s");
					try {
						sleep(500);
					} catch (InterruptedException ie) {
					}
				}
			}
		};
		clock.start();

		page.setIcon(image);

		//partie envoyeur
		JPanel PanEnvoyeuraff = new JPanel();
		PanEnvoyeuraff.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		PanEnvoyeuraff.setBackground(Color.BLACK);
		PanEnvoyeuraff.setBounds(10, 56, 135, 30);
		panpage.add(PanEnvoyeuraff);
		PanEnvoyeuraff.setLayout(new CardLayout(0, 0));

		JLabel lblEnvoyeur = new JLabel("Envoyeur :");
		lblEnvoyeur.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnvoyeur.setForeground(Color.WHITE);
		lblEnvoyeur.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));
		lblEnvoyeur.setBackground(Color.BLACK);
		PanEnvoyeuraff.add(lblEnvoyeur, "name_683425413069500");

		JPanel pantest = new JPanel();
		pantest.setBackground(Color.BLACK);
		pantest.setBounds(0, 421, 807, 66);
		panpage.add(pantest);
		pantest.setLayout(null);

		JButton envoi = new JButton("");
		envoi.setBounds(255, 8, 41, 41);
		envoi.setFont(new Font("Imprint MT Shadow", Font.PLAIN, 17));
		envoi.setBackground(Color.BLACK);
		envoi.setForeground(new Color(255, 255, 255));
		ImageIcon imageIcon3 = new ImageIcon(new ImageIcon(ImageToolBox.envoie).getImage()
		.getScaledInstance(41, 41, Image.SCALE_DEFAULT));
		envoi.setIcon(imageIcon3);
		envoi.setBorder(null);
		pantest.add(envoi);


		JPanel paneltestmessage = new JPanel();
		paneltestmessage.setBackground(Color.RED);
		paneltestmessage.setBounds(158, 11, 10, 10);
		pantest.add(paneltestmessage);

		JLabel testmessage = new JLabel("aucun message envoy\u00E9");
		testmessage.setHorizontalAlignment(SwingConstants.CENTER);
		testmessage.setForeground(Color.WHITE);
		testmessage.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		testmessage.setBounds(10, 17, 160, 38);
		pantest.add(testmessage);

		//partie message
		JTextArea message = new JTextArea("");
		message.setBounds(10, 174, 284, 236);
		panpage.add(message);
		message.setFont(new Font("Gentium Book Basic", Font.BOLD, 17));
		message.setForeground(Color.WHITE);
		message.setLineWrap(true);
		message.setBackground(Color.black);
		message.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));

		JPanel votremess = new JPanel();
		votremess.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		votremess.setBounds(10, 138, 284, 30);
		panpage.add(votremess);
		votremess.setBackground(new Color(0, 0, 0));

		//partie destinataire
		JTextArea destinataire = new JTextArea();
		destinataire.setBounds(159, 97, 135, 30);
		panpage.add(destinataire);
		destinataire.setFont(new Font("Gentium Book Basic", Font.BOLD, 17));
		destinataire.setForeground(Color.WHITE);
		destinataire.setLineWrap(true);
		destinataire.setBackground(Color.black);
		destinataire.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));

		JPanel ledesti = new JPanel();
		ledesti.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		ledesti.setBounds(159, 56, 135, 30);
		panpage.add(ledesti);
		ledesti.setBackground(new Color(0, 0, 0));
		ledesti.setLayout(new CardLayout(0, 0));

		JLabel labledesti = new JLabel("Destinataire :");
		ledesti.add(labledesti, "name_683435622182200");
		labledesti.setBackground(new Color(0, 0, 0));
		labledesti.setForeground(new Color(255, 255, 255));
		labledesti.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));
		labledesti.setHorizontalAlignment(SwingConstants.CENTER);

		//partie contact
		JPanel pancontact = new JPanel();
		pancontact.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		JScrollPane scrollcontact = new JScrollPane(pancontact);
		scrollcontact.setBounds(477, 56, 304, 354);
		panpage.add(scrollcontact);
		scrollcontact.getViewport().setOpaque(false);
		scrollcontact.setBackground(SystemColor.controlHighlight);
		pancontact.setLayout(new GridLayout(0, 1));

		scrollcontact.setOpaque(false);
		pancontact.setOpaque(false);

		//partie haut de l'appli pour naviguer dedans
		JPanel header = new JPanel();
		header.setBackground(new Color(0, 0, 0));
		header.setBounds(0, 0, 807, 45);
		panpage.add(header);
		header.setLayout(null);
		page.setIcon(image);
		panpage.add(page);
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC",
					"root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT `pseudo` FROM `inscription` WHERE 1");

			ArrayList<String> lines = new ArrayList<>();

			while (res.next()) {
				lines.add(res.getString("pseudo"));
            //insert dans l'ArrayList lines tout les pseudo qui sont dans inscription
			}

			ActionListener a2 = new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					String dest = destinataire.getText();
					boolean containsStr = lines.contains(dest);
					if (containsStr == true) {
						// Connexion � la bdd pour ajouter le message
						System.out.println("il existe");
						try {
							String message2 = namec + " : " + message.getText();
							Connection con = DriverManager.getConnection(
									"jdbc:mysql://localhost:3306/projet?serverTimezone=UTC", "root", "root");
							Statement st = con.createStatement();
							String sql = "INSERT INTO `message`(`Destinataire`, `envoyeur`, `message`) VALUES ('" + dest+ "','" + labenvoyeur.getText() + "','" + message2 + "')";

							st.executeUpdate(sql);

							paneltestmessage.setBackground(Color.green); //confirmation message envoy� panel = vert
							testmessage.setText("message envoy�"); //confirmation message envoy�
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							Popuperreur window = new Popuperreur();
							window.frame.setVisible(true); 
						}
					} else {
						System.out.println("n'existe pas");
						paneltestmessage.setBackground(Color.red); //confirmation erreur message panel = rouge
						testmessage.setText("Erreur : Destinataire"); //confirmation erreur message
					}
				}
			};
			envoi.addActionListener(a2); //envoi du message vers la bdd ("INSERT INTO `message`(`Destinataire`, `envoyeur`, `message`)")
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			Popuperreur window = new Popuperreur();
			window.frame.setVisible(true);
		}

		// liste des contacts de la personne connecte

		JButton b = new JButton();//bouton contact

		textField = new JTextField();
		textField.setForeground(new Color(255, 255, 255));
		textField.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));
		textField.setBackground(new Color(0, 0, 0));
		textField.setBounds(476, 8, 279, 26);
		header.add(textField);
		textField.setColumns(10);
		textField.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));

		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();

			ResultSet res = st.executeQuery("SELECT `pseudo` FROM `inscription` WHERE 1");
			ArrayList<String> lines = new ArrayList<>();

			while (res.next()) {
				lines.add(res.getString("pseudo"));
            //lines = tout les pseudos des utilisateurs inscrits
			}
			int index = 0;
			JButton[][] array = new JButton[lines.size()][2];
			for (int i = 0; i < lines.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < lines.size()) {

						b = new JButton("");
						b.setPreferredSize(new Dimension(150, 50));
						array[i][j] = b;
						pancontact.add(array[i][j]);
						b.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b.setBackground(Color.black);
						b.setForeground(new Color(255, 255, 255));
						array[i][j].setText(lines.get(index));
						index = index + 1;
						b.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
						//tableau des contacts /recuperation des pseudo de tout les utilisateurs/pseudo

						ActionListener a1 = new ActionListener() {
							public void actionPerformed(ActionEvent ae) {
								desti = ((AbstractButton) ae.getSource()).getText();
								System.out.println(desti);
								if (desti == ((AbstractButton) ae.getSource()).getText()) {
									Popupmessage window2 = new Popupmessage();
									window2.frame.setVisible(true);//ouvre popupmessage si le desti est bien un contact de l'appli
								}
							}
						};
						b.addActionListener(a1);

					}
				}
			}
		} catch (Exception e) {
			Popuperreur window = new Popuperreur();
			window.frame.setVisible(true);
		}
		// Bouton de recherche
		JButton rechercheC = new JButton("");
		rechercheC.setBackground(Color.BLACK);
		rechercheC.setBounds(760, 8, 25, 26);
		ImageIcon imageIcon2 = new ImageIcon(new ImageIcon(ImageToolBox.search).getImage()
				.getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		rechercheC.setIcon(imageIcon2);
		rechercheC.setBorder(null);
		header.add(rechercheC);
		// Bouton accueil
		JButton accueil = new JButton("");
		accueil.setForeground(Color.WHITE);
		accueil.setFont(new Font("Imprint MT Shadow", Font.PLAIN, 17));
		accueil.setBackground(Color.BLACK);
		accueil.setBounds(10, 8, 31, 31);
		header.add(accueil);
		ImageIcon imageIcon5 = new ImageIcon(new ImageIcon(ImageToolBox.accueil).getImage()
				.getScaledInstance(31, 31, Image.SCALE_DEFAULT));
		accueil.setIcon(imageIcon5);
		accueil.setBorder(null);

		JLabel nomappli = new JLabel("ToolBox");
		nomappli.setHorizontalAlignment(SwingConstants.CENTER);
		nomappli.setForeground(Color.WHITE);
		nomappli.setFont(new Font("Tw Cen MT", Font.PLAIN, 27));
		nomappli.setBounds(342, 8, 88, 31);
		header.add(nomappli);

		accueil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				    Accueil window2 = new Accueil();
					window2.frame.setVisible(true);
				    frame.dispose(); //retour � la page d'accueil de l'appli
			}
		});

		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();

			ResultSet res = st.executeQuery("SELECT `pseudo` FROM inscription WHERE 1");
			// String ligne = res.getString("colonne_1");
			ArrayList<String> lines = new ArrayList<>();

			while (res.next()) {
				lines.add(res.getString("pseudo"));

			}
			rechercheC.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					desti = textField.getText();
					boolean containsStr = lines.contains(textField.getText());
					if (containsStr == true) {
						Popupmessage window2 = new Popupmessage();
						window2.frame.setVisible(true); //recherche le contact pour ensuite ouvrir popupmessage au destinataire cibl�
					} 

				}
			});


		} catch (SQLException e) {

			Popuperreur window = new Popuperreur();
			window.frame.setVisible(true);
		}
	}

	/**
	 *
	 * @return desti
	 */
	public static String getdesti() {
		return desti;
	}
}

