package MessagerieToolBox;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.CardLayout;
import javax.swing.border.MatteBorder;

import PopuperreurToolBox.Popuperreur;

public class Popupmessage extends Messagerie {

	/**
	 * @author Alan et Benjamin
	 * @version 2.0
	 * Launch the application.
	 */
	
	JFrame frame;
	String personneco = getNameC();
	String desti = getdesti();
	private ImageIcon image = new ImageIcon(new ImageIcon("C:\\Users\\Benjamin\\Pictures\\japan.jpg")
			.getImage().getScaledInstance(807, 487, Image.SCALE_DEFAULT));
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Popupmessage window = new Popupmessage();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Popupmessage() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 779, 469);
		frame.getContentPane().setLayout(null);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(0, 0, 784, 437);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new MatteBorder(4, 4, 4, 4, (Color) new Color(0, 0, 0)));
		JScrollPane panelPane = new JScrollPane(panel);
		panelPane.setBounds(10, 47, 740, 368);
		panel_1.add(panelPane);
		panelPane.getViewport().setOpaque(false);
		panelPane.setBackground(SystemColor.controlHighlight);
		panel.setLayout(new GridLayout(0, 1));

		panelPane.getViewport().setOpaque(false);
		panelPane.setOpaque(false);
		panel.setOpaque(false);

		JLabel lblcontact = new JLabel("Message de " + namec + " avec " + desti + " : ");
		lblcontact.setForeground(Color.BLACK);
		lblcontact.setFont(new Font("Gentium Book Basic", Font.PLAIN, 27));
		lblcontact.setBounds(10, 11, 740, 33);
		panel_1.add(lblcontact);
		
		JLabel Wallpaper = new JLabel("");
		Wallpaper.setBounds(0, 0, 774, 437);
		panel_1.add(Wallpaper);
		Wallpaper.setIcon(image);

		JButton b = new JButton();

		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC",
					"root", "root");
			Statement st = con.createStatement();
			Statement st2 = con.createStatement();

			ResultSet res = st.executeQuery("SELECT message FROM message WHERE (Destinataire = '" + namec + "' AND envoyeur ='"
							+ desti + "') OR (Destinataire = '" + desti + "' AND envoyeur ='" + namec + "')");
			
			ArrayList<String> lines = new ArrayList<>();

			while (res.next()) {
				lines.add(res.getString("message"));//lines = message entre namec(users) et desti(contact)

			}

			int index = 0;
			JButton[][] array = new JButton[lines.size()][2];
			for (int i = 0; i < lines.size(); ++i) {
				for (int j = 0; j < 2; ++j) { // parcours (lignes,colonnes) de tableau array
					if (index < lines.size()) {

						b = new JButton("");
						b.setPreferredSize(new Dimension(150, 50));
						array[i][j] = b;
						panel.add(array[i][j]);
						b.setFont(new Font("Gentium Book Basic", Font.BOLD, 17));
						b.setBackground(SystemColor.control);
						b.setOpaque(false);
						b.setForeground(Color.black);
						array[i][j].setText(lines.get(index));
						index = index + 1;
						b.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)); //tableau des messages (insert tout les messages entre users et contact)
					
					} 
				}
			}

		} catch (Exception e) {
			Popuperreur window = new Popuperreur();
			window.frame.setVisible(true);
		}
	}
}
