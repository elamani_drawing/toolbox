package MyClockIT;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import MyClock.Minuteur;

public class PanelMinuteur extends JPanel {

	private ImageIcon imgImageMontreProgrammable = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImageMontreProgrammable).getImage().getScaledInstance(900, 700, Image.SCALE_DEFAULT));
	
	//on creer une referrence vers les elements qui seront modifier lors de l'interration de l'utilisateur avec lapplication
	private JTextField txtFMinuteMinuteur;
	private JTextField txtFSecondeMinuteur;
	private JLabel lblDeuxPoint;
	private JLabel lblMinuteIndication;
	private JLabel lblSecondeIndication;
	private JButton btnStartMinuteur;
	private JLabel lblTimeMinuteur;
	
	//un object minuteur
	private Minuteur minuteur = new Minuteur();

	/**
	 * Le constructeur de la classe PanelMinuteur
	 * @author Said El Amani
	 * @since 2.0
	 * @version 1.0
	 */
	public PanelMinuteur() {
		setBounds(0,0,740,633);
		setLayout(null);
		setVisible(true);
		setBackground(ConstanteDashboard.ColorBlueFonceContainer);
		
		//un label servant a afficher le temps restant du minuteur
		JLabel lblTimeMinuteur = new JLabel("00 : 00");
		lblTimeMinuteur.setForeground(Color.WHITE);
		lblTimeMinuteur.setFont(new Font("Tahoma", Font.PLAIN, 99));
		lblTimeMinuteur.setBounds(296, 151, 359, 239);
		add(lblTimeMinuteur);
		lblTimeMinuteur.setVisible(false);
		this.lblTimeMinuteur = lblTimeMinuteur;
		
		//la zone de saisie des minutes
		txtFMinuteMinuteur = new JTextField();
		txtFMinuteMinuteur.setFont(new Font("Tahoma", Font.PLAIN, 99));
		txtFMinuteMinuteur.setText("30");
		txtFMinuteMinuteur.setBounds(289, 207, 126, 120);
		add(txtFMinuteMinuteur);
		txtFMinuteMinuteur.setColumns(10);
		this.txtFMinuteMinuteur = txtFMinuteMinuteur;
		
		//la zonne de saisie des secondes
		txtFSecondeMinuteur = new JTextField();
		txtFSecondeMinuteur.setText("00");
		txtFSecondeMinuteur.setFont(new Font("Tahoma", Font.PLAIN, 99));
		txtFSecondeMinuteur.setColumns(10);
		txtFSecondeMinuteur.setBounds(492, 207, 126, 120);
		add(txtFSecondeMinuteur);
		this.txtFSecondeMinuteur = txtFSecondeMinuteur;
		//le bouton pour lancer le chronometre

		JButton btnStartMinuteur = new JButton("D\u00E9marrer");
		btnStartMinuteur.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnStartMinuteur.setBounds(367, 400, 176, 39);
		btnStartMinuteur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(minuteur.isActive()==false) {
					//on verifie que les valeurs des minutes et des secondes sont valides
					int nbr_minutes=0;
					int nbr_secondes=0;
					boolean erreur=false;
					String erreurMessage ="";
					try {
						//je convertis les valeurs
						nbr_minutes = Integer.parseInt(txtFMinuteMinuteur.getText());
						nbr_secondes = Integer.parseInt(txtFSecondeMinuteur.getText());
						//le maximum du minuteur est 30 mins 00 secondes, le minimum est 00 mins 10 secondes
							
						//les minutees sont compris entre 30 et 0 et que les secondes sont compris entre 59 et 0, on verifie que si ce nest pas le cas on renvoie une erreur en donnant les valeur -1
						if (nbr_minutes>30 || nbr_minutes <-1 || nbr_secondes >59 || nbr_secondes <0)
						{
							erreur =true;
							erreurMessage = "Les minutes doivent etre entre 30 et 0 et les secondes entres 59 et 0.";
							
						}
						//on verifie que si les minutes sont egal a 30 les secondes sont egal a 0, sinon lutilisateur pourais par exemple mettre 30 min 20 secondes 
						if(nbr_minutes ==30 && nbr_secondes !=0 ) {
							erreur =true;
							erreurMessage="Le minuteur ne peux pas d�passer 30 minutes 00 secondes.";
						}
						if(nbr_minutes ==0 && nbr_secondes <10) {
							//si lutilisateur met 00 minutes, il ne peut pas mettre moin de 10 secondes
							erreur =true;
							erreurMessage ="Vous ne pouvez pas cr�er un minuteur en dessous de 10 secondes.";
						}
						
					} catch (final NumberFormatException error) {
				        //il ya une erreur, surement lutilisateur a rentrer du texte ce qui a fais echouer la conversion
						erreur=true;
						erreurMessage ="Verifier votre saisie, vous ne pouvez mettre que des chiffres.";
				    }
					//sil y a eu une erreur
					if (erreur ==true) {
						nbr_minutes = -1;
						nbr_secondes = -1;		
						JOptionPane.showMessageDialog(btnStartMinuteur, erreurMessage, "Erreur", JOptionPane.PLAIN_MESSAGE);
					}else {
						//pas derreur on preparer le demarrage du minuteur
						
						// on change le texte du boutton pour dire qu'on peut annuler
						btnStartMinuteur.setText("Annuler");
						//on prepare laffichage en changeant le texte du label qui montre le decompte, on le rendant visible, et en effacant les jtextfields et le label ":" 
						
						//on efface de lecran les jtextfield
						txtFMinuteMinuteur.setVisible(false);
						txtFSecondeMinuteur.setVisible(false);
						lblDeuxPoint.setVisible(false);
						//si le minuteur est pas encore lancer est quon clique sur le bouton on lance le minuteur
						btnActionStartMinuteur(nbr_minutes, nbr_secondes, lblTimeMinuteur);
					}

				}else {
					//si le chrono est deja lancer est quon clique sur le bouton on arrete le minuteur
					btnActionStopMinuteur();
				}
			}
		});
		add(btnStartMinuteur);
		this.btnStartMinuteur = btnStartMinuteur;
		//les 2 points pour separer les minutes des secondes
		lblDeuxPoint = new JLabel(":");
		lblDeuxPoint.setForeground(Color.WHITE);
		lblDeuxPoint.setFont(new Font("Tahoma", Font.PLAIN, 99));
		lblDeuxPoint.setBounds(434, 192, 48, 127);
		add(lblDeuxPoint);
		this.lblDeuxPoint =lblDeuxPoint;

		//les indicateurs visuels
		lblMinuteIndication = new JLabel("minute(s)");
		lblMinuteIndication.setForeground(Color.WHITE);
		lblMinuteIndication.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMinuteIndication.setBounds(311, 327, 104, 22);
		add(lblMinuteIndication);
		
		lblSecondeIndication = new JLabel("seconde(s)");
		lblSecondeIndication.setForeground(Color.WHITE);
		lblSecondeIndication.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblSecondeIndication.setBounds(514, 327, 104, 22);
		add(lblSecondeIndication);
		
		//laffichage lorsque le chronometre est lancer
		
		//limage de fond
		JLabel lblFondMinuteur = new JLabel("");
		lblFondMinuteur.setFont(new Font("Tahoma", Font.PLAIN, 27));
		lblFondMinuteur.setBackground(new Color(112, 112, 206));
		lblFondMinuteur.setBounds(0, 0, 740, 633);
		lblFondMinuteur.setIcon(imgImageMontreProgrammable);
		add(lblFondMinuteur);
	}
	
	/**
	 * La fonction permet de lancer le minuteur
	 * @author Said El Amani
	 * @since 2.0
	 * @param minutes est un int representant les minutes du minuteur
	 * @param secondes est un int representant les secondes du minuteur
	 * @param _lblMinuteur est le label servant a afficher le temps du minuteur 
	 */
	private void btnActionStartMinuteur(int minutes, int secondes, JLabel _lblMinuteur) {
		//je renseigne les donn�es du minuteur
		this.minuteur.setMinutes(minutes);
		this.minuteur.setSecondes(secondes);
		//je precise le delay dattente, on convertit les minutes en secondes, ensuite le tout en milliseconde
		this.minuteur.setDelayTimeMilliSeconde((minutes*60+ secondes)*1000);
		_lblMinuteur.setText(this.minuteur.printTime());
		//on affiche le label
		_lblMinuteur.setVisible(true);
		//tout est pres, donc je peux lancer le chrono
		this.minuteur.startMinuteur(_lblMinuteur, this);

	}
	
	/**
	 * La fonction permet d'arreter le minuteur
	 * @author Said El Amani
	 * @since 2.0
	 */
	public void btnActionStopMinuteur() {
		//on efface de lecran le label 
		lblTimeMinuteur.setVisible(false);
		//on raffiche les jtextfield et le label ":"
		txtFMinuteMinuteur.setVisible(true);
		txtFSecondeMinuteur.setVisible(true);
		lblDeuxPoint.setVisible(true);
		// on change le texte pour dire quon peut demarrer
		btnStartMinuteur.setText("Demarrer");
		//jarrete le minuteur
		this.minuteur.cancel();
	}
	
}
