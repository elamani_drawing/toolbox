package MyClockIT;

import java.awt.Color;
import java.nio.file.Paths;

import MyClock.AlarmeManager;
import NotifyManager.NotificationManager;

public interface ConstanteDashboard {
	//manager
	NotificationManager MyClockManagerNotification = new NotificationManager();
	AlarmeManager ManagerAlarme = new AlarmeManager(-1, "jdbc:mysql://localhost:3306/projet", "root", "root");//changer 0 par l'id de lutilisateur
	
	//couleur
	Color ColorBlueFonceBackGround = new Color(0, 0, 0);
	Color ColorBlueFoncePanelMenu = new Color(0, 0, 0);
	Color ColorBlueFonceBoutonNoHover = new Color(33, 33, 33);
	Color ColorBlueFonceContainer = new Color(51, 51, 51);
	Color ColorBlueFonceAlarme = new Color(246, 246, 255  );
	Color ColorRed= new Color(246, 0, 0  );
	
	//Les couleurs pendant les evenements souris sur le Panel du PanelMenu
	//la couleur lorsque la souris entre dans le panel
	Color ColorMouseEntered = new Color(23, 23, 23);
	//la couleur lorsque la souris sort du panel
	Color ColorMouseExited = ColorBlueFonceBoutonNoHover;
	//la couleur lorsque la souris clique sur le panel
	Color ColorMousePressed = new Color(137, 137, 166);
	//la couleur lorsque la souris apres avoir cliquer du panel
	Color ColorMouseRealised = ColorMouseEntered;
	

	//path
	String cheminProjet = System.getProperty("user.dir");
	
	//image
	//les logos des rubrique dans le panelMenu
	String linkImageMyClock  = Paths.get(cheminProjet, "media", "logoMyClock.png").toString();
	String linkImageMesAlarmes  = Paths.get(cheminProjet, "media", "logoMesAlarmes.png").toString();
	String linkImageLogoChronometre  = Paths.get(cheminProjet, "media", "logoChronometre.png").toString();
	String linkImagelogoCreerAlarmes  = Paths.get(cheminProjet, "media", "logoCreerAlarmes.png").toString();
	String linkImagelogoMinuteur  = Paths.get(cheminProjet, "media", "logoMinuteur.png").toString();
	String linkImagelogoClose  = Paths.get(cheminProjet, "media", "logoClose.png").toString();
	
	//le logo des alarmes dans Mes Alarmes

	String linkImageLogoAlarmesMesAlarmes  = Paths.get(cheminProjet, "media", "logoMyClock.png").toString();
	
	//les fleches dans Mes Alarmes
	String linkImageAlarmeArrowLeft  = Paths.get(cheminProjet, "media", "arrowLeft.png").toString();
	String linkImageAlarmeArrowRight  = Paths.get(cheminProjet, "media", "arrowRight.png").toString();
	
	
	//image
	String linkImageMontreProgrammable  = Paths.get(cheminProjet, "media", "imageMontreProgrammable.jpg").toString();
	
	//pannel
	PanelMesAlarmes panelMesAlarmes = new PanelMesAlarmes();
	
}
