package MyClockIT;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import MyClock.Alarme;
import MyClock.AlarmeManager;
import MyClock.ClockException;
import MyClock.Constante;

public class PanelCreerAlarmes extends JPanel {
	private ImageIcon imgImageMontreProgrammable = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImageMontreProgrammable).getImage().getScaledInstance(1000, 800, Image.SCALE_DEFAULT));
	private JTextField txtFHeureAlarme;
	private JTextField txtFMinuteAlarme;
	private JTextField txtFTitreAlarme;
	private JLabel lblErreur;
	private String regulariteAlarme=MyClock.Constante.REGULARITE_ALARME.get(0);//on initialise la regularite a Une seule fois
	private String jourAlarme =MyClock.Constante.JOUR_DE_LA_SEMAINE.get(1);//on initialise la regularite a Dimanche
	private int heure=0;
	private int minute =0;
	private String titre="";
	private String messageErreur="";
	
	private AlarmeManager ManagerAlarme = ConstanteDashboard.ManagerAlarme;//on charge notre manager d'alarme
	
	/**
	 * Create the panel.
	 */
	public PanelCreerAlarmes() {
		setBounds(0,0,740,633);
		setLayout(null);
		setVisible(true);
		setBackground(ConstanteDashboard.ColorBlueFonceContainer);
		
		//les zonnes de saisie textfield
		
		//la zone de saisie des Heures
		txtFHeureAlarme = new JTextField();
		txtFHeureAlarme.setFont(new Font("Tahoma", Font.PLAIN, 99));
		txtFHeureAlarme.setText("00");
		txtFHeureAlarme.setBounds(220, 210, 126, 120);
		add(txtFHeureAlarme);
		txtFHeureAlarme.setColumns(10);
		
		//la zonne de saisie des secondes
		txtFMinuteAlarme = new JTextField();
		txtFMinuteAlarme.setText("00");
		txtFMinuteAlarme.setFont(new Font("Tahoma", Font.PLAIN, 99));
		txtFMinuteAlarme.setColumns(10);
		txtFMinuteAlarme.setBounds(450, 210, 126, 120);
		add(txtFMinuteAlarme);
		
		//la zone de saisie du titre
		txtFTitreAlarme = new JTextField();
		txtFTitreAlarme.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtFTitreAlarme.setColumns(10);
		txtFTitreAlarme.setBounds(260, 140, 280, 40);
		add(txtFTitreAlarme);
		
	
		
		//les selecteurs
		JComboBox jcbSelectDay = new JComboBox();
		jcbSelectDay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//on reccupere la valeur selectionner par lutilisateur
				jourAlarme= jcbSelectDay.getSelectedItem().toString();
			}
		});
		jcbSelectDay.setFont(new Font("Tahoma", Font.PLAIN, 24));
		//je met les elements que lutilisateur peut selectionner
		jcbSelectDay.setModel(new DefaultComboBoxModel(this.getListJour()));
		jcbSelectDay.setBounds(220, 370, 356, 40);
		add(jcbSelectDay);
		
		JComboBox jcbSelectRegularite = new JComboBox();
		jcbSelectRegularite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//on reccupere la valeur selectionner par lutilisateur
				regulariteAlarme= jcbSelectRegularite.getSelectedItem().toString();
			}
		});
		jcbSelectRegularite.setModel(new DefaultComboBoxModel(MyClock.Constante.REGULARITE_ALARME.toArray()));
		jcbSelectRegularite.setFont(new Font("Tahoma", Font.PLAIN, 24));
		jcbSelectRegularite.setBounds(220, 440, 356, 40);
		add(jcbSelectRegularite);

		
		//les indicateurs visuels pour dire � lutilisateur ce quil doit saisir
		
		JLabel lblIndicateurTitre = new JLabel("Saisir le titre de l'alarme:");
		lblIndicateurTitre.setHorizontalAlignment(SwingConstants.CENTER);
		lblIndicateurTitre.setForeground(Color.WHITE);
		lblIndicateurTitre.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblIndicateurTitre.setBounds(220, 108, 356, 22);
		add(lblIndicateurTitre);
		
		JLabel lblIndicateurHeure = new JLabel("Heure(s)::");
		lblIndicateurHeure.setForeground(Color.WHITE);
		lblIndicateurHeure.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblIndicateurHeure.setBounds(220, 190, 126, 22);
		add(lblIndicateurHeure);
		
		JLabel lblIndicateurMinute = new JLabel("Minute(s) :");
		lblIndicateurMinute.setForeground(Color.WHITE);
		lblIndicateurMinute.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblIndicateurMinute.setBounds(450, 190, 126, 22);
		add(lblIndicateurMinute);
		
		JLabel lblIndicateurJour = new JLabel("Jour : ");
		lblIndicateurJour.setForeground(Color.WHITE);
		lblIndicateurJour.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblIndicateurJour.setBounds(220, 340, 356, 22);
		add(lblIndicateurJour);
		
		JLabel lblIndicateurRegularite = new JLabel("Frequence de l'alarme:");
		lblIndicateurRegularite.setForeground(Color.WHITE);
		lblIndicateurRegularite.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblIndicateurRegularite.setBounds(220, 420, 350, 22);
		add(lblIndicateurRegularite);
		
		//le bouton de creation
		JButton jbCreerAlarme = new JButton("Cr\u00E9er");
		jbCreerAlarme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				creerAlarme(); 
			}
		});
		jbCreerAlarme.setFont(new Font("Tahoma", Font.PLAIN, 20));
		jbCreerAlarme.setBounds(320, 522, 150, 40);
		add(jbCreerAlarme);
		
		//un label qui servait afficher les erreurs, et maintenant sert de refference pour le JOptionPane qui previent l'utilisateur de son erreur
		this.lblErreur = new JLabel("");
		lblErreur.setForeground(Color.WHITE);
		lblErreur.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblErreur.setHorizontalAlignment(SwingConstants.CENTER);
		lblErreur.setBounds(20, 10, 700, 40);
		add(lblErreur);
	
		
		
		//l'image de fond
		JLabel lblFondMinuteur = new JLabel("");
		lblFondMinuteur.setFont(new Font("Tahoma", Font.PLAIN, 27));
		lblFondMinuteur.setBackground(new Color(112, 112, 206));
		lblFondMinuteur.setBounds(-100, 0, 1000, 633);
		lblFondMinuteur.setIcon(imgImageMontreProgrammable);
		add(lblFondMinuteur);
		
		
	}
	

	/**
	 * La fonction permet de verifier les �l�ments renseigner dans l'alarmes titre, heure etc. avant de cr�e l'alarmes correspondantes
	 * @author Said El Amani
	 * @since 2.0
	 */
	private void creerAlarme() {
		boolean erreur =false;
		Alarme alarme = null;
		this.titre = this.txtFTitreAlarme.getText();

		try {
			//on reccupere les valeurs et on essaye de les converir en en nombre 
			this.heure = Integer.parseInt(this.txtFHeureAlarme.getText());
			this.minute = Integer.parseInt(this.txtFMinuteAlarme.getText());
		}catch(Exception e) {
			//il y a une erreur, lutilisateur a surement mit une letre ou une chaine non valide
			erreur = true;
			this.messageErreur ="Les heures et les minutes doivent etre des chiffres.";
		}
		//sil ny a pas eu derreur on verifie
		//on verifie les donn�es de l'alarme
		if(erreur ==false) {
			try {
				//on fait les verification sur les donn�es
				this.ManagerAlarme.verificationDonneeAlarme(this.heure, this.minute, this.jourAlarme, this.titre, this.regulariteAlarme);
			} catch (ClockException error) {
				this.messageErreur = error.getMessage();
				erreur = true;
			}
		}
		if (erreur==false){
			//on peut creer notre alarme
			try {
				//this.ManagerAlarme.addAlarme(this.heure, this.minute, this.jourAlarme, this.titre, regulariteAlarme);
				if(this.regulariteAlarme == Constante.REGULARITE_ALARME.get(0)) {
					//pour rappel si lindice du jour d'aujourdhui est inferrieur � celui du jour selectionn�, lalarme renvoie une erreur car le jour se trouve dans le passer 
					//le premier jour de la semaine ici est le dimanche est le dernier est samedi, donc pour exemple si on est jeudi, et que l'alarme de regularite, lutilisateur creer une alarme de regularite une seul fois pour le mardi, cela renverra une erreur
					//cest la regularite une seul fois
					alarme = this.ManagerAlarme.creerAlarmeBasique(this.jourAlarme, this.regulariteAlarme,  this.heure,  this.minute,  this.titre, false);
				}else if(this.regulariteAlarme == Constante.REGULARITE_ALARME.get(1)) {
					//cest la regulariter tout les jours 
					alarme = this.ManagerAlarme.creerAlarmeQuotidien(this.jourAlarme, this.regulariteAlarme,  this.heure,  this.minute,  this.titre, false);
				}else {
					//cest la regulariter toute les semaines ou autre chose qui retournera une erreur
					alarme = this.ManagerAlarme.creerAlarmeSemaine(this.jourAlarme, this.regulariteAlarme,  this.heure,  this.minute,  this.titre, false);
				}
			}catch(ClockException error) {
				this.messageErreur = error.getMessage();
				erreur = true;
			}
		}
		if(erreur==true) {
			//on previent l'utilisateur de son erreur
			JOptionPane.showMessageDialog(lblErreur, this.messageErreur, "Erreur", JOptionPane.PLAIN_MESSAGE);
		}else {
			//on ajoute l'alarme dans la liste des alarmes
			this.ManagerAlarme.addAlarme(alarme);
			//on ajoute lalarme dans la bdd
			try {
				this.ManagerAlarme.reqAddAlarme(alarme);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			try {
				alarme.setIdAlarme(this.ManagerAlarme.reqGetIdLastAlarm());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}//on reccupere lid de la dernier alarme a avoir etait creer et on le lui donne
			//on affiche un message � l'utilisateur
			
			JOptionPane.showMessageDialog(lblErreur, "Votre alarme � bien �t� cr�er.", "Succes", JOptionPane.PLAIN_MESSAGE);
		}
	}
	

	/**
	 * La fonction permet de reccuperer une liste des jours de la semaine, sont le premier element de notre liste qui est ""
	 * @author Said El Amani
	 * @since 2.0
	 * @return un tableau de string contenant les jours de la semaine
	 */
	private String[] getListJour() {
		String liste[]= {"", "","","","","",""}; 
		for(int i=1; i<MyClock.Constante.JOUR_DE_LA_SEMAINE.size(); i++) {
			//je recopie la liste sans prendre le premier element
			liste[i-1] = MyClock.Constante.JOUR_DE_LA_SEMAINE.get(i);
		}
		//apres avoir fini de recopier les elements qui nosu interresse on la retourne
		return liste;
	}
}
