package MyClockIT;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Color;


import java.util.Timer;
import java.util.TimerTask;

import MyClock.Chronometre;

import javax.swing.JButton;

public class PanelChronometre extends JPanel {

	private ImageIcon imgImageMontreProgrammable = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImageMontreProgrammable).getImage().getScaledInstance(900, 700, Image.SCALE_DEFAULT));

	private JLabel lblMinuteIndication;
	private JLabel lblSecondeIndication;

	private JButton btnStartChronometre=null;
	//un object chronometre
	private Chronometre chronometre = new Chronometre();

	/**
	 * Le constructeur de la classe PanelChronometre
	 * @author Said El Amani
	 */
	public PanelChronometre() {
		setBounds(0,0,740,633);
		setLayout(null);
		setVisible(true);
		setBackground(ConstanteDashboard.ColorBlueFonceContainer);		

		//le label qui va servir a afficher les valeurs du chronometre
		JLabel lblMinuteChronometre = new JLabel("00 : 00");
		lblMinuteChronometre.setForeground(Color.WHITE);
		lblMinuteChronometre.setFont(new Font("Tahoma", Font.PLAIN, 99));
		lblMinuteChronometre.setBounds(296, 151, 359, 239);
		add(lblMinuteChronometre);
		
		
		//le bouton pour lancer le chronometre
		JButton btnStartChronometre = new JButton("D\u00E9marrer"); //par defaut le message du bouton est demarrer
		btnStartChronometre.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnStartChronometre.setBounds(367, 400, 176, 39);
		btnStartChronometre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(chronometre.isActive()==false) {
					// on change le texte du boutton pour dire quon peut annuler
					btnStartChronometre.setText("Annuler");

					//si le chrono est pas encore lancer est quon clique sur le bouton on lance le chrono
					btnActionStartChronometre(lblMinuteChronometre);
				}else {
					//si le chrono est deja lancer est quon clique sur le bouton on arrete le chrono
					btnActionStopChronometre();
					// on change le texte pour dire quon peut demarrer
					btnStartChronometre.setText("Demarrer");
				}
			}
		});
		add(btnStartChronometre);
		this.btnStartChronometre = btnStartChronometre;

		//les indicateurs visuels
		lblMinuteIndication = new JLabel("minute(s)");
		lblMinuteIndication.setForeground(Color.WHITE);
		lblMinuteIndication.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMinuteIndication.setBounds(311, 327, 104, 22);
		add(lblMinuteIndication);
		
		lblSecondeIndication = new JLabel("seconde(s)");
		lblSecondeIndication.setForeground(Color.WHITE);
		lblSecondeIndication.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblSecondeIndication.setBounds(514, 327, 104, 22);
		add(lblSecondeIndication);
		
		//laffichage lorsque le chronometre est lancer
		
		//limage de fond
		JLabel lblFondChronometre = new JLabel("");
		lblFondChronometre.setFont(new Font("Tahoma", Font.PLAIN, 27));
		lblFondChronometre.setBackground(new Color(112, 112, 206));
		lblFondChronometre.setBounds(0, 0, 740, 633);
		lblFondChronometre.setIcon(imgImageMontreProgrammable);
		add(lblFondChronometre);
		
	}
	
	private void btnActionStartChronometre(JLabel _lblMinuteChronometre) {
		//je lance le chrono
		this.chronometre.start();
		
		Timer timerChronometre = new Timer();
		timerChronometre.schedule(new TimerTask(){
			@Override
			public void run() {
				//on recalcul le temps
				actualisation(_lblMinuteChronometre);
				if(chronometre.isActive()==false) {
					//si on a eteint le chronometre 
					
					//on remet le label a 0
					_lblMinuteChronometre.setText("00 : 00");
					//on remet le delay a 0 
					chronometre.setDelayTimeMilliSeconde(0);
					//on arrete le chrono 
					timerChronometre.cancel();
				}
			}
		},1000, 1000);//toute les 1 secondes jactive mon timer
	}

	/**
	 * La fonction permet d'actualiser le label servant � afficher les minutes et les secondes du minuteur
	 * @author Said El Amani
	 * @since 2.0
	 * @param _lblMinuteChronometre est le label servant � afficher les minutes et les secondes du minuteur
	 */
	private void actualisation(JLabel _lblMinuteChronometre) {
		chronometre.addDelayTimeMilliSeconde(1000);//on actualise la valeur du delay ecouler
		_lblMinuteChronometre.setText(chronometre.PrintDelayConvert()); //on change la valeur du label

		if(chronometre.getDelayTimeMilliSeconde()>=30*60*1000) {
			//le chronometre peut atteindre au maximum 30 min
			this.btnActionStopChronometre();//on arrete le chronometre
			chronometre.setActive(false);//on change letat du chronometre
			this.btnStartChronometre.setText("Demarrer"); //on change le texte
			//on dit a lutilisateur pourquoi on a arreter son chronometre
			JOptionPane.showMessageDialog(_lblMinuteChronometre, "Le temps maximal du chronometre est de 30 min, nous arretons donc votre chronometre", "Erreur", JOptionPane.PLAIN_MESSAGE);
		}
	}
	/**
	 * La fonction permet de stoper le chronometre
	 * @author Said El Amani
	 * @since 2.0
	 */
	private void btnActionStopChronometre() {
		//jarrete le chrono
		this.chronometre.stop();
	}
}
