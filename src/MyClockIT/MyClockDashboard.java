package MyClockIT;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.border.LineBorder;

import Accueil.Accueil;

import javax.swing.JLabel;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Font;

public class MyClockDashboard extends JFrame {

	private JPanel contentPane;
	
	//image
	//les logos des panel de navigation
	private ImageIcon imgLogoMyClock = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImageMyClock).getImage().getScaledInstance(130, 130, Image.SCALE_DEFAULT));
	private ImageIcon imgLogoCreerAlarme = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImagelogoCreerAlarmes).getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
	private ImageIcon imgLogoMesAlarmes = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImageMesAlarmes).getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
	private ImageIcon imgLogoMinuteur = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImagelogoMinuteur).getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
	private ImageIcon imgLogoChronometre = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImageLogoChronometre).getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
	private ImageIcon imgLogoClose = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImagelogoClose).getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
	
	//panel
	//les panels ayant le contenu de chaque rubrique
	private PanelChronometre panelChronometre = new PanelChronometre();
	private PanelCreerAlarmes panelCreerAlarmes = new PanelCreerAlarmes();
	private PanelMesAlarmes panelMesAlarmes = ConstanteDashboard.panelMesAlarmes;
	private PanelMinuteur panelMinuteur = new PanelMinuteur();
	private PanelMinuteur panelRetour = null;

	/**
	 * Le constructeur de la classe MyClockDashboard
	 * @param id est l'id de l'utilisateur connecter
	 */
	public MyClockDashboard(int id) {
		ConstanteDashboard.ManagerAlarme.setUserID(id);//on met lid de lutilsiateur connecter
		ConstanteDashboard.ManagerAlarme.reStart();//on relance lapplication 
		panelMesAlarmes.updatePanelMesAlarmes(-1);//on actualise laffichage
		
		
		//je met la couleur de la page en bleu fonce par defaut
		setBackground(ConstanteDashboard.ColorBlueFonceBackGround);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1080, 720);
		//on centre lapplication a lecran
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		//je met la couleur de fond du panel en bleu fonce 
		contentPane.setBackground(ConstanteDashboard.ColorBlueFonceBackGround);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//la boite contenant les boutons de navigation
		JPanel panelMenu = new JPanel();
		panelMenu.setBounds(0, 0, 280, 696);
		panelMenu.setBackground(ConstanteDashboard.ColorBlueFoncePanelMenu);
		contentPane.add(panelMenu);
		panelMenu.setLayout(null);
		
		//le logo
		JLabel lblLogoHorloge = new JLabel("");
		lblLogoHorloge.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogoHorloge.setBounds(25, 29, 232, 153);
		lblLogoHorloge.setIcon(this.imgLogoMyClock);
		panelMenu.add(lblLogoHorloge);
		
		
		//les boutons de navigation dans le panel menu
		
		//le panel qui sert a charger toutes les alarmes de lutilisateurs
		//un panel qui va contenir le texte et le logo de la rubrique
		JPanel PanelMesAlarmes = new JPanel();
		//on observe les evenements sur le panel, et on agit en consequence
		PanelMesAlarmes.addMouseListener(new MyClockPanelButtonMousseAdapterEvent(PanelMesAlarmes, this.panelMesAlarmes, this));
		PanelMesAlarmes.setBounds(0, 197, 281, 52);
		panelMenu.add(PanelMesAlarmes);
		PanelMesAlarmes.setBackground(ConstanteDashboard.ColorBlueFonceBoutonNoHover);
		PanelMesAlarmes.setLayout(null);
		
		//le texte pour indiquer a quoi sert la rubrique
		JLabel lblMesAlarmes = new JLabel("MES ALARMES");
		lblMesAlarmes.setForeground(Color.WHITE);
		lblMesAlarmes.setBackground(Color.WHITE);
		lblMesAlarmes.setFont(new Font("Dubai", Font.BOLD, 20));
		lblMesAlarmes.setBounds(86, 10, 193, 32);
		PanelMesAlarmes.add(lblMesAlarmes);
		
		//un label qui va servir a mettre le logo 
		JLabel lblLogoMesAlarme = new JLabel("");
		lblLogoMesAlarme.setBounds(30, 10, 74, 32);
		lblLogoMesAlarme.setIcon(this.imgLogoMesAlarmes);
		//On lajoute a son pannel parrent 
		PanelMesAlarmes.add(lblLogoMesAlarme);
		

		//le panel qui sert a charger le contenu pour pouvoir creer une alarme
		JPanel PanelCreeAlarme = new JPanel();
		PanelCreeAlarme.addMouseListener(new MyClockPanelButtonMousseAdapterEvent(PanelCreeAlarme, this.panelCreerAlarmes, this));
		PanelCreeAlarme.setBounds(0, 251, 281, 52);
		panelMenu.add(PanelCreeAlarme);
		PanelCreeAlarme.setBackground(ConstanteDashboard.ColorBlueFonceBoutonNoHover);
		PanelCreeAlarme.setLayout(null);
		
		JLabel lblCreerAlarme = new JLabel("ALARME");
		lblCreerAlarme.setForeground(Color.WHITE);
		lblCreerAlarme.setFont(new Font("Dubai", Font.BOLD, 20));
		lblCreerAlarme.setBackground(Color.WHITE);
		lblCreerAlarme.setBounds(88, 10, 193, 32);
		PanelCreeAlarme.add(lblCreerAlarme);
		
		JLabel lblLogoAlarme = new JLabel("");
		lblLogoAlarme.setBounds(30, 10, 74, 32);
		lblLogoAlarme.setIcon(this.imgLogoCreerAlarme);
		PanelCreeAlarme.add(lblLogoAlarme);
		

		//le panel qui sert a charger le contenu pour lancer un minuteur
		JPanel PanelCreeMinuteur = new JPanel();
		PanelCreeMinuteur.addMouseListener(new MyClockPanelButtonMousseAdapterEvent(PanelCreeMinuteur, this.panelMinuteur, this));
		PanelCreeMinuteur.setBounds(0, 305, 281, 52);
		panelMenu.add(PanelCreeMinuteur);
		PanelCreeMinuteur.setBackground(ConstanteDashboard.ColorBlueFonceBoutonNoHover);
		PanelCreeMinuteur.setLayout(null);
		
		JLabel lblCreerMinuterie = new JLabel("MINUTEUR");
		lblCreerMinuterie.setForeground(Color.WHITE);
		lblCreerMinuterie.setFont(new Font("Dubai", Font.BOLD, 20));
		lblCreerMinuterie.setBackground(Color.WHITE);
		lblCreerMinuterie.setBounds(88, 10, 193, 32);
		PanelCreeMinuteur.add(lblCreerMinuterie);
		
		JLabel lblLogoMinuteur = new JLabel("");
		lblLogoMinuteur.setBounds(30, 10, 74, 32);
		lblLogoMinuteur.setIcon(this.imgLogoMinuteur);
		PanelCreeMinuteur.add(lblLogoMinuteur);
		
		//le panel qui sert a charger le contenu chronometre
		JPanel PanelCreeChrono = new JPanel();
		PanelCreeChrono.addMouseListener(new MyClockPanelButtonMousseAdapterEvent(PanelCreeChrono, this.panelChronometre, this));
		PanelCreeChrono.setBounds(0, 359, 281, 52);
		panelMenu.add(PanelCreeChrono);
		PanelCreeChrono.setBackground(ConstanteDashboard.ColorBlueFonceBoutonNoHover);
		PanelCreeChrono.setLayout(null);
		
		JLabel lblCreerChronometre = new JLabel("CHRONOMETRE");
		lblCreerChronometre.setForeground(Color.WHITE);
		lblCreerChronometre.setFont(new Font("Dubai", Font.BOLD, 20));
		lblCreerChronometre.setBackground(Color.WHITE);
		lblCreerChronometre.setBounds(88, 10, 193, 32);
		PanelCreeChrono.add(lblCreerChronometre);
		
		JLabel lblLogoChronometre = new JLabel("");
		lblLogoChronometre.setBounds(30, 10, 74, 32);
		lblLogoChronometre.setIcon(this.imgLogoChronometre);
		PanelCreeChrono.add(lblLogoChronometre);
		
		JPanel PanelClose = new JPanel();
		PanelClose.addMouseListener(new MyClockPanelButtonMousseAdapterEvent(PanelClose, this.panelRetour, this));
		
		PanelClose.setLayout(null);
		PanelClose.setBackground(ConstanteDashboard.ColorBlueFonceBoutonNoHover);
		PanelClose.setBounds(0, 414, 281, 52);
		panelMenu.add(PanelClose);
		
		JLabel lblClose = new JLabel("ACCUEIL");
		lblClose.setForeground(Color.WHITE);
		lblClose.setFont(new Font("Dubai", Font.BOLD, 20));
		lblClose.setBackground(Color.WHITE);
		lblClose.setBounds(88, 10, 193, 32);
		PanelClose.add(lblClose);
		
		JLabel lblLogoClose = new JLabel("");
		lblLogoClose.setBounds(30, 10, 74, 32);
		lblLogoClose.setIcon(imgLogoClose);
		PanelClose.add(lblLogoClose);
		
		//je crrer un panel qui va representer le menu
		JPanel panelMainContainer = new JPanel();
		panelMainContainer.setBounds(290, 25, 740, 633);
		panelMainContainer.setLayout(null);
		contentPane.add(panelMainContainer);
		
		//on ajoute les panels ayant chaque contenu de chaque categorie dans le PanelMainContainer 
		panelMainContainer.add(this.panelMesAlarmes);
		panelMainContainer.add(this.panelCreerAlarmes);
		panelMainContainer.add(this.panelMinuteur);
		panelMainContainer.add(this.panelChronometre);
		
		//on efface tout les panels et on en active un
		//par defaut on affiche les alarmes de lutilisateurs connecter
		updatePanelMainContainerWhenClicked(this.panelMesAlarmes);
		
	}
	/**
	 * La fonction permet de reccuperrer le PanelMesAlarmes
	 * @author Said El Amani
	 * @since 2.0
	 * @return l'object panelMesAlarmes de MyClocDashboard
	 */
	public PanelMesAlarmes getPanelMesAlarmes() {
		return this.panelMesAlarmes;
	}
	/**
	 * Desactive tout les pannels dans le PanelMainContainer et active le panel selectionner dans le Panel Menu
	 * @author Said El Amani
	 * @since 2.0
	 * @param panel est le JPanel � afficher l'ecran
	 */
	public void updatePanelMainContainerWhenClicked(JPanel panel) {
		//je desactive tout les panels
		this.panelMesAlarmes.setVisible(false); 
		this.panelCreerAlarmes.setVisible(false); 
		this.panelMinuteur.setVisible(false); 
		this.panelChronometre.setVisible(false); 
		
		//on active le panel choisi 
		panel.setVisible(true);
	}
	/**
	 * Ferme l'application
	 * @author Said El Amani
	 * @since 2.0
	 */
	public void Fermerlecran(){
		Accueil window = new Accueil();
		window.frame.setVisible(true);
		this.dispose();
	}
	
}
