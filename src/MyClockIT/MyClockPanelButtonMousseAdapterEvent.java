package MyClockIT;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

public class MyClockPanelButtonMousseAdapterEvent extends MouseAdapter{

	//le panel cliquer, qui est dans la navigation (panelMenu)
	JPanel panel;
	//le panel a charger, ayant le contenu de la rubrique cliquer
	JPanel panelContaint;
	//Lobject ayant la fonction update (le dashboard de MyClockIT)
	MyClockDashboard dashboard;
	
	/**
	 * Le constructeur de la classe MyClockPanelButtonMousseAdapterEvent
	 * @param panel
	 * @param panelContaint
	 * @param dashboard
	 */
	public MyClockPanelButtonMousseAdapterEvent(JPanel panel,JPanel panelContaint, MyClockDashboard dashboard) {
		// TODO Auto-generated constructor stub
		this.panel = panel;
		this.panelContaint = panelContaint;
		this.dashboard = dashboard;
	}
	
	//reaction par apport a levenement 
	
	/**
	 * La fonction permet de changer la couleur du panel si la souris entre dans le panel
	 * @author Said El Amani
	 * @since 2.0
	 * @param e est un evenement MouseEvent
	 */
	//si la souris entre dans le panel
	@Override
	public void mouseEntered(MouseEvent e) {
		panel.setBackground(ConstanteDashboard.ColorMouseEntered);
	}
		
	
	/**
	 * La fonction permet de changer la couleur du panel si la souris sort du panel
	 * @author Said El Amani
	 * @since 2.0
	 * @param e est un evenement MouseEvent
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		panel.setBackground(ConstanteDashboard.ColorMouseExited);
	}
	
	/**
	 * La fonction permet de changer la couleur du panel si la souris presse le panel
	 * @author Said El Amani
	 * @since 2.0
	 * @param e est un evenement MouseEvent
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		panel.setBackground(ConstanteDashboard.ColorMousePressed);
	}
	
	/**
	 * La fonction permet de changer la couleur du panel apres que la souris ait cliquer sur le panel
	 * @author Said El Amani
	 * @since 2.0
	 * @param e est un evenement MouseEvent
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		panel.setBackground(ConstanteDashboard.ColorMouseRealised);
	}
	
	/**
	 * La fonction permet de changer la couleur du panel si la souris clique sur le panel
	 * @author Said El Amani
	 * @since 2.0
	 * @param e est un evenement MouseEvent
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if(this.panelContaint!=null) {
			//si le panel est MyClockIT.PanelMsAlarmes on actualise le visuel avant dafficher
			if(this.panelContaint.getClass().getName() ==this.dashboard.getPanelMesAlarmes().getClass().getName()) {
				//on positionne lindex de lecture sur le premier element (on revient � la premiere page pour voir la derniere alarme � avoir ete ajouter puisquon � la positionner � lindice 0 dans l'ArrayList )
				this.dashboard.getPanelMesAlarmes().setIndexAlarme(0);
				//on actualise laffichage, on se positionne sur la premiere pages
				this.dashboard.getPanelMesAlarmes().updatePanelMesAlarmes(0);
				
			}
			//je desactive tout les panels et active celui selectionner
			this.dashboard.updatePanelMainContainerWhenClicked(this.panelContaint);
		}else {
			//on a cliquer sur le bouton close
			dashboard.Fermerlecran();
		}
	}
	

}
