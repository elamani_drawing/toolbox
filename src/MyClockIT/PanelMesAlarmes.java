package MyClockIT;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import MyClock.Alarme;
import MyClock.AlarmeManager;

import java.util.ArrayList;
import java.util.Arrays;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

public class PanelMesAlarmes extends JPanel {

	//logo des alarmes dans mes alarmes 
	private ImageIcon imgLogoAlarmeMesAlarmes = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImageLogoAlarmesMesAlarmes).getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
	private ImageIcon imgAlarmeArrowLeft = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImageAlarmeArrowLeft).getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
	private ImageIcon imgAlarmeArrowRight = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImageAlarmeArrowRight).getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
	//manager Alarme est la classe qui gere les alarme, leur creation, leur suppression etc.
	private AlarmeManager ManagerAlarme = ConstanteDashboard.ManagerAlarme;

	//lindex de lecture de la liste des alarmes de lutilisateur 
	private int indexAlarme=0;
	
	//je cr�e des references de mes elements � l'ecran qui seront modifier durant les interaction avec l'utilisateur
	//panel qui vont servir de model pour les alarmes present sur la page
	private JPanel panelAlarme;
	private JPanel panelAlarme2;
	private JPanel panelAlarme3;
	private JPanel panelAlarme4;
	//label heure sont les label present sur les panels models, ont garde une reference pour pouvoir facilement les modifier
	private JLabel lblAlarmeHeure;
	private JLabel lblAlarmeHeure2;
	private JLabel lblAlarmeHeure3;
	private JLabel lblAlarmeHeure4;
	//label frequence present sur les panels models, 
	private JLabel lblAlarmeFrequence;
	private JLabel lblAlarmeFrequence2;
	private JLabel lblAlarmeFrequence3;
	private JLabel lblAlarmeFrequence4;
	//de meme pour les jtextArea qui serviront � afficher les titres de chaque alarme
	private JTextArea jtxtAlarmeTitre;
	private JTextArea jtxtAlarmeTitre2;
	private JTextArea jtxtAlarmeTitre3;
	private JTextArea jtxtAlarmeTitre4;
	//un label qui sert � dire � lutilisateur dans quel page il se trouve, ex: 5/8 ou 2/8 ou 1/1
	private JLabel lblNumeroPage;
	//un message qui sera afficher � l'ecran lorsque lutilisateur naura pas encore d'alarme en cours dexecution
	private JLabel lblMessageNotAlarme;
	
	private ArrayList<JPanel> arrayAlarmePanel = new ArrayList<JPanel>();
	private ArrayList<JLabel> arrayAlarmeLabelHeure = new ArrayList<JLabel>(Arrays.asList(lblAlarmeHeure, lblAlarmeHeure2, lblAlarmeHeure3, lblAlarmeHeure4));
	private ArrayList<JTextArea> arrayAlarmeTextAreaTitre = new ArrayList<JTextArea>(Arrays.asList(jtxtAlarmeTitre, jtxtAlarmeTitre2, jtxtAlarmeTitre3, jtxtAlarmeTitre4));
	private ArrayList<JLabel> arrayAlarmeLabelFrequence = new ArrayList<JLabel>(Arrays.asList(lblAlarmeFrequence, lblAlarmeFrequence2, lblAlarmeFrequence3, lblAlarmeFrequence4));
	
	
	

	/**
	 * Le constructeur de la classe PanelMesAlarmes
	 * @author Said El Amani
	 * @since 2.0
	 * @version 1.0
	 */
	public PanelMesAlarmes() {
		setBounds(0,0,740,633);
		setVisible(true);
		setBackground(ConstanteDashboard.ColorBlueFonceContainer);
		setLayout(null);
		
		this.panelAlarme = new JPanel();
		panelAlarme.setBounds(55, -1, 300, 300);
		panelAlarme.setBackground(ConstanteDashboard.ColorBlueFonceAlarme);
		add(panelAlarme);
		panelAlarme.setLayout(null);
		
		JLabel lblLogoAlarmeMesAlarmes = new JLabel("");
		lblLogoAlarmeMesAlarmes.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogoAlarmeMesAlarmes.setBounds(76, 10, 167, 112);

		lblLogoAlarmeMesAlarmes.setIcon(this.imgLogoAlarmeMesAlarmes);
		panelAlarme.add(lblLogoAlarmeMesAlarmes);
		
		//un label servant a afficher le jour et le moment ou lalarme doit se declencher
		this.lblAlarmeHeure = new JLabel("Lundi 20 h 50");
		lblAlarmeHeure.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlarmeHeure.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblAlarmeHeure.setBounds(0, 118, 323, 58);
		panelAlarme.add(lblAlarmeHeure);
		
		//un label servant a afficher la frequence de lalarme
		JLabel lblAlarmeFrequence = new JLabel("Tous les jours");
		lblAlarmeFrequence.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlarmeFrequence.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAlarmeFrequence.setBounds(0, 174, 323, 58);
		panelAlarme.add(lblAlarmeFrequence);
		
		JPanel panelAlarmeDelete = new JPanel();
		panelAlarmeDelete.setBorder(new LineBorder(Color.RED, 1, true));
		panelAlarmeDelete.setBounds(255, 0, 45, 44);
		panelAlarmeDelete.setBackground(ConstanteDashboard.ColorBlueFonceAlarme);
		panelAlarme.add(panelAlarmeDelete);
		
		//un label qui va permettre � l'utilisateur de supprimer une alarme en cliquand sur le label
		JLabel lblDeleteAlarme = new JLabel("X");
		lblDeleteAlarme.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					//on supprime lalarme de la bdd
					ManagerAlarme.reqDeleteAlarme(ManagerAlarme.getLesAlarmes().get(indexAlarme));
					//on annule lalarme
					ManagerAlarme.getLesAlarmes().get(indexAlarme).stopAlarm();
					//on supprime lalarme de notre array dalarme
					ManagerAlarme.reqDeleteAlarme(ManagerAlarme.getLesAlarmes().remove(indexAlarme));
					//on actualise le visuel
					updatePanelMesAlarmes(0);
					//on indique a lutilisateur que la suppression a fonctionner
					JOptionPane.showMessageDialog(lblDeleteAlarme, "L'alarme � bien �t� supprimer.","Success", JOptionPane.PLAIN_MESSAGE);
				} catch (SQLException e1) {
					//e1.printStackTrace();
					//une alarme basique cest supprimer mais pas visuelement, au lieu dactualiser, lutilisateur tente de la supprimer, on actualise a sa place
					updatePanelMesAlarmes(0);
				}
			}
		});
		lblDeleteAlarme.setHorizontalAlignment(SwingConstants.CENTER);
		lblDeleteAlarme.setForeground(Color.RED);
		lblDeleteAlarme.setFont(new Font("Tahoma", Font.PLAIN, 32));
		panelAlarmeDelete.add(lblDeleteAlarme);
		
		this.jtxtAlarmeTitre = new JTextArea();
		jtxtAlarmeTitre.setBackground(ConstanteDashboard.ColorBlueFonceAlarme);
		jtxtAlarmeTitre.setEditable(false);
		jtxtAlarmeTitre.setLineWrap(true);
		jtxtAlarmeTitre.setText("titre 1");
		jtxtAlarmeTitre.setFont(new Font("Tahoma", Font.PLAIN, 16));
		jtxtAlarmeTitre.setBounds(22, 243, 276, 64);
		panelAlarme.add(jtxtAlarmeTitre);
		
		this.panelAlarme2 = new JPanel();
		panelAlarme2.setLayout(null);
		panelAlarme2.setBackground(new Color(246, 246, 255));
		panelAlarme2.setBounds(385, -1, 300, 300);
		add(panelAlarme2);
		
		JLabel lblLogoAlarmeMesAlarmes2 = new JLabel("");
		lblLogoAlarmeMesAlarmes2.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogoAlarmeMesAlarmes2.setBounds(76, 10, 167, 112);
		lblLogoAlarmeMesAlarmes2.setIcon(this.imgLogoAlarmeMesAlarmes);
		panelAlarme2.add(lblLogoAlarmeMesAlarmes2);
		
		this.lblAlarmeHeure2 = new JLabel("Lundi 20 h 50");
		lblAlarmeHeure2.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlarmeHeure2.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblAlarmeHeure2.setBounds(0, 118, 323, 58);
		panelAlarme2.add(lblAlarmeHeure2);
		
		JLabel lblAlarmeFrequence2 = new JLabel("Tous les jours");
		lblAlarmeFrequence2.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlarmeFrequence2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAlarmeFrequence2.setBounds(0, 174, 323, 58);
		panelAlarme2.add(lblAlarmeFrequence2);
		
		JPanel panelAlarmeDelete2 = new JPanel();
		panelAlarmeDelete2.setBorder(new LineBorder(Color.RED, 1, true));
		panelAlarmeDelete2.setBackground(new Color(246, 246, 255));
		panelAlarmeDelete2.setBounds(255, 0, 45, 44);
		panelAlarme2.add(panelAlarmeDelete2);
		
		JLabel lblDeleteAlarme2 = new JLabel("X");
		lblDeleteAlarme2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					ManagerAlarme.reqDeleteAlarme(ManagerAlarme.getLesAlarmes().get(indexAlarme+1));
					//on annule lalarme
					ManagerAlarme.getLesAlarmes().get(indexAlarme).stopAlarm();
					ManagerAlarme.reqDeleteAlarme(ManagerAlarme.getLesAlarmes().remove(indexAlarme+1));
					updatePanelMesAlarmes(0);
					JOptionPane.showMessageDialog(lblDeleteAlarme2, "L'alarme � bien �t� supprimer.","Success", JOptionPane.PLAIN_MESSAGE);
				} catch (SQLException e1) {
					//e1.printStackTrace();
					//une alarme basique cest supprimer mais pas visuelement, au lieu dactualiser, lutilisateur tente de la supprimer, on actualise a sa place
					updatePanelMesAlarmes(0);
				}
			}
		});
		lblDeleteAlarme2.setHorizontalAlignment(SwingConstants.CENTER);
		lblDeleteAlarme2.setForeground(Color.RED);
		lblDeleteAlarme2.setFont(new Font("Tahoma", Font.PLAIN, 32));
		panelAlarmeDelete2.add(lblDeleteAlarme2);
		
		this.jtxtAlarmeTitre2 = new JTextArea();
		jtxtAlarmeTitre2.setText("titre 2");
		jtxtAlarmeTitre2.setLineWrap(true);
		jtxtAlarmeTitre2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		jtxtAlarmeTitre2.setEditable(false);
		jtxtAlarmeTitre2.setBackground(new Color(246, 246, 255));
		jtxtAlarmeTitre2.setBounds(22, 243, 276, 64);
		panelAlarme2.add(jtxtAlarmeTitre2);
		
		this.panelAlarme3 = new JPanel();
		panelAlarme3.setLayout(null);
		panelAlarme3.setBackground(new Color(246, 246, 255));
		panelAlarme3.setBounds(55, 333, 300, 300);
		add(panelAlarme3);
		
		JLabel lblLogoAlarmeMesAlarmes3 = new JLabel("");
		lblLogoAlarmeMesAlarmes3.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogoAlarmeMesAlarmes3.setBounds(76, 10, 167, 112);
		lblLogoAlarmeMesAlarmes3.setIcon(this.imgLogoAlarmeMesAlarmes);
		panelAlarme3.add(lblLogoAlarmeMesAlarmes3);
		
		this.lblAlarmeHeure3 = new JLabel("Lundi 20 h 50");
		lblAlarmeHeure3.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlarmeHeure3.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblAlarmeHeure3.setBounds(0, 118, 323, 58);
		panelAlarme3.add(lblAlarmeHeure3);
		
		JLabel lblAlarmeFrequence3 = new JLabel("Tous les jours");
		lblAlarmeFrequence3.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlarmeFrequence3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAlarmeFrequence3.setBounds(0, 174, 323, 58);
		panelAlarme3.add(lblAlarmeFrequence3);
		
		JPanel panelAlarmeDelete3 = new JPanel();
		panelAlarmeDelete3.setBorder(new LineBorder(Color.RED, 1, true));
		panelAlarmeDelete3.setBackground(new Color(246, 246, 255));
		panelAlarmeDelete3.setBounds(255, 0, 45, 44);
		panelAlarme3.add(panelAlarmeDelete3);
		
		JLabel lblDeleteAlarme3 = new JLabel("X");
		lblDeleteAlarme3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					ManagerAlarme.reqDeleteAlarme(ManagerAlarme.getLesAlarmes().get(indexAlarme+2));
					//on annule lalarme
					ManagerAlarme.getLesAlarmes().get(indexAlarme).stopAlarm();
					ManagerAlarme.reqDeleteAlarme(ManagerAlarme.getLesAlarmes().remove(indexAlarme+2));
					updatePanelMesAlarmes(0);
					JOptionPane.showMessageDialog(lblDeleteAlarme3, "L'alarme � bien �t� supprimer.","Success", JOptionPane.PLAIN_MESSAGE);
				} catch (SQLException e1) {
					//e1.printStackTrace();
					//une alarme basique cest supprimer mais pas visuelement, au lieu dactualiser, lutilisateur tente de la supprimer, on actualise a sa place
					updatePanelMesAlarmes(0);
				}
			}
		});
		lblDeleteAlarme3.setHorizontalAlignment(SwingConstants.CENTER);
		lblDeleteAlarme3.setForeground(Color.RED);
		lblDeleteAlarme3.setFont(new Font("Tahoma", Font.PLAIN, 32));
		panelAlarmeDelete3.add(lblDeleteAlarme3);
		
		this.jtxtAlarmeTitre3 = new JTextArea();
		jtxtAlarmeTitre3.setText("titre 3");
		jtxtAlarmeTitre3.setLineWrap(true);
		jtxtAlarmeTitre3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		jtxtAlarmeTitre3.setEditable(false);
		jtxtAlarmeTitre3.setBackground(new Color(246, 246, 255));
		jtxtAlarmeTitre3.setBounds(22, 243, 276, 64);
		panelAlarme3.add(jtxtAlarmeTitre3);
		
		this.panelAlarme4 = new JPanel();
		panelAlarme4.setLayout(null);
		panelAlarme4.setBackground(new Color(246, 246, 255));
		panelAlarme4.setBounds(385, 333, 300, 300);
		add(panelAlarme4);
		
		JLabel lblLogoAlarmeMesAlarmes4 = new JLabel("");
		lblLogoAlarmeMesAlarmes4.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogoAlarmeMesAlarmes4.setBounds(76, 10, 167, 112);
		lblLogoAlarmeMesAlarmes4.setIcon(this.imgLogoAlarmeMesAlarmes);
		panelAlarme4.add(lblLogoAlarmeMesAlarmes4);
		
		this.lblAlarmeHeure4 = new JLabel("Lundi 20 h 50");
		lblAlarmeHeure4.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlarmeHeure4.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblAlarmeHeure4.setBounds(0, 118, 323, 58);
		panelAlarme4.add(lblAlarmeHeure4);
		
		JLabel lblAlarmeFrequence4 = new JLabel("Tous les jours");
		lblAlarmeFrequence4.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlarmeFrequence4.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAlarmeFrequence4.setBounds(0, 174, 323, 58);
		panelAlarme4.add(lblAlarmeFrequence4);
		
		JPanel panelAlarmeDelete4 = new JPanel();
		panelAlarmeDelete4.setBorder(new LineBorder(Color.RED, 1, true));
		panelAlarmeDelete4.setBackground(new Color(246, 246, 255));
		panelAlarmeDelete4.setBounds(255, 0, 45, 44);
		panelAlarme4.add(panelAlarmeDelete4);
		
		JLabel lblDeleteAlarme4 = new JLabel("X");
		lblDeleteAlarme4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					ManagerAlarme.reqDeleteAlarme(ManagerAlarme.getLesAlarmes().get(indexAlarme+3));
					//on annule lalarme
					ManagerAlarme.getLesAlarmes().get(indexAlarme).stopAlarm();
					ManagerAlarme.reqDeleteAlarme(ManagerAlarme.getLesAlarmes().remove(indexAlarme+3));
					updatePanelMesAlarmes(0);
					JOptionPane.showMessageDialog(lblDeleteAlarme4, "L'alarme � bien �t� supprimer.","Success", JOptionPane.PLAIN_MESSAGE);
				} catch (SQLException e1) {
					//e1.printStackTrace();
					//une alarme basique cest supprimer mais pas visuelement, au lieu dactualiser, lutilisateur tente de la supprimer, on actualise a sa place
					updatePanelMesAlarmes(0);
				}
			}
		});
		lblDeleteAlarme4.setHorizontalAlignment(SwingConstants.CENTER);
		lblDeleteAlarme4.setForeground(Color.RED);
		lblDeleteAlarme4.setFont(new Font("Tahoma", Font.PLAIN, 32));
		panelAlarmeDelete4.add(lblDeleteAlarme4);
		
		this.jtxtAlarmeTitre4 = new JTextArea();
		jtxtAlarmeTitre4.setText("titre 4");
		jtxtAlarmeTitre4.setLineWrap(true);
		jtxtAlarmeTitre4.setFont(new Font("Tahoma", Font.PLAIN, 16));
		jtxtAlarmeTitre4.setEditable(false);
		jtxtAlarmeTitre4.setBackground(new Color(246, 246, 255));
		jtxtAlarmeTitre4.setBounds(22, 243, 276, 64);
		panelAlarme4.add(jtxtAlarmeTitre4);
		
		//une image de feche vers la droite
		JLabel lblArrowRight = new JLabel("");
		lblArrowRight.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//si on clique sur droite, on actualise lecran en mettant les 4 prochaines alarmes
				updatePanelMesAlarmes(1);
			}
		});
		lblArrowRight.setBounds(685, 309, 50, 50);
		lblArrowRight.setIcon(this.imgAlarmeArrowRight);
		add(lblArrowRight);
		
		//une image de fleche vers la gauche
		JLabel lblArrowLeft = new JLabel("");
		lblArrowLeft.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//si on clique sur gauche, on actualise lecran en mettant les 4 alarmes qui precedent
				updatePanelMesAlarmes(-1);
			}
		});
		lblArrowLeft.setBounds(1, 309, 50, 50);
		lblArrowLeft.setIcon(this.imgAlarmeArrowLeft);
		add(lblArrowLeft);
		
		//un label qui sert � indiquer � l'utilisateur dans quelle page il se trouve et combien de page il reste
		lblNumeroPage = new JLabel("10/40");
		lblNumeroPage.setForeground(Color.WHITE);
		lblNumeroPage.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNumeroPage.setHorizontalAlignment(SwingConstants.CENTER);
		lblNumeroPage.setBounds(690, 610, 45, 13);
		add(lblNumeroPage);
		
		//un message qui sera afficher si l'utilisateur n'a pas d'alarmes dans sa liste dalarmes
		this.lblMessageNotAlarme = new JLabel("Vous n'avez pas d'alarme en cours d'execution.");
		lblMessageNotAlarme.setForeground(Color.WHITE);
		lblMessageNotAlarme.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblMessageNotAlarme.setHorizontalAlignment(SwingConstants.CENTER);
		lblMessageNotAlarme.setBounds(0, 296, 735, 36);
		add(lblMessageNotAlarme);
		
		//je reccupere les elements dont jai besoin dans les array creer plus haut
		//panel : les boites
		this.arrayAlarmePanel = new ArrayList<JPanel>(Arrays.asList(this.panelAlarme, this.panelAlarme2, this.panelAlarme3, this.panelAlarme4));	
		//label : les jours et heures 
		this.arrayAlarmeLabelHeure = new ArrayList<JLabel>(Arrays.asList(this.lblAlarmeHeure, this.lblAlarmeHeure2, this.lblAlarmeHeure3, this.lblAlarmeHeure4));
		//jtextArea : les titres 
		this.arrayAlarmeTextAreaTitre = new ArrayList<JTextArea>(Arrays.asList(this.jtxtAlarmeTitre, this.jtxtAlarmeTitre2, this.jtxtAlarmeTitre3, this.jtxtAlarmeTitre4));
		//label : les frequences
		this.arrayAlarmeLabelFrequence = new ArrayList<JLabel>(Arrays.asList(lblAlarmeFrequence, lblAlarmeFrequence2, lblAlarmeFrequence3, lblAlarmeFrequence4));
		
		//on actualise laffichage
		updatePanelMesAlarmes(0);

	}
	/**
	 * La fonction permet de savoir combien de page la liste des alarmes permet de realiser
	 * @author Said El Amani
	 * @since 2.0
	 * @return un int representant le maximum de pages
	 */
	private int getMaxPage() {
		int maxPage = this.ManagerAlarme.getLesAlarmes().size()-1;
		maxPage = maxPage/4; //on compte combien de page de 4 alarmes ont peut faire
		maxPage += maxPage%4; //on verifie sil reste des alarmes ou pas 
		//si le nombre maximal de page est 0, en met 1, car la page une est c'elle sur laquelle se trouve actuellement lutilisateur (la page 0, des qu'il y a au moin une alarme il existe 1 page)
		if(maxPage==0) {
			maxPage=1;
		}
		return maxPage;
	}
	/**
	 * La fonction permet de reccupperer l'index de la liste des alarmes
	 * @author Said El Amani
	 * @since 2.0
	 * @return un int representant l'index
	 */
	public int getIndexAlarme() {
		return this.indexAlarme;
	}
	/**
	 * La fonction permet de modifier l'index de la liste des alarmes
	 * @author Said El Amani
	 * @since 2.0
	 * @param value est un int representant l'index de la liste des alarmes
	 */
	public void setIndexAlarme(int value) {
		this.indexAlarme = value;
	}
	
	/**
	 * La fonction permet de mettre � jour la pagination de l'utilsiateur
	 * @author Said El Amani
	 * @since 2.0
	 */
	private void updatePagination(int index) {
		int maxPage = this.getMaxPage();
		int page = index/2;
		if(page==0) {//si lindex est � 0, la page sera la page 1
			page=1;
		}
		//on change la valeur afficher
		this.lblNumeroPage.setText(String.valueOf(page)+"/"+String.valueOf(maxPage)); //on ecrit le message
	}
	
	/**
	 * La fonction permet de mettre � jour les alarmes present dans le pannel
	 * @author Said El Amani
	 * @since 2.0
	 * @param sens est un int, si sens est egal � 1, on affiche les 4 prochainnes alarmes de la liste des alarmes, sinon si sens est egal � -1 on affiche les 4 precedentes alarmes.
	 */
	public void updatePanelMesAlarmes(int sens) {
		//si sens est � 1 on ajoute 4 sinon on retire 4 pour naviguer entre les pages, si sens==0 on ne fait rien (on veut juste actualiser le visuel)
		if(sens == 1) {
			this.indexAlarme+=4;
		}else if(sens==-1){
			this.indexAlarme-=4;
		}
		//si lindex est negatif on le met � 0
		if(this.indexAlarme<0) {
			this.indexAlarme= 0;
		}else if(this.indexAlarme>=this.ManagerAlarme.getLesAlarmes().size()) {
			//si au dessu du nombre dalarme on le remet � la valeur � laquelle il etait 
			this.indexAlarme -= 4;
		}
		//on actualise la pagination
		updatePagination(this.indexAlarme);
		//on actualise les alarmes affich�
		updateAffichageAlarmes();
	}
	

	/**
	 * La fonction permet de mettre � jour l'affichage de l'ecran
	 * @author Said El Amani
	 * @since 2.0
	 */
	private void updateAffichageAlarmes() {
		JLabel label;
		JLabel labelFrequence;
		JTextArea textArea;
		JPanel panel;
		Alarme alarme = null;
		boolean erreur;
		if(this.ManagerAlarme.getLesAlarmes().size() >0) {
			//si l'utilisateur � des alarmes, on peut les afficher
			this.lblMessageNotAlarme.setVisible(false); //on efface de lecran le message qui dit que la liste est vide
			//on affiche le message qui informe lutilisateur de la page � laquelle il se situe
			this.lblNumeroPage.setVisible(true);
			//on parcour la liste des model panel pour les afficher et afficher les informations des alarmes qui sont sur la page
			for(int i=0; i<4; i++){
				erreur = false;
				//on reccupere les information a modifier
				panel = this.arrayAlarmePanel.get(i);//le panel
				label = this.arrayAlarmeLabelHeure.get(i);//le label pour afficher l'heure
				textArea = this.arrayAlarmeTextAreaTitre.get(i);//le JTextArea pour afficher le titre
				labelFrequence = this.arrayAlarmeLabelFrequence.get(i);//la frequence
				//On active les panel au cas ou ils avait ete desactiver
				panel.setVisible(true);
				//on reccupere lalarme 
				try {
					//si on a pus reccuperer lalarme on ne desactive pas le panel
					alarme = this.ManagerAlarme.getLesAlarmes().get(this.indexAlarme +i);
				}catch(Exception error) {
					//il ya eu une erreur car lalarme nexiste pas, on desactive le panel
					erreur =true;
					//on desactive le panel
					panel.setVisible(false);
				}
				if (erreur==false ){
					//on modifie les valeurs
					label.setText(alarme.getDayHour());
					labelFrequence.setText(alarme.getRegularite());
					textArea.setText(alarme.getTitre());
				}
				//ajouter des alarmes pour parametrer laffichage, tester la suppression et lajout, faire test sql, corriger code
			}
		}else {
			//on affiche le message pour dire � l'utilisateur qu'il ny a pas d'alarme
			this.lblMessageNotAlarme.setVisible(true); //on affiche � l'ecran le message
			//on s'assure que les jpanel montre par defaut son effacer de l'ecran
			for(int i=0; i<4; i++){
				panel = this.arrayAlarmePanel.get(i);//on reccupere chaque panel
				panel.setVisible(false);//on lefface
			}
			//on efface le message qui informe lutilisateur de la page � laquelle il se situe
			this.lblNumeroPage.setVisible(false);
		}
	}
}
