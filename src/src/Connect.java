package src;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
	/**
	 * @author Youssouf Moussa
	 */
	Connection con;
	public Connect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}  catch(ClassNotFoundException e) {
			System.err.println(e);
		}
		try{
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
		}catch(SQLException e) {System.err.println(e);}
	}
	Connection connexion() {return con;}

}
