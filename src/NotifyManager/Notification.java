package NotifyManager;

public class Notification {

	private String titre;
	private String message;
	
	/**
	 * Le constructeur de la classe Notification
	 * @param titre est le titre de la notification
	 * @param message est un string repressentant le message affich� dans la notificatin
	 * @author Said El Amani
	 * @since 2.0
	 * @version 1.0
	 */
	public Notification(String titre, String message) {
		this.titre=titre; 
		this.message = message;
	}
	/**
	 * La fonction permet de reccuperrer le titre de la notification
	 * @author Said El Amani
	 * @since 2.0
	 * @return le titre de la notification
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * La fonction permet de modifier le titre de la notification
	 * @author Said El Amani
	 * @since 2.0
	 * @param titre est un String representant le nouveau titre de la notification
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}

	/**
	 * La fonction permet de reccupperrer le Message de la notification
	 * @author Said El Amani
	 * @since 2.0
	 * @return un String representant le message de la notification
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * La fonction permet de modifier le message de la notification
	 * @author Said El Amani
	 * @since 1.0
	 * @param message est un String representant le nouveau message de la notification
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
