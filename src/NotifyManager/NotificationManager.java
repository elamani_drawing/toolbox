package NotifyManager;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;

import javax.swing.ImageIcon;

import MyClockIT.ConstanteDashboard;

public class NotificationManager {
	
	private final Image image ;
	private final TrayIcon trayIcon;
	private final SystemTray tray;
	
	/**
	 * Le constructeur de la classe NotificationManager, la classe sert � manager tout les notifications de l'application.
	 */
	public NotificationManager() {
		//on parametre l'image
        this.image = new ImageIcon(new ImageIcon(ConstanteDashboard.linkImageMyClock).getImage().getScaledInstance(130, 130, Image.SCALE_DEFAULT)).getImage();
        this.trayIcon = new TrayIcon(image, "Tool box");
        this.trayIcon.setImageAutoSize(true);
        
        this.trayIcon.setToolTip("ToolBox");
        this.tray = SystemTray.getSystemTray();
        try {
			this.tray.add(trayIcon);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * La fonction permet de lancer une notification
	 * @author Said El Amani
	 * @since 2.0
	 * @param notification est un Object Notification
	 * @throws AWTException
	 */
	public void sendNotification(Notification notification) throws AWTException{
		if (SystemTray.isSupported()) {
			//le system supporte les notifications donc on peut l'utiliser
			this.trayIcon.displayMessage(notification.getTitre(), notification.getMessage(), MessageType.INFO);
		}else {
			//sinon on envoie juste la notification
			System.out.println("Les notifications ne sont pas supporter sur se system.");
		}
	}
}
