package CalendrierToolBox;

import java.awt.EventQueue;

public class Index {
	/**
	 *contient la methode main pour lancer l'application. 
	 */

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceGraphic frame = new InterfaceGraphic();
					frame.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}
