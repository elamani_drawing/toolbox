package CalendrierToolBox;


import java.nio.file.Paths;
/**
 * 
 * @author Youssouf Moussa
 * une interface pour le chemin de repertoire qui contient mes logo
 *
 */
public interface MesLogo {

	String lienlogo  = Paths.get(System.getProperty("user.dir"), "logo","logo.png").toString();
	String lienlogoPlus  = Paths.get(System.getProperty("user.dir"), "logo","plus.png").toString();
	String lienlogoDelete  = Paths.get(System.getProperty("user.dir"), "logo","delete.png").toString();
	
}
