package CalendrierToolBox;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;

import Accueil.Accueil;
import InscriptionetConnexion.Connexion_Variablesession;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

/**
 * @author Youssouf Moussa
 * @since 1.2
 * @extends JFrame
 */
public class InterfaceGraphic extends Connexion_Variablesession {
	String user = getNameC();
	public JFrame frame;
	
	private static final long serialVersionUID = 1L;
	final int TAILLE = 11;
	private JPanel contentPane;
	private JTextField title;
	private JDateChooser date;
	private JTable table;
	private JScrollPane scrollPane;
	DefaultTableModel model;
	private JTextPane note = new JTextPane();
	private ImageIcon imgLogo = new ImageIcon(new ImageIcon(MesLogo.lienlogo).getImage());
	private ImageIcon imgLogoPlus = new ImageIcon(new ImageIcon(MesLogo.lienlogoPlus).getImage());
	private ImageIcon imgLogoDelete = new ImageIcon(new ImageIcon(MesLogo.lienlogoDelete).getImage());
	Connect con = new Connect();
	java.sql.Statement stm;
	ResultSet Rs;

	/**
	 * Creation de l'interface graphique.
	 * 
	 * @author Youssouf Moussa
	 */
	   public InterfaceGraphic() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 800, 583);
		contentPane = new JPanel();
		contentPane.setBackground(Color.CYAN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(135, 206, 235));
		panel.setBounds(0, 0, 830, 546);
		contentPane.add(panel);
		panel.setLayout(null);
        JLabel utilisateur = new JLabel (user);
        utilisateur.setForeground(new Color(0,128,255));
        utilisateur.setBounds(10, 10, 100, 14);
		JLabel lblDate = new JLabel("Date");
		
		lblDate.setBounds(76, 129, 49, 14);
		panel.add(lblDate);
		panel.add(utilisateur);

		JLabel lblEven = new JLabel("Title");
		
		lblEven.setBounds(76, 154, 49, 14);
		panel.add(lblEven);
		title = new JTextField();
		title.setBounds(122, 151, 96, 20);
		panel.add(title);
		scrollPane = new JScrollPane();
		scrollPane.setBounds(251, 124, 258, 345);
		panel.add(scrollPane);
		table = new JTable();
		table.setBackground(new Color(240, 248, 255));
		model = new DefaultTableModel();
		Object[] colum = { "Id", "Day", "Title", "Note" };
		model.setColumnIdentifiers(colum);
		final Object[] row = new Object[4];

		table.setModel(model);
		try {

			stm = con.connexion().createStatement();

			/**
			 * recharge les donnees de l'utilisateur connecte dans la table de donnees
			 * depuis la base de donnees
			 */
			java.sql.ResultSet Rs = stm.executeQuery("select * from agenda where agenda.user = '"+user+"'");
			while (Rs.next()) {

				row[0] = Rs.getString("id");
				row[1] = Rs.getString("day");
				row[2] = Rs.getString("title");
				row[3] = Rs.getString("note");
				model.addRow(row);

			}

		} catch (Exception e) {
			System.err.println(e);
		}
		/**
		 * on optimise les dimensions des colonnes quand elles contiennent des donnees
		 */
		if (table.getColumnModel().getColumnCount() > 0) {
			table.getColumnModel().getColumn(0).setPreferredWidth(30);
			table.getColumnModel().getColumn(1).setPreferredWidth(120);
			table.getColumnModel().getColumn(2).setPreferredWidth(130);
			table.getColumnModel().getColumn(3).setPreferredWidth(150);

		}

		scrollPane.setViewportView(table);

		JLabel lblNote = new JLabel("Note");
		
		lblNote.setBounds(76, 179, 49, 14);
		panel.add(lblNote);

		JButton btnPlus = new JButton("");
		btnPlus.setIcon(this.imgLogoPlus);
		/**@author youssouf moussa
		 * une action sur le button plus (ou ajouter ) pour ajouter des donnees dans la base de donnees et dans la JTable
		 */
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				 * on verifie si toutes les cases sont remplis sinon on envoie un message 
				 */
				if (title.getText().equals("") || note.getText().equals("") || date.getDate().equals("")) {
					JOptionPane.showMessageDialog(null, "Please Fill Complete Information");

				}
               /**
                * 
                * si les donnees sont, on insert dans la base de donnees et on affiche dans la jtable (notre agenda)
                */
				else {

					try {

						stm = con.connexion().createStatement();
						stm.executeUpdate("insert into agenda (day,title,note,user ) values('"
								+ date.getDate().toString().substring(0, TAILLE) + "','" + title.getText() + "','"
								+ note.getText() + "','" + user + "')");
						
						java.sql.ResultSet Rs = stm.executeQuery("select * from agenda where user = '"+ user+"'");
						while (Rs.next()) {

							row[0] = Rs.getString("id");
							row[1] = Rs.getString("day");
							row[2] = Rs.getString("title");
							row[3] = Rs.getString("note");
							
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					/**
					 * message de confirmation d'enregistrement de donnees
					 */
					JOptionPane.showMessageDialog(null, "Saved Successfully");
					model.addRow(row);

					date.setCalendar(null);
					title.setText("");
					note.setText("");
				}
				/**
				 * on optimise la dimension des colonnes quand elles ne sont pas vides des donnees
				 */
				if (table.getColumnModel().getColumnCount() > 0) {
					table.getColumnModel().getColumn(0).setPreferredWidth(30);
					table.getColumnModel().getColumn(1).setPreferredWidth(120);
					table.getColumnModel().getColumn(2).setPreferredWidth(130);
					table.getColumnModel().getColumn(3).setPreferredWidth(150);

				}

			}
		});

		btnPlus.setBounds(27, 295, 83, 63);
		panel.add(btnPlus);

		JButton btnDelete = new JButton("");
		/**@author youssouf moussa
		 * une action pour supprimer les donnees 
		 */
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/**
				 * @author youssouf moussa
				 * on selectionne une ligne  � supprimer
				 */
				int i = table.getSelectedRow();
				
				String str  = String.valueOf(table.getValueAt(i,0));

				int id = Integer.parseInt(str);
				
				if (i >= 0) {
			    model.removeRow(i);
					
					try {
						stm = con.connexion().createStatement();
						stm.executeUpdate("delete from agenda where id = '"+id+"' and user = '"+user+"'");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					/**
					 * message de confirmation de suppression de la ligne selectionn�e
					 */
					JOptionPane.showMessageDialog(null, "Deleted Successfully");
				} else {
					JOptionPane.showMessageDialog(null, "Please Select a Row First");
				}
			}
		});
		btnDelete.setIcon(this.imgLogoDelete);
		btnDelete.setBounds(141, 295, 89, 63);
		panel.add(btnDelete);
		JCalendar calendar = new JCalendar();
		calendar.setBounds(531, 124, 205, 194);
		panel.add(calendar);
		note.setBounds(122, 176, 96, 20);
		panel.add(note);
		date = new JDateChooser();
		date.setBounds(122, 126, 96, 20);
		panel.add(date);
		JLabel logo = new JLabel("");
		logo.setIcon(this.imgLogo);
		logo.setBounds(10, 31, 831, 543);
		panel.add(logo);
		
		JButton btnaccueil = new JButton("accueil");
		btnaccueil.setBackground(Color.WHITE);
		btnaccueil.setBounds(669, 10, 96, 32);
		panel.add(btnaccueil);

		btnaccueil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Accueil acceuil = new Accueil();
				acceuil.frame.setVisible(true);// ouverture calendrier
				frame.dispose();
			}
		});
		
	}
}