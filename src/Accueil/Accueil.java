package Accueil;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

import CalendrierToolBox.InterfaceGraphic;
import InscriptionetConnexion.Connexion_Variablesession;
import MessagerieToolBox.ImageToolBox;
import MessagerieToolBox.Messagerie;
import MyClockIT.MyClockDashboard;
import PortefeuilleToolBox.Portefeuille;

public class Accueil extends Connexion_Variablesession {

	/**
	 * @author Alan et Benjamin
	 * @version 2.0
	 * Launch the application.
	 */
	
	public JFrame frame;
	static String namec = getNameC();
	static int idconnexion = getIdC();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Accueil window = new Accueil();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Accueil() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 447, 602);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton calendrier = new JButton("");
		calendrier.setBackground(Color.WHITE);
		calendrier.setBounds(220, 79, 200, 200);
		frame.getContentPane().add(calendrier);
		ImageIcon imageIcon = new ImageIcon(new ImageIcon(ImageToolBox.calendrier).getImage()
				.getScaledInstance(180, 180, Image.SCALE_DEFAULT));
		calendrier.setIcon(imageIcon);

		calendrier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InterfaceGraphic calendrier = new InterfaceGraphic();
				calendrier.frame.setVisible(true);// ouverture calendrier
				frame.dispose();
			}
		});

		JButton messagerie = new JButton("");
		messagerie.setBackground(Color.WHITE);
		messagerie.setBounds(220, 290, 200, 200);
		frame.getContentPane().add(messagerie);
		ImageIcon imageIcon2 = new ImageIcon(new ImageIcon(ImageToolBox.messagerie).getImage()
				.getScaledInstance(180, 180, Image.SCALE_DEFAULT));
		messagerie.setIcon(imageIcon2);

		messagerie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Messagerie window = new Messagerie();
				window.frame.setVisible(true);// ouverture messagerie
				frame.dispose();
			}
		});

		JButton chrono = new JButton("");
		chrono.setBackground(Color.WHITE);
		chrono.setBounds(10, 79, 200, 200);
		frame.getContentPane().add(chrono);
		ImageIcon imageIcon3 = new ImageIcon(new ImageIcon(ImageToolBox.chrono).getImage()
				.getScaledInstance(160, 191, Image.SCALE_DEFAULT));
		chrono.setIcon(imageIcon3);

		chrono.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MyClockDashboard window = new MyClockDashboard(getIdC());
				window.setVisible(true);
				frame.dispose();
			}
		});

		JButton portefeuille = new JButton("");
		portefeuille.setBackground(Color.WHITE);
		portefeuille.setBounds(10, 290, 200, 200);
		frame.getContentPane().add(portefeuille);
		ImageIcon imageIcon4 = new ImageIcon(new ImageIcon(ImageToolBox.portefeuille).getImage()
				.getScaledInstance(180, 180, Image.SCALE_DEFAULT));
		portefeuille.setIcon(imageIcon4);

		portefeuille.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Portefeuille window = new Portefeuille();
				window.frame.setVisible(true);// ouverture portefeuille
				frame.dispose();
			}
		});

		JPanel paneldeconnexion = new JPanel();
		paneldeconnexion.setBackground(Color.BLACK);
		paneldeconnexion.setBounds(0, 0, 442, 68);
		frame.getContentPane().add(paneldeconnexion);
		paneldeconnexion.setLayout(null);

		JLabel lblUC = new JLabel("Utilisateur connect\u00E9 : " + getNameC());
		lblUC.setForeground(Color.WHITE);
		lblUC.setBounds(12, 0, 430, 24);
		paneldeconnexion.add(lblUC);

		JLabel lblToolBox = new JLabel("ToolBox");
		lblToolBox.setHorizontalAlignment(SwingConstants.CENTER);
		lblToolBox.setForeground(Color.WHITE);
		lblToolBox.setFont(new Font("Tw Cen MT", Font.PLAIN, 30));
		lblToolBox.setBounds(112, 19, 216, 38);
		paneldeconnexion.add(lblToolBox);

		JButton btnDeconnexion = new JButton("Deconnexion");
		btnDeconnexion.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.PLAIN, 17));
		btnDeconnexion.setBackground(Color.WHITE);
		btnDeconnexion.setBounds(79, 501, 265, 47);
		frame.getContentPane().add(btnDeconnexion);

		btnDeconnexion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Connexion_Variablesession window = new Connexion_Variablesession();
				window.frame.setVisible(true);//deconnexion de l'appli
				frame.dispose();
			}
		});
	}
}
