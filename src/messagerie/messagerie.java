<<<<<<< HEAD
package messagerie;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Dimension;
import java.awt.Window.Type;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import inscriptionconnexion.connexion;

import java.awt.List;
import javax.swing.JMenuItem;
import java.awt.Panel;
import java.awt.Color;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import java.awt.TextArea;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.JTextArea;
import java.awt.CardLayout;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.border.MatteBorder;

public class messagerie extends connexion {

	JFrame frame;
	String namec = getNameC();
	static String desti = "";
	String namelist = "";
	private JTextField textField;
	int heure;
	int minute;
	int seconde;
	public static ImageIcon image = new ImageIcon(new ImageIcon("C:\\\\Users\\\\Benjamin\\\\Pictures\\\\seoulcoree.jpg")
			.getImage().getScaledInstance(807, 487, Image.SCALE_DEFAULT));
	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					messagerie window = new messagerie();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public messagerie() {
		M_Project();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws SQLException
	 */
	private void M_Project() {

		// menu principal
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 813, 513);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Border border = BorderFactory.createLineBorder(Color.WHITE);

		JPanel panel_2 = new JPanel();
		panel_2.setBounds(0, 0, 807, 487);
		frame.getContentPane().add(panel_2);

		JLabel lblNewLabel_3 = new JLabel();
		lblNewLabel_3.setBounds(0, 0, 807, 487);

		panel_2.setLayout(null);

		ActionListener a = new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				frame.dispose();
				theme window = new theme();
				window.frame.setVisible(true);

			}

		};
		
				JLabel lblNewLabel_1 = new JLabel("Votre message :");
				lblNewLabel_1.setBounds(92, 141, 120, 22);
				panel_2.add(lblNewLabel_1);
				lblNewLabel_1.setForeground(new Color(255, 255, 255));
				lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
				lblNewLabel_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 18));

		JPanel panel_3_4 = new JPanel();
		panel_3_4.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		panel_3_4.setBackground(Color.BLACK);
		panel_3_4.setBounds(10, 97, 135, 30);
		panel_2.add(panel_3_4);
		panel_3_4.setLayout(new CardLayout(0, 0));

		JLabel lblNewLabel_4 = new JLabel(getNameC());
		panel_3_4.add(lblNewLabel_4, "name_622090901324900");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setBackground(Color.BLACK);
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));

		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.BLACK);
		panel_5.setBounds(304, 42, 163, 39);
		panel_2.add(panel_5);
		panel_5.setLayout(null);
		JLabel lblNewLabel_5 = new JLabel("");
		lblNewLabel_5.setBounds(0, 0, 163, 39);
		panel_5.add(lblNewLabel_5);
		lblNewLabel_5.setForeground(Color.WHITE);
		lblNewLabel_5.setFont(new Font("Gentium Book Basic", Font.PLAIN, 25));
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.CENTER);

		Thread clock = new Thread() {

			public void run() {
				while (true) {
					Calendar cal = new GregorianCalendar();
					heure = cal.get(Calendar.HOUR_OF_DAY);
					minute = cal.get(Calendar.MINUTE);
					seconde = cal.get(Calendar.SECOND);
					lblNewLabel_5.setText(String.valueOf(heure) + "h" + String.valueOf(minute) + "min"
							+ String.valueOf(seconde) + "s");
					try {
						sleep(500);
					} catch (InterruptedException ie) {
					}
				}
			}
		};
		clock.start();

		ImageIcon imageIcon = getimage();
		lblNewLabel_3.setIcon(imageIcon);

		JPanel panel_3_3 = new JPanel();
		panel_3_3.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		panel_3_3.setBackground(Color.BLACK);
		panel_3_3.setBounds(10, 56, 135, 30);
		panel_2.add(panel_3_3);
		panel_3_3.setLayout(new CardLayout(0, 0));

		JLabel lblEnvoyeur = new JLabel("Envoyeur :");
		lblEnvoyeur.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnvoyeur.setForeground(Color.WHITE);
		lblEnvoyeur.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));
		lblEnvoyeur.setBackground(Color.BLACK);
		panel_3_3.add(lblEnvoyeur, "name_683425413069500");

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setBounds(0, 421, 807, 66);
		panel_2.add(panel_1);
		panel_1.setLayout(null);

		JButton envoi = new JButton("");
		envoi.setBounds(255, 8, 41, 41);
		envoi.setFont(new Font("Imprint MT Shadow", Font.PLAIN, 17));
		envoi.setBackground(Color.BLACK);
		envoi.setForeground(new Color(255, 255, 255));
		ImageIcon imageIcon3 = new ImageIcon(new ImageIcon("C:\\Users\\Benjamin\\Pictures\\envoie.png").getImage()
		.getScaledInstance(41, 41, Image.SCALE_DEFAULT));
		envoi.setIcon(imageIcon3);
		envoi.setBorder(null);
		panel_1.add(envoi);
		

		JPanel paneltestmessage = new JPanel();
		paneltestmessage.setBackground(Color.RED);
		paneltestmessage.setBounds(158, 11, 10, 10);
		panel_1.add(paneltestmessage);

		JLabel testmessage = new JLabel("aucun message envoy\u00E9");
		testmessage.setHorizontalAlignment(SwingConstants.CENTER);
		testmessage.setForeground(Color.WHITE);
		testmessage.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		testmessage.setBounds(10, 17, 160, 38);
		panel_1.add(testmessage);

		JButton exit = new JButton("");
		exit.setForeground(Color.WHITE);
		exit.setFont(new Font("Imprint MT Shadow", Font.PLAIN, 17));
		exit.setBackground(Color.BLACK);
		exit.setBounds(738, 8, 41, 41);
		panel_1.add(exit);
		ImageIcon imageIcon4 = new ImageIcon(new ImageIcon("C:\\Users\\Benjamin\\Pictures\\porte.png").getImage()
				.getScaledInstance(41, 41, Image.SCALE_DEFAULT));
		exit.setIcon(imageIcon4);
		exit.setBorder(null);
		
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				    frame.dispose(); //ferme l'appli
			}
		});

		JTextArea message = new JTextArea("");
		message.setBounds(10, 174, 284, 236);
		panel_2.add(message);
		message.setFont(new Font("Gentium Book Basic", Font.BOLD, 17));
		message.setForeground(Color.WHITE);
		message.setLineWrap(true);
		message.setBackground(Color.black);
		message.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));

		JPanel panel_3_1 = new JPanel();
		panel_3_1.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		panel_3_1.setBounds(10, 138, 284, 30);
		panel_2.add(panel_3_1);
		panel_3_1.setBackground(new Color(0, 0, 0));

		JTextArea destinataire = new JTextArea();
		destinataire.setBounds(159, 97, 135, 30);
		panel_2.add(destinataire);
		destinataire.setFont(new Font("Gentium Book Basic", Font.BOLD, 17));
		destinataire.setForeground(Color.WHITE);
		destinataire.setLineWrap(true);
		destinataire.setBackground(Color.black);
		destinataire.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		panel_3.setBounds(159, 56, 135, 30);
		panel_2.add(panel_3);
		panel_3.setBackground(new Color(0, 0, 0));
		panel_3.setLayout(new CardLayout(0, 0));

		JLabel lblNewLabel = new JLabel("Destinataire :");
		panel_3.add(lblNewLabel, "name_683435622182200");
		lblNewLabel.setBackground(new Color(0, 0, 0));
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel panel = new JPanel();
		panel.setBorder(null);
		JScrollPane panelPane = new JScrollPane(panel);
		panelPane.setBounds(477, 56, 304, 354);
		panel_2.add(panelPane);
		panelPane.getViewport().setOpaque(false);
		panelPane.setBackground(SystemColor.controlHighlight);
		panel.setLayout(new GridLayout(0, 1));

		panelPane.setOpaque(false);
		panel.setOpaque(false);
					
		JPanel panel_3_2 = new JPanel();
		panel_3_2.setBackground(new Color(0, 0, 0));
		panel_3_2.setBounds(0, 0, 807, 45);
		panel_2.add(panel_3_2);
		panel_3_2.setLayout(null);
		lblNewLabel_3.setIcon(imageIcon);
		panel_2.add(lblNewLabel_3);
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/messagerie?serverTimezone=UTC",
					"root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT `Nom` FROM `contact` WHERE 1");

			ArrayList<String> lines = new ArrayList<>();

			while (res.next()) {
				lines.add(res.getString("Nom"));

			}

			ActionListener a2 = new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					String dest = destinataire.getText();
					boolean containsStr = lines.contains(dest);
					if (containsStr == true) {
						// Connexion � la bdd pour ajouter le message
						System.out.println("il existe");
						try {
							String message2 = namec + " : " + message.getText();
							Connection con = DriverManager.getConnection(
									"jdbc:mysql://localhost:3306/messagerie?serverTimezone=UTC", "root", "root");
							Statement st = con.createStatement();
							String sql = "INSERT INTO `message`(`Destinataire`, `envoyeur`, `message`) VALUES ('" + dest+ "','" + lblNewLabel_4.getText() + "','" + message2 + "')";

							st.executeUpdate(sql);

							paneltestmessage.setBackground(Color.green); //confirmation message envoy� panel = vert
							testmessage.setText("message envoy�"); //confirmation message envoy�
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						System.out.println("n'existe pas");
						paneltestmessage.setBackground(Color.red); //confirmation erreur message panel = rouge
						testmessage.setText("Erreur : Destinataire"); //confirmation erreur message
					}
				}
			};
			envoi.addActionListener(a2); //envoi du message vers la bdd ("INSERT INTO `message`(`Destinataire`, `envoyeur`, `message`)")
			/*
			 * }else if (containsStr == false){ System.out.println("n'existe pas"); }
			 */
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// liste des contacts de la personne connecte

		JButton b = new JButton();
		JButton btnNewButton_3 = new JButton();

		textField = new JTextField();
		textField.setForeground(new Color(255, 255, 255));
		textField.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));
		textField.setBackground(new Color(0, 0, 0));
		textField.setBounds(476, 8, 279, 26);
		panel_3_2.add(textField);
		textField.setColumns(10);
		textField.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, Color.WHITE));

		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/inscription?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();

			ResultSet res = st.executeQuery("SELECT `pseudo` FROM `inscriptions` WHERE 1");
			// String ligne = res.getString("colonne_1");
			ArrayList<String> lines = new ArrayList<>();

			while (res.next()) {
				lines.add(res.getString("pseudo"));

			}
			System.out.println(lines);
			int index = 0;
			JButton[][] array = new JButton[lines.size()][2];
			for (int i = 0; i < lines.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < lines.size()) {

						b = new JButton("");
						b.setPreferredSize(new Dimension(150, 50));
						array[i][j] = b;
						panel.add(array[i][j]);
						b.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b.setBackground(Color.black);
						b.setForeground(new Color(255, 255, 255));
						array[i][j].setText(lines.get(index));
						index = index + 1;
						b.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

						ActionListener a1 = new ActionListener() {
							public void actionPerformed(ActionEvent ae) {
								desti = ((AbstractButton) ae.getSource()).getText();
								System.out.println(desti);
								if (desti == ((AbstractButton) ae.getSource()).getText()) {
									popupmessage window2 = new popupmessage();
									window2.frame.setVisible(true);
								}
							}
						};
						b.addActionListener(a1);

					}
				}
			}
		} catch (Exception e) {
			System.out.println("une erreur" + e);
		}

		JButton rechercheC = new JButton("");
		rechercheC.setBackground(Color.BLACK);
		rechercheC.setBounds(760, 8, 25, 26);
		ImageIcon imageIcon2 = new ImageIcon(new ImageIcon("C:\\Users\\Benjamin\\Pictures\\search2.png").getImage()
				.getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		rechercheC.setIcon(imageIcon2);
		rechercheC.setBorder(null);
		panel_3_2.add(rechercheC);

		JButton wallpaper = new JButton("");
		wallpaper.setBounds(51, 8, 31, 31);
		panel_3_2.add(wallpaper);
		wallpaper.setBackground(new Color(0, 0, 0));
		wallpaper.setFont(new Font("Imprint MT Shadow", Font.PLAIN, 17));
		wallpaper.setForeground(Color.WHITE);
		ImageIcon imageIcon6 = new ImageIcon(new ImageIcon("C:\\Users\\Benjamin\\Pictures\\pinceau.png").getImage()
		.getScaledInstance(31, 31, Image.SCALE_DEFAULT));
		wallpaper.setIcon(imageIcon6);
		wallpaper.setBorder(null);

		JButton accueil = new JButton("");
		accueil.setForeground(Color.WHITE);
		accueil.setFont(new Font("Imprint MT Shadow", Font.PLAIN, 17));
		accueil.setBackground(Color.BLACK);
		accueil.setBounds(10, 8, 31, 31);
		panel_3_2.add(accueil);
		ImageIcon imageIcon5 = new ImageIcon(new ImageIcon("C:\\Users\\Benjamin\\Pictures\\accueil.png").getImage()
				.getScaledInstance(31, 31, Image.SCALE_DEFAULT));
		accueil.setIcon(imageIcon5);
		accueil.setBorder(null);

		accueil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				    accueil window2 = new accueil();
					window2.frame.setVisible(true);
				    frame.dispose(); //retour � la page accueil de l'appli
			}
		});
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/inscription?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();

			ResultSet res = st.executeQuery("SELECT `pseudo` FROM inscriptions WHERE 1");
			// String ligne = res.getString("colonne_1");
			ArrayList<String> lines = new ArrayList<>();

			while (res.next()) {
				lines.add(res.getString("pseudo"));

			}
			rechercheC.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					desti = textField.getText();
					System.out.println(desti);
					boolean containsStr = lines.contains(textField.getText());
					if (containsStr == true) {
						popupmessage window2 = new popupmessage();
						window2.frame.setVisible(true); //recherche le contact pour ensuite ouvrir popupmessage au destinataire cibl�
					} else {
						System.out.println("n'existe pas");
					}

				}
			});
			wallpaper.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					frame.dispose();
					theme window2 = new theme();
					window2.frame.setVisible(true); //ouvre la page theme pour ensuite changer le wallpaper

				}
			});

		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
	public static ImageIcon getimage() {
		return image;
	}

	public static String getdesti() {
		return desti;
	}
}
=======
package messagerie;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Dimension;
import java.awt.Window.Type;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import inscriptionconnexion.connexion;

import java.awt.List;
import javax.swing.JMenuItem;
import java.awt.Panel;
import java.awt.Color;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import java.awt.TextArea;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.JTextArea;
import java.awt.CardLayout;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.border.MatteBorder;

public class messagerie extends connexion {

	JFrame frame;
	String namec = getNameC();
	static String desti = "";
	String namelist = "";
	private JTextField textField;
	int heure;
	int minute;
	int seconde;
	public static ImageIcon image = new ImageIcon(new ImageIcon("C:\\\\Users\\\\Benjamin\\\\Pictures\\\\seoulcoree.jpg")
			.getImage().getScaledInstance(807, 487, Image.SCALE_DEFAULT));
	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					messagerie window = new messagerie();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public messagerie() {
		M_Project();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws SQLException
	 */
	private void M_Project() {

		// menu principal
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 813, 513);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Border border = BorderFactory.createLineBorder(Color.WHITE);

		JPanel panel_2 = new JPanel();
		panel_2.setBounds(0, 0, 807, 487);
		frame.getContentPane().add(panel_2);

		JLabel lblNewLabel_3 = new JLabel();
		lblNewLabel_3.setBounds(0, 0, 807, 487);

		panel_2.setLayout(null);

		ActionListener a = new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				frame.dispose();
				theme window = new theme();
				window.frame.setVisible(true);

			}

		};
		
				JLabel lblNewLabel_1 = new JLabel("Votre message :");
				lblNewLabel_1.setBounds(92, 141, 120, 22);
				panel_2.add(lblNewLabel_1);
				lblNewLabel_1.setForeground(new Color(255, 255, 255));
				lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
				lblNewLabel_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 18));

		JPanel panel_3_4 = new JPanel();
		panel_3_4.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		panel_3_4.setBackground(Color.BLACK);
		panel_3_4.setBounds(10, 97, 135, 30);
		panel_2.add(panel_3_4);
		panel_3_4.setLayout(new CardLayout(0, 0));

		JLabel lblNewLabel_4 = new JLabel(getNameC());
		panel_3_4.add(lblNewLabel_4, "name_622090901324900");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setBackground(Color.BLACK);
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));

		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.BLACK);
		panel_5.setBounds(304, 42, 163, 39);
		panel_2.add(panel_5);
		panel_5.setLayout(null);
		JLabel lblNewLabel_5 = new JLabel("");
		lblNewLabel_5.setBounds(0, 0, 163, 39);
		panel_5.add(lblNewLabel_5);
		lblNewLabel_5.setForeground(Color.WHITE);
		lblNewLabel_5.setFont(new Font("Gentium Book Basic", Font.PLAIN, 25));
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.CENTER);

		Thread clock = new Thread() {

			public void run() {
				while (true) {
					Calendar cal = new GregorianCalendar();
					heure = cal.get(Calendar.HOUR_OF_DAY);
					minute = cal.get(Calendar.MINUTE);
					seconde = cal.get(Calendar.SECOND);
					lblNewLabel_5.setText(String.valueOf(heure) + "h" + String.valueOf(minute) + "min"
							+ String.valueOf(seconde) + "s");
					try {
						sleep(500);
					} catch (InterruptedException ie) {
					}
				}
			}
		};
		clock.start();

		ImageIcon imageIcon = getimage();
		lblNewLabel_3.setIcon(imageIcon);

		JPanel panel_3_3 = new JPanel();
		panel_3_3.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		panel_3_3.setBackground(Color.BLACK);
		panel_3_3.setBounds(10, 56, 135, 30);
		panel_2.add(panel_3_3);
		panel_3_3.setLayout(new CardLayout(0, 0));

		JLabel lblEnvoyeur = new JLabel("Envoyeur :");
		lblEnvoyeur.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnvoyeur.setForeground(Color.WHITE);
		lblEnvoyeur.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));
		lblEnvoyeur.setBackground(Color.BLACK);
		panel_3_3.add(lblEnvoyeur, "name_683425413069500");

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setBounds(0, 421, 807, 66);
		panel_2.add(panel_1);
		panel_1.setLayout(null);

		JButton envoi = new JButton("");
		envoi.setBounds(255, 8, 41, 41);
		envoi.setFont(new Font("Imprint MT Shadow", Font.PLAIN, 17));
		envoi.setBackground(Color.BLACK);
		envoi.setForeground(new Color(255, 255, 255));
		ImageIcon imageIcon3 = new ImageIcon(new ImageIcon("C:\\Users\\Benjamin\\Pictures\\envoie.png").getImage()
		.getScaledInstance(41, 41, Image.SCALE_DEFAULT));
		envoi.setIcon(imageIcon3);
		envoi.setBorder(null);
		panel_1.add(envoi);
		

		JPanel paneltestmessage = new JPanel();
		paneltestmessage.setBackground(Color.RED);
		paneltestmessage.setBounds(158, 11, 10, 10);
		panel_1.add(paneltestmessage);

		JLabel testmessage = new JLabel("aucun message envoy\u00E9");
		testmessage.setHorizontalAlignment(SwingConstants.CENTER);
		testmessage.setForeground(Color.WHITE);
		testmessage.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		testmessage.setBounds(10, 17, 160, 38);
		panel_1.add(testmessage);

		JButton exit = new JButton("");
		exit.setForeground(Color.WHITE);
		exit.setFont(new Font("Imprint MT Shadow", Font.PLAIN, 17));
		exit.setBackground(Color.BLACK);
		exit.setBounds(738, 8, 41, 41);
		panel_1.add(exit);
		ImageIcon imageIcon4 = new ImageIcon(new ImageIcon("C:\\Users\\Benjamin\\Pictures\\porte.png").getImage()
				.getScaledInstance(41, 41, Image.SCALE_DEFAULT));
		exit.setIcon(imageIcon4);
		exit.setBorder(null);
		
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				    frame.dispose(); //ferme l'appli
			}
		});

		JTextArea message = new JTextArea("");
		message.setBounds(10, 174, 284, 236);
		panel_2.add(message);
		message.setFont(new Font("Gentium Book Basic", Font.BOLD, 17));
		message.setForeground(Color.WHITE);
		message.setLineWrap(true);
		message.setBackground(Color.black);
		message.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));

		JPanel panel_3_1 = new JPanel();
		panel_3_1.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		panel_3_1.setBounds(10, 138, 284, 30);
		panel_2.add(panel_3_1);
		panel_3_1.setBackground(new Color(0, 0, 0));

		JTextArea destinataire = new JTextArea();
		destinataire.setBounds(159, 97, 135, 30);
		panel_2.add(destinataire);
		destinataire.setFont(new Font("Gentium Book Basic", Font.BOLD, 17));
		destinataire.setForeground(Color.WHITE);
		destinataire.setLineWrap(true);
		destinataire.setBackground(Color.black);
		destinataire.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.WHITE));
		panel_3.setBounds(159, 56, 135, 30);
		panel_2.add(panel_3);
		panel_3.setBackground(new Color(0, 0, 0));
		panel_3.setLayout(new CardLayout(0, 0));

		JLabel lblNewLabel = new JLabel("Destinataire :");
		panel_3.add(lblNewLabel, "name_683435622182200");
		lblNewLabel.setBackground(new Color(0, 0, 0));
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel panel = new JPanel();
		panel.setBorder(null);
		JScrollPane panelPane = new JScrollPane(panel);
		panelPane.setBounds(477, 56, 304, 354);
		panel_2.add(panelPane);
		panelPane.getViewport().setOpaque(false);
		panelPane.setBackground(SystemColor.controlHighlight);
		panel.setLayout(new GridLayout(0, 1));

		panelPane.setOpaque(false);
		panel.setOpaque(false);
					
		JPanel panel_3_2 = new JPanel();
		panel_3_2.setBackground(new Color(0, 0, 0));
		panel_3_2.setBounds(0, 0, 807, 45);
		panel_2.add(panel_3_2);
		panel_3_2.setLayout(null);
		lblNewLabel_3.setIcon(imageIcon);
		panel_2.add(lblNewLabel_3);
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/messagerie?serverTimezone=UTC",
					"root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT `Nom` FROM `contact` WHERE 1");

			ArrayList<String> lines = new ArrayList<>();

			while (res.next()) {
				lines.add(res.getString("Nom"));

			}

			ActionListener a2 = new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					String dest = destinataire.getText();
					boolean containsStr = lines.contains(dest);
					if (containsStr == true) {
						// Connexion � la bdd pour ajouter le message
						System.out.println("il existe");
						try {
							String message2 = namec + " : " + message.getText();
							Connection con = DriverManager.getConnection(
									"jdbc:mysql://localhost:3306/messagerie?serverTimezone=UTC", "root", "root");
							Statement st = con.createStatement();
							String sql = "INSERT INTO `message`(`Destinataire`, `envoyeur`, `message`) VALUES ('" + dest+ "','" + lblNewLabel_4.getText() + "','" + message2 + "')";

							st.executeUpdate(sql);

							paneltestmessage.setBackground(Color.green); //confirmation message envoy� panel = vert
							testmessage.setText("message envoy�"); //confirmation message envoy�
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						System.out.println("n'existe pas");
						paneltestmessage.setBackground(Color.red); //confirmation erreur message panel = rouge
						testmessage.setText("Erreur : Destinataire"); //confirmation erreur message
					}
				}
			};
			envoi.addActionListener(a2); //envoi du message vers la bdd ("INSERT INTO `message`(`Destinataire`, `envoyeur`, `message`)")
			/*
			 * }else if (containsStr == false){ System.out.println("n'existe pas"); }
			 */
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// liste des contacts de la personne connecte

		JButton b = new JButton();
		JButton btnNewButton_3 = new JButton();

		textField = new JTextField();
		textField.setForeground(new Color(255, 255, 255));
		textField.setFont(new Font("Gentium Book Basic", Font.PLAIN, 17));
		textField.setBackground(new Color(0, 0, 0));
		textField.setBounds(476, 8, 279, 26);
		panel_3_2.add(textField);
		textField.setColumns(10);
		textField.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, Color.WHITE));

		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/inscription?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();

			ResultSet res = st.executeQuery("SELECT `pseudo` FROM `inscriptions` WHERE 1");
			// String ligne = res.getString("colonne_1");
			ArrayList<String> lines = new ArrayList<>();

			while (res.next()) {
				lines.add(res.getString("pseudo"));

			}
			System.out.println(lines);
			int index = 0;
			JButton[][] array = new JButton[lines.size()][2];
			for (int i = 0; i < lines.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < lines.size()) {

						b = new JButton("");
						b.setPreferredSize(new Dimension(150, 50));
						array[i][j] = b;
						panel.add(array[i][j]);
						b.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b.setBackground(Color.black);
						b.setForeground(new Color(255, 255, 255));
						array[i][j].setText(lines.get(index));
						index = index + 1;
						b.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

						ActionListener a1 = new ActionListener() {
							public void actionPerformed(ActionEvent ae) {
								desti = ((AbstractButton) ae.getSource()).getText();
								System.out.println(desti);
								if (desti == ((AbstractButton) ae.getSource()).getText()) {
									popupmessage window2 = new popupmessage();
									window2.frame.setVisible(true);
								}
							}
						};
						b.addActionListener(a1);

					}
				}
			}
		} catch (Exception e) {
			System.out.println("une erreur" + e);
		}

		JButton rechercheC = new JButton("");
		rechercheC.setBackground(Color.BLACK);
		rechercheC.setBounds(760, 8, 25, 26);
		ImageIcon imageIcon2 = new ImageIcon(new ImageIcon("C:\\Users\\Benjamin\\Pictures\\search2.png").getImage()
				.getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		rechercheC.setIcon(imageIcon2);
		rechercheC.setBorder(null);
		panel_3_2.add(rechercheC);

		JButton wallpaper = new JButton("");
		wallpaper.setBounds(51, 8, 31, 31);
		panel_3_2.add(wallpaper);
		wallpaper.setBackground(new Color(0, 0, 0));
		wallpaper.setFont(new Font("Imprint MT Shadow", Font.PLAIN, 17));
		wallpaper.setForeground(Color.WHITE);
		ImageIcon imageIcon6 = new ImageIcon(new ImageIcon("C:\\Users\\Benjamin\\Pictures\\pinceau.png").getImage()
		.getScaledInstance(31, 31, Image.SCALE_DEFAULT));
		wallpaper.setIcon(imageIcon6);
		wallpaper.setBorder(null);

		JButton accueil = new JButton("");
		accueil.setForeground(Color.WHITE);
		accueil.setFont(new Font("Imprint MT Shadow", Font.PLAIN, 17));
		accueil.setBackground(Color.BLACK);
		accueil.setBounds(10, 8, 31, 31);
		panel_3_2.add(accueil);
		ImageIcon imageIcon5 = new ImageIcon(new ImageIcon("C:\\Users\\Benjamin\\Pictures\\accueil.png").getImage()
				.getScaledInstance(31, 31, Image.SCALE_DEFAULT));
		accueil.setIcon(imageIcon5);
		accueil.setBorder(null);

		accueil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				    accueil window2 = new accueil();
					window2.frame.setVisible(true);
				    frame.dispose(); //retour � la page accueil de l'appli
			}
		});
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/inscription?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();

			ResultSet res = st.executeQuery("SELECT `pseudo` FROM inscriptions WHERE 1");
			// String ligne = res.getString("colonne_1");
			ArrayList<String> lines = new ArrayList<>();

			while (res.next()) {
				lines.add(res.getString("pseudo"));

			}
			rechercheC.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					desti = textField.getText();
					System.out.println(desti);
					boolean containsStr = lines.contains(textField.getText());
					if (containsStr == true) {
						popupmessage window2 = new popupmessage();
						window2.frame.setVisible(true); //recherche le contact pour ensuite ouvrir popupmessage au destinataire cibl�
					} else {
						System.out.println("n'existe pas");
					}

				}
			});
			wallpaper.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					frame.dispose();
					theme window2 = new theme();
					window2.frame.setVisible(true); //ouvre la page theme pour ensuite changer le wallpaper

				}
			});

		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
	public static ImageIcon getimage() {
		return image;
	}

	public static String getdesti() {
		return desti;
	}
}
>>>>>>> 7e5869195ae5aa3ed2f0afc6e62d3313b2f58985
