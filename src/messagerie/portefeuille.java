<<<<<<< HEAD
package messagerie;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import java.awt.CardLayout;
import javax.swing.SwingConstants;

import inscriptionconnexion.connexion;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class portefeuille extends connexion{

	JFrame frame;
	public double TotaldepenseD;
    String personneco = getNameC();
    
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					portefeuille window = new portefeuille();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public portefeuille() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 904, 473);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel_10 = new JPanel();
		panel_10.setBounds(528, 187, 349, 238);
		frame.getContentPane().add(panel_10);
		panel_10.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 349, 238);
		panel_10.add(scrollPane);
		JPanel panel_7 = new JPanel();
		scrollPane.setViewportView(panel_7);
		panel_7.setLayout(new GridLayout(0, 1));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(10, 120, 508, 44);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblVotrePortefauille = new JLabel("Wallet de");
		lblVotrePortefauille.setBounds(194, 11, 58, 19);
		lblVotrePortefauille.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		panel_1.add(lblVotrePortefauille);
		lblVotrePortefauille.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblVotrePortefauille_1 = new JLabel(personneco);
		lblVotrePortefauille_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblVotrePortefauille_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		lblVotrePortefauille_1.setBounds(256, 11, 59, 19);
		panel_1.add(lblVotrePortefauille_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(10, 175, 508, 250);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("montant  disponilbe sur votre compte :");
		lblNewLabel_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1.setBounds(34, 217, 253, 22);
		panel_2.add(lblNewLabel_1);
		
		JLabel TotalCompteLB = new JLabel("0\u20AC");
		TotalCompteLB.setHorizontalAlignment(SwingConstants.CENTER);
		TotalCompteLB.setBounds(234, 217, 96, 22);
		panel_2.add(TotalCompteLB);
		
		JButton btnClearCompte = new JButton("Clear compte");
		btnClearCompte.setFont(new Font("Gentium Book Basic", Font.PLAIN, 11));
		btnClearCompte.setBackground(Color.WHITE);
		btnClearCompte.setBounds(325, 218, 173, 23);
		panel_2.add(btnClearCompte);
		
		JLabel lblNewLabel_3 = new JLabel("montant total des d\u00E9penses :");
		lblNewLabel_3.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_3.setBounds(33, 194, 148, 22);
		panel_2.add(lblNewLabel_3);
		
		JLabel TotaldepenseLB = new JLabel("0\u20AC");
		TotaldepenseLB.setHorizontalAlignment(SwingConstants.CENTER);
		TotaldepenseLB.setBounds(191, 194, 96, 22);
		panel_2.add(TotaldepenseLB);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.BLACK);
		panel_4.setBounds(0, 0, 900, 23);
		frame.getContentPane().add(panel_4);
		panel_4.setLayout(null);
		
		JButton btnNewButton = new JButton("refresh");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(614, 0, 126, 23);
		panel_4.add(btnNewButton);
		
		JButton btnFermer = new JButton("menu");
		btnFermer.setBackground(Color.WHITE);
		btnFermer.setBounds(750, 0, 126, 23);
		panel_4.add(btnFermer);
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accueil window = new accueil();
				window.frame.setVisible(true);
				frame.dispose();
			}
		});
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				portefeuille window = new portefeuille();
				window.frame.setVisible(true);
				frame.dispose();
			}
		});
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_5.setBounds(528, 153, 250, 23);
		frame.getContentPane().add(panel_5);
		panel_5.setLayout(new CardLayout(0, 0));
		
		JLabel lblVosNotes = new JLabel("Vos notes");
		panel_5.add(lblVosNotes, "name_695435672197900");
		lblVosNotes.setHorizontalAlignment(SwingConstants.CENTER);
		lblVosNotes.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(528, 87, 349, 55);
		frame.getContentPane().add(textArea);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(Color.WHITE);
		panel_6.setBounds(528, 34, 349, 44);
		frame.getContentPane().add(panel_6);
		panel_6.setLayout(new CardLayout(0, 0));
		
		JLabel lblAjoutDpense_1 = new JLabel("Ajout note");
		panel_6.add(lblAjoutDpense_1, "name_687243045278200");
		lblAjoutDpense_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblAjoutDpense_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		
		JButton notebtn = new JButton("E");
		notebtn.setBackground(Color.WHITE);
		notebtn.setBounds(788, 153, 89, 23);
		frame.getContentPane().add(notebtn);
		
		notebtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
					Statement st = con.createStatement();
					st.executeUpdate("INSERT INTO `Notes`(`note`,`utilisateur`) VALUES ('"+textArea.getText()+"','"+personneco+"')");

				} catch (SQLException e1) {

					e1.printStackTrace();
				}
				    
			}
		});
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		panel_3.setBounds(268, 34, 250, 44);
		frame.getContentPane().add(panel_3);
		panel_3.setLayout(new CardLayout(0, 0));
		
		JLabel lblAjoutDpense = new JLabel("ajout d\u00E9pense");
		lblAjoutDpense.setHorizontalAlignment(SwingConstants.CENTER);
		lblAjoutDpense.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		panel_3.add(lblAjoutDpense, "name_687915893725400");
		
		JPanel panel_8 = new JPanel();
		panel_8.setBackground(Color.WHITE);
		panel_8.setBounds(10, 34, 250, 44);
		frame.getContentPane().add(panel_8);
		panel_8.setLayout(new CardLayout(0, 0));
		
		JLabel lblAjoutCompte = new JLabel("ajout argent ");
		lblAjoutCompte.setHorizontalAlignment(SwingConstants.CENTER);
		lblAjoutCompte.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		panel_8.add(lblAjoutCompte, "name_687914880487400");
		
		JTextField ajoutargent = new JTextField();
		ajoutargent.setColumns(10);
		ajoutargent.setBounds(10, 89, 190, 20);
		frame.getContentPane().add(ajoutargent);
		JButton ajoutargentBTN = new JButton("E");
		ajoutargentBTN.setBackground(Color.WHITE);
		ajoutargentBTN.setBounds(210, 88, 50, 23);
		frame.getContentPane().add(ajoutargentBTN);
		
		JButton btnClearDpenses = new JButton("Clear d\u00E9penses");
		btnClearDpenses.setFont(new Font("Gentium Book Basic", Font.PLAIN, 11));
		btnClearDpenses.setBackground(Color.WHITE);
		btnClearDpenses.setBounds(325, 195, 173, 23);
		panel_2.add(btnClearDpenses);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(34, 35, 200, 148);
		panel_2.add(scrollPane_1);
		
		JPanel panel = new JPanel();
		scrollPane_1.setViewportView(panel);
		panel.setLayout(new GridLayout(0, 1));
		
		JScrollPane scrollPane_1_1 = new JScrollPane();
		scrollPane_1_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1_1.setBounds(269, 35, 201, 148);
		panel_2.add(scrollPane_1_1);
		
		JPanel panel_9 = new JPanel();
		scrollPane_1_1.setViewportView(panel_9);
		panel_9.setLayout(new GridLayout(0, 1));
		panel_9.setLayout(new GridLayout(0, 1));
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Vos d\u00E9penses");
		lblNewLabel_1_1_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1_1_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		lblNewLabel_1_1_1.setBounds(269, 11, 158, 22);
		panel_2.add(lblNewLabel_1_1_1);
		
		JLabel lblNewLabel_1_1_1_1 = new JLabel("Vos ajouts total ");
		lblNewLabel_1_1_1_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1_1_1_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		lblNewLabel_1_1_1_1.setBounds(34, 11, 158, 22);
		panel_2.add(lblNewLabel_1_1_1_1);
		
		JLabel lblNewLabel = new JLabel("\"");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(118, 15, 116, 14);
		panel_2.add(lblNewLabel);
		
		ajoutargentBTN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double v = Double.parseDouble(ajoutargent.getText());
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
					Statement st = con.createStatement();
					st.executeUpdate("INSERT INTO `gestionportefeuille`(`ajoutargent`,`Utilisateur`) VALUES ('"+v+"','"+personneco+"')");

				} catch (SQLException e1) {

					e1.printStackTrace();
				}
				    
			}
		});
		
		JTextField ajoutdepense = new JTextField();
		ajoutdepense.setColumns(10);
		ajoutdepense.setBounds(268, 89, 190, 20);
		frame.getContentPane().add(ajoutdepense);
		
		JButton ajoutdepenseBTN = new JButton("E");
		ajoutdepenseBTN.setBackground(Color.WHITE);
		ajoutdepenseBTN.setBounds(468, 88, 50, 23);
		frame.getContentPane().add(ajoutdepenseBTN);
		
		ajoutdepenseBTN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double v = Double.parseDouble(ajoutdepense.getText());
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
					Statement st = con.createStatement();
					st.executeUpdate("INSERT INTO `gestionportefeuille`(`ajoutdepense`,`Utilisateur`) VALUES ('"+v+"','"+personneco+"')");

				} catch (SQLException e1) {

					e1.printStackTrace();
				}
				    
			}
		});
		
		JButton b = new JButton();
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT ajoutargent FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("ajoutargent"));

			}
			
			int index = 0;
			JButton[][] array = new JButton[TC.size()][2];
			for (int i = 0; i < TC.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < TC.size()) {

						b = new JButton("");
						b.setPreferredSize(new Dimension(180, 30));
						array[i][j] = b;
						panel.add(array[i][j]);
						b.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b.setBackground(Color.WHITE);
						array[i][j].setText(TC.get(index));
						index = index + 1;
						b.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
					}
				}
			}
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}
		JButton b1 = new JButton();
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT ajoutdepense FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("ajoutdepense"));

			}
			
			int index = 0;
			JButton[][] array = new JButton[TC.size()][2];
			for (int i = 0; i < TC.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < TC.size()) {

						b1 = new JButton("");
						b1.setPreferredSize(new Dimension(180, 30));
						array[i][j] = b1;
						panel_9.add(array[i][j]);
						b1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b1.setBackground(Color.WHITE);
						array[i][j].setText(TC.get(index));
						index = index + 1;
						b1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
					}
				}
			}
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT note FROM `Notes` WHERE `utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("note"));

			}
			System.out.println(TC);
			int index = 0;
			JButton[][] array = new JButton[TC.size()][2];
			for (int i = 0; i < TC.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < TC.size()) {

						b1 = new JButton("");
						b1.setPreferredSize(new Dimension(300, 30));
						array[i][j] = b1;
						panel_7.add(array[i][j]);
						b1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b1.setBackground(Color.WHITE);
						array[i][j].setText(TC.get(index));
						index = index + 1;
						b1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
					}
				}
			}
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT SUM(ajoutargent) FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("SUM(ajoutargent)"));

			}
			
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT SUM(ajoutdepense) FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("SUM(ajoutdepense)"));

			}
			TotaldepenseLB.setText(TC.get(0));
			TotaldepenseD = Double.parseDouble(TC.get(0));
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT SUM(ajoutargent) FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("SUM(ajoutargent)"));
			}
			double TotalCompteD = Double.parseDouble(TC.get(0));
			double t = TotalCompteD - TotaldepenseD;
			TotalCompteLB.setText(""+t);
			lblNewLabel.setText(""+TotalCompteD);
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}

		
	}
}
=======
package messagerie;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import java.awt.CardLayout;
import javax.swing.SwingConstants;

import inscriptionconnexion.connexion;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class portefeuille extends connexion{

	JFrame frame;
	public double TotaldepenseD;
    String personneco = getNameC();
    
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					portefeuille window = new portefeuille();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public portefeuille() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 904, 473);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel_10 = new JPanel();
		panel_10.setBounds(528, 187, 349, 238);
		frame.getContentPane().add(panel_10);
		panel_10.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 349, 238);
		panel_10.add(scrollPane);
		JPanel panel_7 = new JPanel();
		scrollPane.setViewportView(panel_7);
		panel_7.setLayout(new GridLayout(0, 1));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(10, 120, 508, 44);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblVotrePortefauille = new JLabel("Wallet de");
		lblVotrePortefauille.setBounds(194, 11, 58, 19);
		lblVotrePortefauille.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		panel_1.add(lblVotrePortefauille);
		lblVotrePortefauille.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblVotrePortefauille_1 = new JLabel(personneco);
		lblVotrePortefauille_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblVotrePortefauille_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		lblVotrePortefauille_1.setBounds(256, 11, 59, 19);
		panel_1.add(lblVotrePortefauille_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(10, 175, 508, 250);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("montant  disponilbe sur votre compte :");
		lblNewLabel_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1.setBounds(34, 217, 253, 22);
		panel_2.add(lblNewLabel_1);
		
		JLabel TotalCompteLB = new JLabel("0\u20AC");
		TotalCompteLB.setHorizontalAlignment(SwingConstants.CENTER);
		TotalCompteLB.setBounds(234, 217, 96, 22);
		panel_2.add(TotalCompteLB);
		
		JButton btnClearCompte = new JButton("Clear compte");
		btnClearCompte.setFont(new Font("Gentium Book Basic", Font.PLAIN, 11));
		btnClearCompte.setBackground(Color.WHITE);
		btnClearCompte.setBounds(325, 218, 173, 23);
		panel_2.add(btnClearCompte);
		
		JLabel lblNewLabel_3 = new JLabel("montant total des d\u00E9penses :");
		lblNewLabel_3.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_3.setBounds(33, 194, 148, 22);
		panel_2.add(lblNewLabel_3);
		
		JLabel TotaldepenseLB = new JLabel("0\u20AC");
		TotaldepenseLB.setHorizontalAlignment(SwingConstants.CENTER);
		TotaldepenseLB.setBounds(191, 194, 96, 22);
		panel_2.add(TotaldepenseLB);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.BLACK);
		panel_4.setBounds(0, 0, 900, 23);
		frame.getContentPane().add(panel_4);
		panel_4.setLayout(null);
		
		JButton btnNewButton = new JButton("refresh");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(614, 0, 126, 23);
		panel_4.add(btnNewButton);
		
		JButton btnFermer = new JButton("menu");
		btnFermer.setBackground(Color.WHITE);
		btnFermer.setBounds(750, 0, 126, 23);
		panel_4.add(btnFermer);
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accueil window = new accueil();
				window.frame.setVisible(true);
				frame.dispose();
			}
		});
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				portefeuille window = new portefeuille();
				window.frame.setVisible(true);
				frame.dispose();
			}
		});
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_5.setBounds(528, 153, 250, 23);
		frame.getContentPane().add(panel_5);
		panel_5.setLayout(new CardLayout(0, 0));
		
		JLabel lblVosNotes = new JLabel("Vos notes");
		panel_5.add(lblVosNotes, "name_695435672197900");
		lblVosNotes.setHorizontalAlignment(SwingConstants.CENTER);
		lblVosNotes.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(528, 87, 349, 55);
		frame.getContentPane().add(textArea);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(Color.WHITE);
		panel_6.setBounds(528, 34, 349, 44);
		frame.getContentPane().add(panel_6);
		panel_6.setLayout(new CardLayout(0, 0));
		
		JLabel lblAjoutDpense_1 = new JLabel("Ajout note");
		panel_6.add(lblAjoutDpense_1, "name_687243045278200");
		lblAjoutDpense_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblAjoutDpense_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		
		JButton notebtn = new JButton("E");
		notebtn.setBackground(Color.WHITE);
		notebtn.setBounds(788, 153, 89, 23);
		frame.getContentPane().add(notebtn);
		
		notebtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
					Statement st = con.createStatement();
					st.executeUpdate("INSERT INTO `Notes`(`note`,`utilisateur`) VALUES ('"+textArea.getText()+"','"+personneco+"')");

				} catch (SQLException e1) {

					e1.printStackTrace();
				}
				    
			}
		});
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		panel_3.setBounds(268, 34, 250, 44);
		frame.getContentPane().add(panel_3);
		panel_3.setLayout(new CardLayout(0, 0));
		
		JLabel lblAjoutDpense = new JLabel("ajout d\u00E9pense");
		lblAjoutDpense.setHorizontalAlignment(SwingConstants.CENTER);
		lblAjoutDpense.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		panel_3.add(lblAjoutDpense, "name_687915893725400");
		
		JPanel panel_8 = new JPanel();
		panel_8.setBackground(Color.WHITE);
		panel_8.setBounds(10, 34, 250, 44);
		frame.getContentPane().add(panel_8);
		panel_8.setLayout(new CardLayout(0, 0));
		
		JLabel lblAjoutCompte = new JLabel("ajout argent ");
		lblAjoutCompte.setHorizontalAlignment(SwingConstants.CENTER);
		lblAjoutCompte.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		panel_8.add(lblAjoutCompte, "name_687914880487400");
		
		JTextField ajoutargent = new JTextField();
		ajoutargent.setColumns(10);
		ajoutargent.setBounds(10, 89, 190, 20);
		frame.getContentPane().add(ajoutargent);
		JButton ajoutargentBTN = new JButton("E");
		ajoutargentBTN.setBackground(Color.WHITE);
		ajoutargentBTN.setBounds(210, 88, 50, 23);
		frame.getContentPane().add(ajoutargentBTN);
		
		JButton btnClearDpenses = new JButton("Clear d\u00E9penses");
		btnClearDpenses.setFont(new Font("Gentium Book Basic", Font.PLAIN, 11));
		btnClearDpenses.setBackground(Color.WHITE);
		btnClearDpenses.setBounds(325, 195, 173, 23);
		panel_2.add(btnClearDpenses);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(34, 35, 200, 148);
		panel_2.add(scrollPane_1);
		
		JPanel panel = new JPanel();
		scrollPane_1.setViewportView(panel);
		panel.setLayout(new GridLayout(0, 1));
		
		JScrollPane scrollPane_1_1 = new JScrollPane();
		scrollPane_1_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1_1.setBounds(269, 35, 201, 148);
		panel_2.add(scrollPane_1_1);
		
		JPanel panel_9 = new JPanel();
		scrollPane_1_1.setViewportView(panel_9);
		panel_9.setLayout(new GridLayout(0, 1));
		panel_9.setLayout(new GridLayout(0, 1));
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Vos d\u00E9penses");
		lblNewLabel_1_1_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1_1_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		lblNewLabel_1_1_1.setBounds(269, 11, 158, 22);
		panel_2.add(lblNewLabel_1_1_1);
		
		JLabel lblNewLabel_1_1_1_1 = new JLabel("Vos ajouts total ");
		lblNewLabel_1_1_1_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_1_1_1_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		lblNewLabel_1_1_1_1.setBounds(34, 11, 158, 22);
		panel_2.add(lblNewLabel_1_1_1_1);
		
		JLabel lblNewLabel = new JLabel("\"");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(118, 15, 116, 14);
		panel_2.add(lblNewLabel);
		
		ajoutargentBTN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double v = Double.parseDouble(ajoutargent.getText());
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
					Statement st = con.createStatement();
					st.executeUpdate("INSERT INTO `gestionportefeuille`(`ajoutargent`,`Utilisateur`) VALUES ('"+v+"','"+personneco+"')");

				} catch (SQLException e1) {

					e1.printStackTrace();
				}
				    
			}
		});
		
		JTextField ajoutdepense = new JTextField();
		ajoutdepense.setColumns(10);
		ajoutdepense.setBounds(268, 89, 190, 20);
		frame.getContentPane().add(ajoutdepense);
		
		JButton ajoutdepenseBTN = new JButton("E");
		ajoutdepenseBTN.setBackground(Color.WHITE);
		ajoutdepenseBTN.setBounds(468, 88, 50, 23);
		frame.getContentPane().add(ajoutdepenseBTN);
		
		ajoutdepenseBTN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double v = Double.parseDouble(ajoutdepense.getText());
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
					Statement st = con.createStatement();
					st.executeUpdate("INSERT INTO `gestionportefeuille`(`ajoutdepense`,`Utilisateur`) VALUES ('"+v+"','"+personneco+"')");

				} catch (SQLException e1) {

					e1.printStackTrace();
				}
				    
			}
		});
		
		JButton b = new JButton();
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT ajoutargent FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("ajoutargent"));

			}
			
			int index = 0;
			JButton[][] array = new JButton[TC.size()][2];
			for (int i = 0; i < TC.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < TC.size()) {

						b = new JButton("");
						b.setPreferredSize(new Dimension(180, 30));
						array[i][j] = b;
						panel.add(array[i][j]);
						b.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b.setBackground(Color.WHITE);
						array[i][j].setText(TC.get(index));
						index = index + 1;
						b.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
					}
				}
			}
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}
		JButton b1 = new JButton();
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT ajoutdepense FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("ajoutdepense"));

			}
			
			int index = 0;
			JButton[][] array = new JButton[TC.size()][2];
			for (int i = 0; i < TC.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < TC.size()) {

						b1 = new JButton("");
						b1.setPreferredSize(new Dimension(180, 30));
						array[i][j] = b1;
						panel_9.add(array[i][j]);
						b1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b1.setBackground(Color.WHITE);
						array[i][j].setText(TC.get(index));
						index = index + 1;
						b1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
					}
				}
			}
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT note FROM `Notes` WHERE `utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("note"));

			}
			System.out.println(TC);
			int index = 0;
			JButton[][] array = new JButton[TC.size()][2];
			for (int i = 0; i < TC.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < TC.size()) {

						b1 = new JButton("");
						b1.setPreferredSize(new Dimension(300, 30));
						array[i][j] = b1;
						panel_7.add(array[i][j]);
						b1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b1.setBackground(Color.WHITE);
						array[i][j].setText(TC.get(index));
						index = index + 1;
						b1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
					}
				}
			}
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}
		
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT SUM(ajoutargent) FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("SUM(ajoutargent)"));

			}
			
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT SUM(ajoutdepense) FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("SUM(ajoutdepense)"));

			}
			TotaldepenseLB.setText(TC.get(0));
			TotaldepenseD = Double.parseDouble(TC.get(0));
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/portefeuille?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT SUM(ajoutargent) FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");
			
			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("SUM(ajoutargent)"));
			}
			double TotalCompteD = Double.parseDouble(TC.get(0));
			double t = TotalCompteD - TotaldepenseD;
			TotalCompteLB.setText(""+t);
			lblNewLabel.setText(""+TotalCompteD);
			
		} catch (SQLException e1) {

			e1.printStackTrace();
		
		}

		
	}
}
>>>>>>> 7e5869195ae5aa3ed2f0afc6e62d3313b2f58985
