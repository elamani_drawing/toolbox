<<<<<<< HEAD
package messagerie;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.border.MatteBorder;

public class calculatrice {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					calculatrice window = new calculatrice();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public calculatrice() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 328, 551);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.BLACK));
		panel.setForeground(Color.WHITE);
		panel.setBounds(10, 11, 294, 102);
		frame.getContentPane().add(panel);
		panel.setLayout(new CardLayout(0, 0));
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 70));
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(lblNewLabel, "name_682113463660500");
		
		ArrayList<String> calcul = new ArrayList<>();
		
		JButton btnNewButton_4 = new JButton("-");
		btnNewButton_4.setBackground(Color.WHITE);
		btnNewButton_4.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4.setBounds(162, 125, 66, 66);
		frame.getContentPane().add(btnNewButton_4);
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText("-");
				calcul.add("-");
			}
		});
		
		JButton btnNewButton_4_1 = new JButton(">");
		btnNewButton_4_1.setBackground(Color.WHITE);
		btnNewButton_4_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_1.setBounds(10, 433, 66, 66);
		frame.getContentPane().add(btnNewButton_4_1);
		btnNewButton_4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText("");

			}
		});
		
		JButton btnNewButton_4_2 = new JButton("+");
		btnNewButton_4_2.setBackground(Color.WHITE);
		btnNewButton_4_2.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_2.setBounds(86, 125, 66, 66);
		frame.getContentPane().add(btnNewButton_4_2);
		
		JButton btnNewButton_4_3 = new JButton("*");
		btnNewButton_4_3.setBackground(Color.WHITE);
		btnNewButton_4_3.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_3.setBounds(238, 125, 66, 66);
		frame.getContentPane().add(btnNewButton_4_3);
		
		JButton btnNewButton_4_4 = new JButton("4");
		btnNewButton_4_4.setBackground(Color.WHITE);
		btnNewButton_4_4.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4.setBounds(86, 279, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4);
		
		JButton btnNewButton_4_4_1 = new JButton("2");
		btnNewButton_4_4_1.setBackground(Color.WHITE);
		btnNewButton_4_4_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_1.setBounds(162, 202, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_1);
		
		JButton btnNewButton_4_4_2 = new JButton("3");
		btnNewButton_4_4_2.setBackground(Color.WHITE);
		btnNewButton_4_4_2.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_2.setBounds(238, 202, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_2);
		
		JButton btnNewButton_4_4_3 = new JButton("1");
		btnNewButton_4_4_3.setBackground(Color.WHITE);
		btnNewButton_4_4_3.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_3.setBounds(86, 202, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_3);
		
		JButton btnNewButton_4_4_4 = new JButton("5");
		btnNewButton_4_4_4.setBackground(Color.WHITE);
		btnNewButton_4_4_4.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_4.setBounds(162, 279, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_4);
		
		JButton btnNewButton_4_4_5 = new JButton("6");
		btnNewButton_4_4_5.setBackground(Color.WHITE);
		btnNewButton_4_4_5.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_5.setBounds(238, 279, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_5);
		
		JButton btnNewButton_4_4_6 = new JButton("7");
		btnNewButton_4_4_6.setBackground(Color.WHITE);
		btnNewButton_4_4_6.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_6.setBounds(86, 356, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_6);
		
		JButton btnNewButton_4_4_7 = new JButton("8");
		btnNewButton_4_4_7.setBackground(Color.WHITE);
		btnNewButton_4_4_7.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_7.setBounds(162, 356, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_7);
		
		JButton btnNewButton_4_4_8 = new JButton("9");
		btnNewButton_4_4_8.setBackground(Color.WHITE);
		btnNewButton_4_4_8.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_8.setBounds(238, 356, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_8);
		
		JButton btnNewButton_4_4_9 = new JButton("0");
		btnNewButton_4_4_9.setBackground(Color.WHITE);
		btnNewButton_4_4_9.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_9.setBounds(162, 433, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_9);
		
		JButton btnNewButton_4_3_1 = new JButton("/");
		btnNewButton_4_3_1.setBackground(Color.WHITE);
		btnNewButton_4_3_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_3_1.setBounds(10, 124, 66, 66);
		frame.getContentPane().add(btnNewButton_4_3_1);
		
		JButton btnNewButton_4_1_1 = new JButton("=");
		btnNewButton_4_1_1.setBackground(Color.WHITE);
		btnNewButton_4_1_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_1_1.setBounds(10, 202, 66, 220);
		frame.getContentPane().add(btnNewButton_4_1_1);
		
		JButton btnNewButton_4_1_1_1 = new JButton(",");
		btnNewButton_4_1_1_1.setBackground(Color.WHITE);
		btnNewButton_4_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_1_1_1.setBounds(86, 433, 66, 66);
		frame.getContentPane().add(btnNewButton_4_1_1_1);
		
		JButton btnNewButton_4_4_9_1 = new JButton("menu");
		btnNewButton_4_4_9_1.setBackground(Color.WHITE);
		btnNewButton_4_4_9_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnNewButton_4_4_9_1.setBounds(238, 433, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_9_1);
	}
}
=======
package messagerie;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.border.MatteBorder;

public class calculatrice {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					calculatrice window = new calculatrice();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public calculatrice() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 328, 551);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.BLACK));
		panel.setForeground(Color.WHITE);
		panel.setBounds(10, 11, 294, 102);
		frame.getContentPane().add(panel);
		panel.setLayout(new CardLayout(0, 0));
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 70));
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(lblNewLabel, "name_682113463660500");
		
		ArrayList<String> calcul = new ArrayList<>();
		
		JButton btnNewButton_4 = new JButton("-");
		btnNewButton_4.setBackground(Color.WHITE);
		btnNewButton_4.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4.setBounds(162, 125, 66, 66);
		frame.getContentPane().add(btnNewButton_4);
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText("-");
				calcul.add("-");
			}
		});
		
		JButton btnNewButton_4_1 = new JButton(">");
		btnNewButton_4_1.setBackground(Color.WHITE);
		btnNewButton_4_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_1.setBounds(10, 433, 66, 66);
		frame.getContentPane().add(btnNewButton_4_1);
		btnNewButton_4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText("");

			}
		});
		
		JButton btnNewButton_4_2 = new JButton("+");
		btnNewButton_4_2.setBackground(Color.WHITE);
		btnNewButton_4_2.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_2.setBounds(86, 125, 66, 66);
		frame.getContentPane().add(btnNewButton_4_2);
		
		JButton btnNewButton_4_3 = new JButton("*");
		btnNewButton_4_3.setBackground(Color.WHITE);
		btnNewButton_4_3.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_3.setBounds(238, 125, 66, 66);
		frame.getContentPane().add(btnNewButton_4_3);
		
		JButton btnNewButton_4_4 = new JButton("4");
		btnNewButton_4_4.setBackground(Color.WHITE);
		btnNewButton_4_4.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4.setBounds(86, 279, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4);
		
		JButton btnNewButton_4_4_1 = new JButton("2");
		btnNewButton_4_4_1.setBackground(Color.WHITE);
		btnNewButton_4_4_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_1.setBounds(162, 202, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_1);
		
		JButton btnNewButton_4_4_2 = new JButton("3");
		btnNewButton_4_4_2.setBackground(Color.WHITE);
		btnNewButton_4_4_2.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_2.setBounds(238, 202, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_2);
		
		JButton btnNewButton_4_4_3 = new JButton("1");
		btnNewButton_4_4_3.setBackground(Color.WHITE);
		btnNewButton_4_4_3.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_3.setBounds(86, 202, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_3);
		
		JButton btnNewButton_4_4_4 = new JButton("5");
		btnNewButton_4_4_4.setBackground(Color.WHITE);
		btnNewButton_4_4_4.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_4.setBounds(162, 279, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_4);
		
		JButton btnNewButton_4_4_5 = new JButton("6");
		btnNewButton_4_4_5.setBackground(Color.WHITE);
		btnNewButton_4_4_5.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_5.setBounds(238, 279, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_5);
		
		JButton btnNewButton_4_4_6 = new JButton("7");
		btnNewButton_4_4_6.setBackground(Color.WHITE);
		btnNewButton_4_4_6.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_6.setBounds(86, 356, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_6);
		
		JButton btnNewButton_4_4_7 = new JButton("8");
		btnNewButton_4_4_7.setBackground(Color.WHITE);
		btnNewButton_4_4_7.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_7.setBounds(162, 356, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_7);
		
		JButton btnNewButton_4_4_8 = new JButton("9");
		btnNewButton_4_4_8.setBackground(Color.WHITE);
		btnNewButton_4_4_8.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_8.setBounds(238, 356, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_8);
		
		JButton btnNewButton_4_4_9 = new JButton("0");
		btnNewButton_4_4_9.setBackground(Color.WHITE);
		btnNewButton_4_4_9.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_4_9.setBounds(162, 433, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_9);
		
		JButton btnNewButton_4_3_1 = new JButton("/");
		btnNewButton_4_3_1.setBackground(Color.WHITE);
		btnNewButton_4_3_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_3_1.setBounds(10, 124, 66, 66);
		frame.getContentPane().add(btnNewButton_4_3_1);
		
		JButton btnNewButton_4_1_1 = new JButton("=");
		btnNewButton_4_1_1.setBackground(Color.WHITE);
		btnNewButton_4_1_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_1_1.setBounds(10, 202, 66, 220);
		frame.getContentPane().add(btnNewButton_4_1_1);
		
		JButton btnNewButton_4_1_1_1 = new JButton(",");
		btnNewButton_4_1_1_1.setBackground(Color.WHITE);
		btnNewButton_4_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton_4_1_1_1.setBounds(86, 433, 66, 66);
		frame.getContentPane().add(btnNewButton_4_1_1_1);
		
		JButton btnNewButton_4_4_9_1 = new JButton("menu");
		btnNewButton_4_4_9_1.setBackground(Color.WHITE);
		btnNewButton_4_4_9_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnNewButton_4_4_9_1.setBounds(238, 433, 66, 66);
		frame.getContentPane().add(btnNewButton_4_4_9_1);
	}
}
>>>>>>> 7e5869195ae5aa3ed2f0afc6e62d3313b2f58985
