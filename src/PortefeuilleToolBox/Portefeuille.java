package PortefeuilleToolBox;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import java.awt.CardLayout;
import javax.swing.SwingConstants;

import Accueil.Accueil;
import InscriptionetConnexion.Connexion_Variablesession;
import PopuperreurToolBox.Popuperreur;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class Portefeuille extends Connexion_Variablesession{

	public JFrame frame;
	public double TotaldepenseD;
    String personneco = getNameC();

	/**@author Benjamin
	 * @version 1.0
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Portefeuille window = new Portefeuille();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Portefeuille() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 904, 473);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		// Cr�ation du panel note + le scroll pan
		JPanel pannote = new JPanel();
		pannote.setBounds(528, 187, 349, 238);
		frame.getContentPane().add(pannote);
		pannote.setLayout(null);

		JScrollPane scrollnote = new JScrollPane();
		scrollnote.setBounds(0, 0, 349, 238);
		pannote.add(scrollnote);
		JPanel panscrollnote = new JPanel();
		scrollnote.setViewportView(panscrollnote);
		panscrollnote.setLayout(new GridLayout(0, 1));

		//panel Wallet
		JPanel panwallet = new JPanel();
		panwallet.setBackground(Color.WHITE);
		panwallet.setBounds(10, 120, 508, 44);
		frame.getContentPane().add(panwallet);
		panwallet.setLayout(null);

		JLabel lblVotrePortefeuille = new JLabel("Wallet de");
		lblVotrePortefeuille.setBounds(194, 11, 58, 19);
		lblVotrePortefeuille.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		panwallet.add(lblVotrePortefeuille);
		lblVotrePortefeuille.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel lblVotrePortefeuille_1 = new JLabel(personneco);
		lblVotrePortefeuille_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblVotrePortefeuille_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		lblVotrePortefeuille_1.setBounds(256, 11, 59, 19);
		panwallet.add(lblVotrePortefeuille_1);

		//Panel avec la partie gestion argent
		JPanel Pangestion = new JPanel();
		Pangestion.setBackground(Color.WHITE);
		Pangestion.setBounds(10, 175, 508, 250);
		frame.getContentPane().add(Pangestion);
		Pangestion.setLayout(null);

		//Montant sur le compte
		JLabel montantwallet = new JLabel("montant  disponilbe sur votre compte :");
		montantwallet.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		montantwallet.setHorizontalAlignment(SwingConstants.LEFT);
		montantwallet.setBounds(33, 218, 253, 22);
		Pangestion.add(montantwallet);

		JLabel TotalCompteLB = new JLabel("0\u20AC");
		TotalCompteLB.setHorizontalAlignment(SwingConstants.CENTER);
		TotalCompteLB.setBounds(233, 218, 96, 22);
		Pangestion.add(TotalCompteLB);

		JLabel montanttotdepense = new JLabel("montant total des d\u00E9penses :");
		montanttotdepense.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		montanttotdepense.setHorizontalAlignment(SwingConstants.LEFT);
		montanttotdepense.setBounds(33, 194, 148, 22);
		Pangestion.add(montanttotdepense);

		JLabel TotaldepenseLB = new JLabel("0\u20AC");
		TotaldepenseLB.setHorizontalAlignment(SwingConstants.CENTER);
		TotaldepenseLB.setBounds(191, 194, 96, 22);
		Pangestion.add(TotaldepenseLB);

		//Header pour refresh ou fermer l'appli
		JPanel Headerpf = new JPanel();
		Headerpf.setBackground(Color.BLACK);
		Headerpf.setBounds(0, 0, 900, 28);
		frame.getContentPane().add(Headerpf);
		Headerpf.setLayout(null);

		JButton refresh = new JButton("refresh");
		refresh.setBackground(Color.WHITE);
		refresh.setBounds(647, 6, 110, 17);
		Headerpf.add(refresh);

		JButton btnFermer = new JButton("accueil");
		btnFermer.setBackground(Color.WHITE);
		btnFermer.setBounds(767, 6, 110, 17);
		Headerpf.add(btnFermer);

		JLabel nomappli = new JLabel("ToolBox");
		nomappli.setHorizontalAlignment(SwingConstants.CENTER);
		nomappli.setForeground(Color.WHITE);
		nomappli.setFont(new Font("Tw Cen MT", Font.PLAIN, 27));
		nomappli.setBounds(10, 0, 88, 31);
		Headerpf.add(nomappli);
		btnFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Accueil window = new Accueil();
				window.frame.setVisible(true);
				frame.dispose();
			}
		});

		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Portefeuille window = new Portefeuille();
				window.frame.setVisible(true);
				frame.dispose();
			}
		});

		//Notes: �critre ajout et lecture
		JPanel panvosnotes = new JPanel();
		panvosnotes.setBackground(Color.WHITE);
		panvosnotes.setBounds(528, 153, 250, 23);
		frame.getContentPane().add(panvosnotes);
		panvosnotes.setLayout(new CardLayout(0, 0));

		JLabel lblVosNotes = new JLabel("Vos notes");
		panvosnotes.add(lblVosNotes, "name_695435672197900");
		lblVosNotes.setHorizontalAlignment(SwingConstants.CENTER);
		lblVosNotes.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));

		JTextArea noteareaecriture = new JTextArea();
		noteareaecriture.setBounds(528, 87, 349, 55);
		frame.getContentPane().add(noteareaecriture);

		JPanel panajoutnote = new JPanel();
		panajoutnote.setBackground(Color.WHITE);
		panajoutnote.setBounds(528, 34, 349, 44);
		frame.getContentPane().add(panajoutnote);
		panajoutnote.setLayout(new CardLayout(0, 0));

		JLabel lblAjoutDpense_1 = new JLabel("Ajout note");
		panajoutnote.add(lblAjoutDpense_1, "name_687243045278200");
		lblAjoutDpense_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblAjoutDpense_1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));

		JButton notebtn = new JButton("E");
		notebtn.setBackground(Color.WHITE);
		notebtn.setBounds(788, 153, 89, 23);
		frame.getContentPane().add(notebtn);

		notebtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
					Statement st = con.createStatement();
					st.executeUpdate("INSERT INTO `note`(`note`,`utilisateur`) VALUES ('"+noteareaecriture.getText()+"','"+personneco+"')");

				} catch (SQLException e1) {

					e1.printStackTrace();
				}

			}
		});

		//Panel gestion pour ajouter des d�penses ou ajouter de l'argent
		JPanel panajoutdepense = new JPanel();
		panajoutdepense.setBackground(Color.WHITE);
		panajoutdepense.setBounds(268, 34, 250, 44);
		frame.getContentPane().add(panajoutdepense);
		panajoutdepense.setLayout(new CardLayout(0, 0));

		JLabel lblAjoutDpense = new JLabel("ajout d\u00E9pense");
		lblAjoutDpense.setHorizontalAlignment(SwingConstants.CENTER);
		lblAjoutDpense.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		panajoutdepense.add(lblAjoutDpense, "name_687915893725400");

		JPanel panajoutargent = new JPanel();
		panajoutargent.setBackground(Color.WHITE);
		panajoutargent.setBounds(10, 34, 250, 44);
		frame.getContentPane().add(panajoutargent);
		panajoutargent.setLayout(new CardLayout(0, 0));

		JLabel lblAjoutCompte = new JLabel("ajout argent ");
		lblAjoutCompte.setHorizontalAlignment(SwingConstants.CENTER);
		lblAjoutCompte.setFont(new Font("Gentium Book Basic", Font.PLAIN, 15));
		panajoutargent.add(lblAjoutCompte, "name_687914880487400");

		JTextField ajoutargent = new JTextField();
		ajoutargent.setColumns(10);
		ajoutargent.setBounds(10, 89, 190, 20);
		frame.getContentPane().add(ajoutargent);
		JButton ajoutargentBTN = new JButton("E");
		ajoutargentBTN.setBackground(Color.WHITE);
		ajoutargentBTN.setBounds(210, 88, 50, 23);
		frame.getContentPane().add(ajoutargentBTN);

		//bouton pour clear les d�penses
		JButton btnClearDpenses = new JButton("Clear portefeuille");
		btnClearDpenses.setFont(new Font("Gentium Book Basic", Font.PLAIN, 11));
		btnClearDpenses.setBackground(Color.WHITE);
		btnClearDpenses.setBounds(325, 219, 173, 23);
		Pangestion.add(btnClearDpenses);

		btnClearDpenses.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
					Statement st = con.createStatement();
					st.executeUpdate("DELETE FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");
		            st.executeUpdate("INSERT INTO `gestionportefeuille`(`ajoutargent`, `ajoutdepense`, `Utilisateur`) VALUES ('0','0','"+personneco+"')");
		            frame.dispose();
		            Portefeuille window = new Portefeuille();
					window.frame.setVisible(true);
					//supprime les d�penses de l'utilisateur connect�
				} catch (SQLException e1) {

					e1.printStackTrace();
				}

			}
		});

		//Panel pour lire en bas de l'appli les d�penses et ajout fait
		JScrollPane panajout = new JScrollPane();
		panajout.setBounds(34, 35, 200, 148);
		Pangestion.add(panajout);

		JPanel panajoutlecture = new JPanel();
		panajout.setViewportView(panajoutlecture);
		panajoutlecture.setLayout(new GridLayout(0, 1));

		JScrollPane pandepense = new JScrollPane();
		pandepense.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pandepense.setBounds(269, 35, 201, 148);
		Pangestion.add(pandepense);

		JPanel pandepenselec = new JPanel();
		pandepense.setViewportView(pandepenselec);
		pandepenselec.setLayout(new GridLayout(0, 1));
		pandepenselec.setLayout(new GridLayout(0, 1));

		JLabel labvosdepenses = new JLabel("Vos d\u00E9penses");
		labvosdepenses.setHorizontalAlignment(SwingConstants.LEFT);
		labvosdepenses.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		labvosdepenses.setBounds(269, 11, 158, 22);
		Pangestion.add(labvosdepenses);

		JLabel labvosajouts = new JLabel("Vos ajouts total ");
		labvosajouts.setHorizontalAlignment(SwingConstants.LEFT);
		labvosajouts.setFont(new Font("Gentium Book Basic", Font.PLAIN, 12));
		labvosajouts.setBounds(34, 11, 158, 22);
		Pangestion.add(labvosajouts);

		JLabel nbajoutotal = new JLabel("\"");
		nbajoutotal.setHorizontalAlignment(SwingConstants.CENTER);
		nbajoutotal.setBounds(118, 15, 116, 14);
		Pangestion.add(nbajoutotal);

		ajoutargentBTN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double v = Double.parseDouble(ajoutargent.getText());
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
					Statement st = con.createStatement();
					st.executeUpdate("INSERT INTO `gestionportefeuille`(`ajoutargent`,`Utilisateur`) VALUES ('"+v+"','"+personneco+"')");

				} catch (SQLException e1) {

					e1.printStackTrace();
				}

			}
		});

		//bouton ajout depense
		JTextField ajoutdepense = new JTextField();
		ajoutdepense.setColumns(10);
		ajoutdepense.setBounds(268, 89, 190, 20);
		frame.getContentPane().add(ajoutdepense);

		JButton ajoutdepenseBTN = new JButton("E");
		ajoutdepenseBTN.setBackground(Color.WHITE);
		ajoutdepenseBTN.setBounds(468, 88, 50, 23);
		frame.getContentPane().add(ajoutdepenseBTN);

		ajoutdepenseBTN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double v = Double.parseDouble(ajoutdepense.getText());
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
					Statement st = con.createStatement();
					st.executeUpdate("INSERT INTO `gestionportefeuille`(`ajoutdepense`,`Utilisateur`) VALUES ('"+v+"','"+personneco+"')");

				} catch (SQLException e1) {

					e1.printStackTrace();
				}

			}
		});

		JButton b = new JButton();
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT ajoutargent FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");

			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("ajoutargent"));

			}

			int index = 0;
			JButton[][] array = new JButton[TC.size()][2];
			for (int i = 0; i < TC.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < TC.size()) {

						b = new JButton("");
						b.setPreferredSize(new Dimension(180, 30));
						array[i][j] = b;
						panajoutlecture.add(array[i][j]);
						b.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b.setBackground(Color.WHITE);
						array[i][j].setText(TC.get(index));
						index = index + 1;
						b.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));//tableau des ajouts d'argent de l'utilisateur connect�
					}
				}
			}

		} catch (SQLException e1) {

			e1.printStackTrace();

		}
		JButton b1 = new JButton();
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT ajoutdepense FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");

			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("ajoutdepense"));

			}

			int index = 0;
			JButton[][] array = new JButton[TC.size()][2];
			for (int i = 0; i < TC.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < TC.size()) {

						b1 = new JButton("");
						b1.setPreferredSize(new Dimension(180, 30));
						array[i][j] = b1;
						pandepenselec.add(array[i][j]);
						b1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b1.setBackground(Color.WHITE);
						array[i][j].setText(TC.get(index));
						index = index + 1;
						b1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)); //tableau des d�penses de l'utilisateur connect�
					}
				}
			}

		} catch (SQLException e1) {

			Popuperreur window = new Popuperreur();
			window.frame.setVisible(true);

		}

		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT note FROM `note` WHERE `utilisateur`= '"+personneco+"'");

			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("note"));

			}
			int index = 0;
			JButton[][] array = new JButton[TC.size()][2];
			for (int i = 0; i < TC.size(); ++i) {
				for (int j = 0; j < 2; ++j) {
					if (index < TC.size()) {

						b1 = new JButton("");
						b1.setPreferredSize(new Dimension(300, 30));
						array[i][j] = b1;
						panscrollnote.add(array[i][j]);
						b1.setFont(new Font("Gentium Book Basic", Font.PLAIN, 20));
						b1.setBackground(Color.WHITE);
						array[i][j].setText(TC.get(index));
						index = index + 1;
						b1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));//tableau des notes de l'utilisateur connect�
					}
				}
			}

		} catch (SQLException e1) {

			Popuperreur window = new Popuperreur();
			window.frame.setVisible(true);

		}

		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT SUM(ajoutargent) FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");

			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("SUM(ajoutargent)"));
            //TC = SUM(ajoutargent) de l'utilisateur connect�
			}
 

		} catch (SQLException e1) {

			Popuperreur window = new Popuperreur();
			window.frame.setVisible(true);

		}
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT SUM(ajoutdepense) FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");

			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("SUM(ajoutdepense)"));
			//TC = SUM(ajoutdepense) de l'utilisateur connect�
			}
			TotaldepenseLB.setText(TC.get(0));
			TotaldepenseD = Double.parseDouble(TC.get(0));

		} catch (SQLException e1) {

			Popuperreur window = new Popuperreur();
			window.frame.setVisible(true);

		}
		try {
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery("SELECT SUM(ajoutargent) FROM `gestionportefeuille` WHERE `Utilisateur`= '"+personneco+"'");

			ArrayList<String> TC = new ArrayList<>();
			while (res.next()) {
				TC.add(res.getString("SUM(ajoutargent)"));
			//TC = SUM(ajoutargent) de l'utilisateur connect�
			}
			//argent dispo sur le compte
			double TotalCompteD = Double.parseDouble(TC.get(0));
			double t = TotalCompteD - TotaldepenseD;
			TotalCompteLB.setText(""+t);
			nbajoutotal.setText(""+TotalCompteD);

			//bouton clear note
			JButton btnClearNote = new JButton("Clear note");
			btnClearNote.setFont(new Font("Gentium Book Basic", Font.PLAIN, 11));
			btnClearNote.setBackground(Color.WHITE);
			btnClearNote.setBounds(325, 194, 173, 23);
			Pangestion.add(btnClearNote);

			btnClearNote.addActionListener(new ActionListener() {


				/**
				 * @author Benjamin
				 * @args e
				 */
				public void actionPerformed(ActionEvent e) {
					try {
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projet?serverTimezone=UTC","root", "root");
						Statement st = con.createStatement();
						st.executeUpdate("DELETE FROM `note` WHERE `Utilisateur`= '"+personneco+"'");
			            st.executeUpdate("INSERT INTO `note`(`note`, `utilisateur`) VALUES ('note vide pour test','"+personneco+"')");
			            frame.dispose();
			            Portefeuille window = new Portefeuille();
						window.frame.setVisible(true);
						//supprime les notes de l'utilisateur connect�
					} catch (SQLException e1) {

						Popuperreur window = new Popuperreur();
						window.frame.setVisible(true);
					}

				}
			});
		} catch (SQLException e1) {

			Popuperreur window = new Popuperreur();
			window.frame.setVisible(true);

		}


	}
}