# ToolBox

Projet Genie Logiciel et POO en licence 2, 2021-2022

## Un projet de 
* ASSANI Said El Amani
* Benjamin Sebert
* Alan Chabault
* Van Bao Phuong
* Youssouf Moussa
* Jalil Chibout
* Arthur Plomdeur


## License
For open source projects, say how it is licensed.

Licensed under the [MIT License](LICENSE).


## Comment installer l'aplication? 
* Tout d'abbord si ce n'est pas déjà fait, vous devez installer et déclarer les modules présent dans le "module-info.java" dans votre BuildPath.

* Ensuite pour que l'application fonctionne correctement, vous devez crée une base de donnée avec les informations suivantes : host= localhost, username =root, mdp = root, nom de la base de donnée = projet.

* Bravo, maintenant vous pouvez lancer le fichier ./src/InscriptionetConnexion/Inscription.java pour vous inscrire et vous connectez!